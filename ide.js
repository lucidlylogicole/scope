//---o:
let IDE = {
    files:{},
    counter:0,
    PREV_FILE:undefined,
    CURRENT_FILE:undefined,
    settings:{
        exts:{},
        modes:{},
        ace:{}
    },
    status_timeout:undefined,
    status_text:'',
    plugins:{},
    CLOSING:0,
    CWD:undefined,
    FileBrowser:undefined,
    path_delim:'/',
    folder_files:['__init__.py','main.js','index.html'],

    addEditor: function(filename,txt) {
        let prom = $P.promise()
        
        // Check if open
        let found = IDE.isFileOpen(filename)
        if (found) {
            IDE.selectTab(found)
            prom.accept(found)
        } else {
            IDE.counter++
            let uuid = $P.uuid()
            
            let basename = IDE.getBaseName(filename)
            
            // Get Tab name
            let tabname = basename
            if (IDE.folder_files.includes(tabname)) {
                try {
                    tabname = IDE.getDirName(filename).split(IDE.path_delim).slice(-1)[0]+IDE.path_delim+basename
                } catch(e){}
            }
            
            if (txt === undefined) {filename = undefined}
            
            // Find mode
            let mode
            let modespl = basename.split('.')
            if (modespl.length > 1) {
                mode = modespl.slice(-1)[0]
            } else {
                mode = 'text'
            }
            if (mode in IDE.settings.extModeMap) {
                mode = IDE.settings.extModeMap[mode]
            }

            //---find editor from mode
            let editor_type = IDE.settings.editors.default
            if (mode in IDE.settings.modes && IDE.settings.modes[mode].editor !== undefined) {
                editor_type = IDE.settings.modes[mode].editor
            }

            let options = {
                path:filename,
                name:basename,
                tabname:tabname,
                type:'editor',
                ext:basename.split('.').slice(-1)[0],
                src:IDE.settings.editors[editor_type].src,
                txt:txt,
                mode:mode,
                editor_type:editor_type,
            }
            
            IDE.addTab(options).then(uuid=>{
                prom.accept(uuid)
            })
            
        }
        return prom
        
    },
    
    //---Tabs
    addTab: function(options) {
        let prom = $P.promise()
        
        let uuid = $P.uuid()
        IDE.files[uuid] = {
            path:options.path,
            name:options.name,
            tabname:options.tabname,
            changed:0,
            lastTime:new Date().getTime()-5000,
            settings:{},
            is_settings:0,
            dialog_open:0,
            type:options.type,
            mode:options.mode,
            closeable:1,
        }
        
        if (options.closeable !== undefined) {IDE.files[uuid].closeable = options.closeable}
        
        // Add Tab
        let tab_config = {h:'.tab',onclick:`IDE.selectTab('${uuid}')`, 'data-uuid':uuid, title:options.path, draggable:true, c:[]}
        if (options.icon !== undefined) {
            tab_config.c.push({h:'img.tab-icon',src:options.icon})
        }
        if (options.tabname !== undefined) {
            tab_config.c.push({h:'.tab-title ext-'+options.ext, text:options.tabname})
        } else {
            tab_config.c[0].h = 'img.tab-icon icon-only'
        }
        if (IDE.files[uuid].closeable) {
            tab_config.onauxclick=`if(event.which == 2){IDE.closeTab('${uuid}')}`
            tab_config.c.push({h:'.tab-close',text:'&times;', onclick:`IDE.closeTab('${uuid}')`})
        }
        
        let tab = $P.create(tab_config)
        $P.elm('tabbar').insertBefore(tab,$P.elm('tab_spacer'))
        
        // Add Editor
        let editor_pg = $P.create({h:'iframe.ifr-editor page', scrolling:'auto', 'data-uuid':uuid,
            src:options.src,
            parent:'editor_pages',
        })
        
        IDE.files[uuid].element = editor_pg
        
        // on iframe load
        editor_pg.addEventListener('load',function(){
            
            // Editor Setup
            let Editor
            if (options.type == 'editor') {
                Editor = editor_pg.contentWindow.CodeEditor
                Editor.uuid = uuid
                
                // Get editor settings
                let editor_settings = JSON.parse(JSON.stringify(IDE.settings.editors[options.editor_type]))
                if (options.mode in IDE.settings.modes && options.editor_type in IDE.settings.modes[options.mode]) {
                    let main_settings = IDE.settings.modes[options.mode][options.editor_type]
                    for (let ky in main_settings) {
                        if (['options','outline'].indexOf(ky) > -1) {
                            for (let oky in main_settings[ky]) {
                                editor_settings[ky][oky] = main_settings[ky][oky]
                            }
                            
                        } else {
                            editor_settings[ky] = main_settings[ky]
                        }
                    }
                }
                
                Editor.load(options.txt,options.mode,editor_settings)
                
                IDE.files[uuid].Editor = Editor
            }
            
            // Select Tab
            IDE.selectTab(uuid)
            
            // Events
            IDE.addDropEvent(editor_pg.contentWindow.document.body)
            $P.listen(editor_pg.contentWindow.document,'keydown',IDE.keymap.event)
            
            // Editor Events
            if (options.type == 'editor') {

                // Editor Save Event
                Editor.element.addEventListener('saveevent', (event)=>{
                    IDE.saveFile(event.detail)
                })
    
                // Editor Changed Event
                Editor.element.addEventListener('textchanged', (event)=>{
                    let uuid = event.detail.uuid
                    IDE.files[uuid].changed = event.detail.changed
                    if (IDE.files[uuid].changed) {
                        $P.class.add($P.query(`.tab[data-uuid="${uuid}"] .tab-title`),'unsaved')
                    } else {
                        $P.class.remove($P.query(`.tab[data-uuid="${uuid}"] .tab-title`),'unsaved')
                    }
                })

                // Send Open Event
                var evt = new CustomEvent('fileOpened',{detail:{uuid:uuid,filepath:options.path}})
                window.dispatchEvent(evt)

            }
            
            prom.accept(uuid)
            
        })
        return prom
    },
    
    selectTab: function(uuid) {
        // Get Current Tab
        this.PREV_FILE = IDE.currentTab()
        
        $P.class.remove($P.query('#tabbar .tab.active'),'active')
        $P.class.add($P.query(`.tab[data-uuid="${uuid}"]`),'active')
        
        // Select Page
        $P.class.remove($P.query('#editor_pages .page.active'),'active')
        $P.class.add($P.query(`.page[data-uuid="${uuid}"]`),'active')
        
        IDE.CURRENT_FILE = uuid
        
        if (uuid && uuid in IDE.files && IDE.files[uuid].type == 'editor') {
            document.title = IDE.files[uuid].tabname + ' - '+IDE.files[uuid].path
            
            // Focus
            if (IDE.files[uuid].Editor) {
                IDE.files[uuid].Editor.focus()
            }
            
            var evt = new CustomEvent('tabChanged',{detail:{name:IDE.files[uuid].tabname,uuid:uuid}})
            window.dispatchEvent(evt)
            
        } else {
            document.title = 'Scope'
        }
        
    },
    
    currentTab: function() {
        let cur_tab = $P.query('#tabbar .tab.active')
        if (cur_tab.length > 0) {
            return $P.attr(cur_tab[0],'data-uuid')
        }
        return null
    },
    currentFile: function() {
        if (IDE.CURRENT_FILE) {
            return IDE.files[IDE.CURRENT_FILE]
        }
        return undefined
    },
    
    currentEditor: function() {
        let uuid = IDE.currentTab()
        if (uuid == null) {return null}
        if (uuid in IDE.files) {
            return IDE.files[uuid].Editor
        } else {
            return null
        }
        // if ($P.query(`.page[data-uuid="${uuid}"]`)[0].contentWindow === undefined){return null}
        // return  $P.query(`.page[data-uuid="${uuid}"]`)[0].contentWindow.code_editor
    },
    
    file: function(uuid) {
        // Get File Object (current if undefined)
        if (uuid === undefined) {uuid = IDE.CURRENT_FILE}
        if (uuid) {
            return IDE.files[uuid]
        } else {
            return undefined
        }
    },
    
    closeTab: async function(uuid) {
        if (event) {
            event.stopPropagation()
        }
        
        // Check to save
        let ok = 1
        if (IDE.files[uuid].changed) {
            let resp = await $P.dialog.message({text:'Save changes to '+IDE.files[uuid].name+'?',btns:['yes','no','cancel']})
            if (resp == 'yes') {
                ok = IDE.saveFile(uuid)
            } else if (resp == 'cancel') {
                ok = 0
            }
        }
        
        if (ok) {

            if (uuid == IDE.PREV_FILE) {
                IDE.PREV_FILE = undefined
            }
            
            // Close Help if Settings File
            if (IDE.files[uuid].is_settings) {
                IDE.RightWindow.toggle(0)
            }
            
            // Select another tab
            if (uuid == IDE.CURRENT_FILE) {
                IDE.incrementTab(-1)
            }
            
            let filename = IDE.files[uuid].path
            
            $P.remove($P.query1(`.tab[data-uuid="${uuid}"]`))
            $P.remove($P.query1(`.page[data-uuid="${uuid}"]`))
            
            delete IDE.files[uuid]
            
            var evt = new CustomEvent('tabClosed',{detail:{uuid:uuid,filepath:filename}})
            window.dispatchEvent(evt)
            
        }
        return ok
        
    },
    
    closeAllTabs: async function() {
        let ok = 1
        IDE.CLOSING = 1
        for (let uuid in IDE.files) {
            if (IDE.files[uuid].type == 'editor') {
                ok = await IDE.closeTab(uuid)
                if (!ok) {
                    break
                }
            }
        }
        IDE.CLOSING = 0
        return ok
    },
    
    incrementTab: function(inc) {
        let tabs = []
        let active_tab=0
        let cnt = -1
        for (let tab of $P.query('#tabbar .tab')) {
            tabs.push($P.attr(tab,'data-uuid'))
            cnt++
            if ($P.class.has(tab,'active')) {
               active_tab = cnt 
            }
        }
        if (tabs.length > 0) {        
            active_tab = active_tab + inc
            if (active_tab > tabs.length-1) {
                active_tab = 0
            } else if (active_tab < 0) {
                active_tab = tabs.length-1
            }
            
            IDE.selectTab(tabs[active_tab])
        }
        
    },
    
    //---Editor Functions
    newFile: function(prompt) {
        if (prompt) {
            $P.dialog.input({text:'new file name',val:'Untitled '+(IDE.counter+1)}).then(resp => {
                if (resp) {
                    IDE.addEditor(resp)
                }
            })
        } else {
            IDE.addEditor('Untitled '+(IDE.counter+1))
        }
    },
    
    getBaseName: function(filename) {
        return filename.split(IDE.path_delim).slice(-1)[0]
    },

    getDirName: function(filename) {
        return filename.split(IDE.path_delim).slice(0,-1).join(IDE.path_delim)
    },

    renameFile: function(uuid,filename) {
        let basename = IDE.getBaseName(filename)
        IDE.files[uuid].path = filename
        IDE.files[uuid].name = basename
        IDE.files[uuid].tabname = basename
        
        // Update Tab
        let tab_elm = 
        $P.query1(`.tab[data-uuid="${uuid}"]`).title=filename
        $P.query1(`.tab[data-uuid="${uuid}"] .tab-title`).innerText=basename
        
        // Update Mode
        let mode
        let modespl = basename.split('.')
        if (modespl.length > 1) {
            mode = modespl.slice(-1)[0]
        } else {
            mode = 'text'
        }
        if (mode in IDE.settings.extModeMap) {
            mode = IDE.settings.extModeMap[mode]
        }

        IDE.files[uuid].Editor.setMode(mode)

        // Update title
        document.title = IDE.files[uuid].tabname + ' - '+IDE.files[uuid].path
    },
    
    addDropEvent: function(element) {
        element.ondragover = () => {return false}
        element.ondragleave = () => {return false}
        element.ondragend = () => {return false}
        element.ondrop = (e) => {
            e.preventDefault()
            for (let f of e.dataTransfer.files) {
                var reader = new FileReader()
                reader.onload = function(ev) {
                    // finished reading file data.
                    IDE.addEditor(f.name,ev.target.result)
                }
    
                reader.readAsText(f) // start reading the file data.
            }
            return false
        }
    },
    
    showEditorSettings: function() {
        let cedit = IDE.currentEditor()
        if (cedit) {    
            cedit.showSettings()
        }
    },
    
    isFileOpen: function(filename) {
        let found = 0
        for (let uuid in IDE.files) {
            if (IDE.files[uuid].path == filename) {
                found = uuid
                break
            }
        }
        return found
    },
    
    //---
    //---o:
    BottomTab: {
        index:0,
        currentTab:0,
        prevTab:0,
        
        nextIndex: function() {
            IDE.BottomTab.index++
            return IDE.BottomTab.index
        },
        
        addTab: function(options) {
            let tab_ind = IDE.BottomTab.nextIndex()
            let new_tab = {h:`#bt_tab_${tab_ind}.tab`, 'tab-id':tab_ind, onclick:`IDE.BottomTab.selectTab(${tab_ind})`, c:[]}
            if (options.tab_class) { new_tab.h += ' '+options.tab_class}
            // Tooltip
            if (options.tooltip) {new_tab.title = options.tooltip}
            // Icon
            let icon_elm
            if (options.icon) {
                icon_elm = {h:'img.tab-icon', src:options.icon}
                new_tab.c.push(icon_elm)
            }
            // Title
            let title_elm = {h:'.tab-title',text:''}
            if (options.title) {
                title_elm.text = options.title
                new_tab.c.push(title_elm)
            } else {
                icon_elm.h += ' icon-only'
            }
            
            // Closable Tab
            if (options.close === undefined || options.close) {
                new_tab.onauxclick = `if(event.which == 2){IDE.BottomTab.closeTab('${tab_ind}')}`
                let close_elm = {h:'.tab-close',text:'&times;', onclick:`IDE.ButtonTab.closeTab('${tab_ind}',event)`}
                if (typeof(options.close) == 'string') {
                    close_elm.onclick=options.close
                    new_tab.onauxclick = `if(event.which == 2){${options.close}}`
                }
                new_tab.c.push(close_elm)
            }
            let tab_elm = $P.create(new_tab)
            
            $P.elm('bottom_tabbar').insertBefore(tab_elm, $P.elm('bottom_tab_spacer'))
            
            // Create a Page
            let new_page = {h:`#bt_page_${tab_ind}.page`,'tab-id':tab_ind, parent:'bottom_pages', c:[]}
            if (options.page !== undefined) {
                new_page.c.push(options.page)
            }
            let page_elm = $P.create(new_page)
            
            return {index:tab_ind, tab:tab_elm, page:page_elm}
        },
        
        selectTab: function(index) {
            let tab_qry = $P.query(`#bottom_tabbar .tab[tab-id="${index}"]`)
            if (tab_qry.length >0) {
                let tab_elm = tab_qry[0]
                let page_elm = $P.query1(`#bottom_pages .page[tab-id="${index}"]`)
                $P.class.remove($P.query('#bottom_tabbar .tab.active'),'active')
                IDE.BottomTab.currentTab = index
                if (index == 0) {
                    // Hide Bottom Section
                    $P.hide('bottom_pages')
                    $P.hide('bottom_win_splitter')
                    $P.hide('bottom_buttons')
                    $P.class.add(tab_elm,'active')
                } else {
                    // Show Page
                    $P.show('bottom_pages')
                    $P.show('bottom_win_splitter')
                    $P.show('bottom_buttons')
                    $P.class.add(tab_elm,'active')
                    $P.page.show(page_elm)
                    IDE.BottomTab.prevTab = index
                }
            }
        },
        
        closeTab: function(index) {
            // Remove Page and Tab
            $P.remove(`bt_tab_${index}`)
            $P.remove(`bt_page_${index}`)
        },
        
        toggleVisible: function() {
            if (IDE.BottomTab.currentTab == 0) {
                // Show
                if (IDE.BottomTab.prevTab != 0) {
                    IDE.BottomTab.selectTab(IDE.BottomTab.prevTab)
                }
            } else {
                // Hide 
                $P.elm('b_btm_hide').click()
            }
        },
        
        expand: function() {
            $P.elm('bottom_pages').style['flex-basis']='50vh'
            
        },
        
        shrink: function() {
            $P.elm('bottom_pages').style['flex-basis']='186px'
        },
        
        toggleSize: function() {
            if ($P.elm('bottom_pages').offsetHeight > 200) {
                IDE.BottomTab.shrink()
            } else {
                IDE.BottomTab.expand()
            }
        },
        
        incrementTab: function(inc) {
            let tabs = []
            let active_tab=0
            let cnt = -1
            for (let tab of $P.query('#bottom_tabbar .tab')) {
                tabs.push($P.attr(tab,'tab-id'))
                cnt++
                if ($P.class.has(tab,'active')) {
                   active_tab = cnt 
                }
            }
            if (tabs.length > 0) {        
                active_tab = active_tab + inc
                if (active_tab > tabs.length-1) {
                    active_tab = 0
                } else if (active_tab < 0) {
                    active_tab = tabs.length-1
                }
                
                IDE.BottomTab.selectTab(tabs[active_tab])
            }
        }

    },

    //---
    //---o:
    RightWindow: {
        pages:{},
        
        toggle: function(visibility) {
            
            if (visibility === undefined) {
                visibility = $P.class.has('right_window','hidden')
            }
            
            if (visibility) {
                $P.show('right_window')
                $P.show('right_win_splitter')
                $P.show('b_right_menu')
                $P.elm('img_right_toggle').src = 'icons/arrow_r.svg'
                $P.class.add('b_right_toggle','active')
            } else {
                $P.hide('right_window')
                $P.hide('right_win_splitter')
                $P.hide('b_right_menu')
                $P.elm('img_right_toggle').src = 'icons/arrow_l.svg'
                $P.class.remove('b_right_toggle','active')
            }
            
            
        },
        
        showPage: function(pg_uuid) {
            IDE.RightWindow.toggle(1)
            $P.class.remove($P.query('#right_window .page.active'),'active')
            $P.class.add(IDE.RightWindow.pages[pg_uuid].element,'active')
        },
        
        closePage: function(pg_uuid) {
            // Get Current page
            if (pg_uuid === undefined) {
                let pg_elms = $P.query('#right_window .page.active')
                if (pg_elms.length > 0 && $P.attr(pg_elms[0],'data-pg-uuid')) {
                    pg_uuid = $P.attr(pg_elms[0],'data-pg-uuid')
                }
            }
            
            if (pg_uuid !== undefined) {
                let ok = 1
                if (IDE.RightWindow.pages[pg_uuid].onclose !== undefined) {
                    ok = IDE.RightWindow.pages[pg_uuid].onclose(pg_uuid)
                }
                
                if (ok) {
                    $P.remove(IDE.RightWindow.pages[pg_uuid].element)
                    delete IDE.RightWindow.pages[pg_uuid]
                }
            }
        },
        
        addPage: function(opt) {
            
            let right_elm
            
            let pg_uuid = $P.uuid(0)
            IDE.RightWindow.pages[pg_uuid] = {closable:1}
            
            if ('file_uuid' in opt) {
                // Check for existing file
                for (let ipg_uuid in IDE.RightWindow.pages) {
                    if (IDE.RightWindow.pages[ipg_uuid].file_uuid == opt['file_uuid']) {
                        IDE.RightWindow.showPage(ipg_uuid)
                        return IDE.RightWindow.pages[ipg_uuid].element
                    }
                }
                IDE.RightWindow.pages[pg_uuid].file_uuid = opt['file_uuid']
            }
            
            if (opt.close==0) {
                IDE.RightWindow.pages[pg_uuid].closable = 0
            } else if (typeof(opt.close) == 'function') {
                IDE.RightWindow.pages[pg_uuid].onclose = opt.close
            }
            
            if (!opt.iframe && opt.iframe !== undefined) {
                // Load Content
                opt.content['data-pg-uuid'] = pg_uuid
                right_elm = $P.create(opt.content, {parent:'right_window'})
            } else {
                // Load an iframe
                let iframe_elm = {
                    h:'iframe.page active', 'data-pg-uuid':pg_uuid, c:[],
                }
                
                if (opt.sandbox === undefined || opt.sandbox) {
                    iframe_elm.sandbox = 'allow-scripts allow-popups'
                }
                
                if (opt.src !== undefined) {
                    // Source
                    iframe_elm.src = opt.src
                } else if (opt.content !== undefined) {
                    // Content
                    iframe_elm.c.push(opt.content)
                }
                
                right_elm = $P.create(iframe_elm, {parent:'right_window'})
                
            }

            IDE.RightWindow.pages[pg_uuid].element = right_elm
            return pg_uuid
        },
        
        showPageMenu: function(evt) {
            //
        }
        
    },
    
    
    //---
    //---Statusbar
    setStatusBar: function(txt,mode,delay,append) {
        // modes: undefined, warning, error
        
        if (append) {
            if (IDE.status_text != '') {
                txt = IDE.status_text + '<br>'+txt
            }
        }
        IDE.status_text = txt
        
        if (delay === undefined) {delay = 3}
        $P.html('statusbar_text',txt)
        if (mode !== undefined) {
            $P.elm('statusbar').className=mode
        } else {
            $P.elm('statusbar').className=''
        }
        if (delay != 0) {
            
            if (IDE.status_timeout) {
                clearTimeout(IDE.status_timeout)
            }
            
            IDE.status_timeout = setTimeout(function(){
                $P.hide('statusbar')
                IDE.status_text=''
            }, delay*1000)
        }
    },

    
    fileStats: function() {
        let uuid = IDE.currentTab()
        if (uuid == null) {return null}
        let pos = IDE.files[uuid].Editor.getCurrentPosition()
        let txt = IDE.files[uuid].Editor.getText()
        let lns = txt.split('\n').length
        let wds = txt.split(' ').length
        
        txt = `Row: ${pos[0]+1} Column: ${pos[1]} Lines: ${lns} Words: ${wds}`
        IDE.setStatusBar(txt, '', 5)
    },

    //---
    //---Events
    loadKeyMapping: function() {
        IDE.keymap = $P.keymap(document)
        // IDE.keymap.add('r',func,options) 
        IDE.keymap.add('Escape',$P.menu.close) 
        IDE.keymap.add('n',IDE.newFile, {ctrl:1})
        IDE.keymap.add('o',IDE.browseFiles, {ctrl:1})
        IDE.keymap.add('s',IDE.saveFile, {ctrl:1})
        IDE.keymap.add('s',IDE.fileStats, {alt:1})
        IDE.keymap.add('w',()=>{}, {ctrl:1})
        IDE.keymap.add('w',()=>{}, {alt:1})
        IDE.keymap.add('e',()=>{
            // Focus on editor
            if (IDE.currentEditor()) {
                IDE.currentEditor().focus()
            }
        }, {alt:1})
        // F5 Run
        IDE.keymap.add('F5',()=>{IDE.runCommand(1)}, {shift:1})
        IDE.keymap.add('F5',()=>{IDE.runCommand(0,1)}, {ctrl:1})
        IDE.keymap.add('F5',()=>{IDE.runCommand()})
        // F6
        IDE.keymap.add('F6',()=>{IDE.compileCommand()})
        IDE.keymap.add('F6',()=>{IDE.compileCommand(1)}, {shift:1})
        // F3
        IDE.keymap.add('F3',()=>{
            if (IDE.FileBrowser) {
                IDE.FileBrowser.focus()
            }
        }, {shift:1})
        IDE.keymap.add('F3',()=>{
            if (IDE.FileBrowser) {
                IDE.FileBrowser.toggle()
            }
        })
        IDE.keymap.add('F8',IDE.BottomTab.toggleSize,{shift:1})
        IDE.keymap.add('F8',IDE.BottomTab.toggleVisible)
        IDE.keymap.add('F9',IDE.RightWindow.toggle)
        IDE.keymap.add('F11',()=>{
            if (document.fullscreenElement) {
                document.exitFullscreen()
            } else {
                document.documentElement.requestFullscreen()
            }
        })
        IDE.keymap.add('F12',()=>{
            if (IDE.toggleDevTools) {
                IDE.toggleDevTools()
            }
        })
        IDE.keymap.add('Tab',()=>{
            if (IDE.PREV_FILE !== undefined) {
                event.preventDefault()
                IDE.selectTab(IDE.PREV_FILE)
            }
        }, {ctrl:1, preventDefault:0})
        
        // Page Up/Down
        IDE.keymap.add('PageUp',event=>{IDE.incrementTab(-1)}, {ctrl:1})
        IDE.keymap.add('PageDown',()=>{IDE.incrementTab(1)}, {ctrl:1})

        IDE.keymap.add('PageUp',event=>{IDE.BottomTab.incrementTab(-1)}, {alt:1})
        IDE.keymap.add('PageDown',()=>{IDE.BottomTab.incrementTab(1)}, {alt:1})

    },

    load: function() {

        // document.onkeydown = IDE.keyPressEvent
        IDE.loadKeyMapping()
        
        //---Drag/Drop Files
        IDE.addDropEvent(document.body)
        
        // Load Workspaces
        IDE.Workspace.load()
        
        $P.elm('b_btm_hide').click()
        
        // Get Settings
        IDE.getSettings()
        if (IDE.settings.tabs.color) {
            $P.class.add('tabbar','show-color')
        }
        IDE.CWD = IDE.settings.cwd
        
        // Update UI Settings
        // $P.elm('left_window').style.width = IDE.settings.window.leftWindow.width
        $P.elm('left_window').style['flex-basis'] = IDE.settings.window.leftWindow.width
        let rt_size = 1
        rt_size = IDE.settings.window.rightWindow.size/100
        if (rt_size != 1) {
            rt_size = rt_size/(1-rt_size)
        }
        $P.elm('right_window').style['flex'] = rt_size
        
        //---Drag and Drop Tabs
        $P.setDropEvent({
            element:'tabbar',
            onDrop:(evt, e_uuid, drag_typ) =>{
                evt.preventDefault()
                let move_elm = $P.query1(`.tab[data-uuid="${e_uuid}"]`)
                if (evt.target.id == 'tab_spacer') {
                    // Move to end
                    $P.elm('tabbar').insertBefore(move_elm, $P.elm('tab_spacer'))
                } else {
                    // Before a tab
                    $P.elm('tabbar').insertBefore(move_elm, evt.target.closest('.tab'))
                }
            },
            dragOver:evt =>{
                let elm = evt.target.closest('.tab')
                if (elm) {
                    $P.class.add(elm,'tab_dragover')
                }
            },
            dragLeave:evt =>{
                let elm = evt.target.closest('.tab')
                if (elm) {
                    $P.class.remove(elm,'tab_dragover')
                }
            },
        })
        
        $P.setDragEvent({
            element:'tabbar',
            attr:'data-uuid',
            type:'tab',
            dragStart:function(evt) {
                evt.target.click()
            },
            dragEnd:function(evt) {
                $P.class.remove($P.query('.tab_dragover'),'tab_dragover')
            }
        })
        
        //---Splitters
        $P.splitter('left_win_splitter','left_window','left')
        $P.splitter('right_win_splitter','right_window','right')
        $P.splitter('bottom_win_splitter','bottom_pages','bottom')
    },


    //---
    //---Workspaces
    //---o:
    Workspace:{
        current:undefined, // {path,name}
        workspaces:[],
        MENUITEM:undefined,
        
        load: function() {
            IDE.Workspace.getWorkspaces()
            
            // Workspace Open Event
            // var evt = 
            window.dispatchEvent(new CustomEvent('workspaceLoaded',{detail:{}}))
            
            // Load to Home Page
            // $P.clear('workspaces')
            // let wksp_elms = []
            // for (let wksp of IDE.Workspace.workspaces) {
            //     if (!wksp.startsWith('_')) {
            //         wksp_elms.push({
            //             h:'.workspace-item grad-text',text:wksp, 'data-wksp':wksp, onclick:`IDE.Workspace.open('${wksp}')`
            //         })
            //     }
            // }
            // $P.create(wksp_elms,{parent:'workspaces'})
        },
        
        open: async function(wksp_name) {
            // Close Current Workspace
            let ok = await IDE.Workspace.close()
            
            if (ok) {
                let wD = IDE.Workspace.getWorkspace(wksp_name)
                
                if (!wD) {
                    $P.dialog.message({text:'The Workspace could not be opened. Maybe the path changed.'})
                    return
                }
                
                IDE.Workspace.current = {
                    name:wksp_name,
                    path:wD.path
                }

                // Set Current Directory
                IDE.CWD = wD.path
                
                // Open Files
                for (let fD of wD.files) {
                    let nf_uuid = await IDE.openFile(fD.path)
                    if (fD.settings && nf_uuid) {
                        IDE.files[nf_uuid].settings = fD.settings
                    }
                }
                // Open Last Open File
                if (wD.lastOpenFile) {
                    IDE.openFile(wD.lastOpenFile)
                }
                
                $P.show('wksp-tab-container')
                $P.html('wksp_title',wksp_name)
                
                // Load Filebrowser
                if (IDE.FileBrowser) {
                    IDE.FileBrowser.loadDirectory(wD.path,'fb_dir')
                }
                
                // Workspace Open Event
                var evt = new CustomEvent('workspaceOpened',{detail:{name:wksp_name,path:wD.path}})
                window.dispatchEvent(evt)
                
            }
            
        },
        
        save: function() {
            let prom = $P.promise()
            let wD = {path:IDE.Workspace.current.path, files:[]}
            
            // Get Open Tabs (in order)
            for (let tab_elm of $P.query('#tabbar .tab')) {
                let uuid = $P.attr(tab_elm,'data-uuid')
                if (uuid in IDE.files && IDE.files[uuid].path) {
                    wD.files.push({path:IDE.files[uuid].path,settings:IDE.files[uuid].settings})
                }
            }
            
            let cur_file = IDE.currentFile()
            if (cur_file) {
                wD.lastOpenFile = cur_file.path
            }
            
            // Save to File
            IDE.Workspace.saveWorkspace(IDE.Workspace.current.name,wD)
            prom.accept()
            
            return prom
        },
        
        close: async function(reload_filebrowser) {
            
            // Save Workspace
            if (IDE.Workspace.current !== undefined) {
                await IDE.Workspace.save()
            }
            
            // Close all Tabs
            IDE.Workspace.current = undefined
            let ok = await IDE.closeAllTabs()
            if (ok) {
                $P.hide('wksp-tab-container')
                
                // Reload to default directory
                if (reload_filebrowser && IDE.FileBrowser) {
                    IDE.FileBrowser.loadDirectory(IDE.settings.cwd,'fb_dir')
                }
                
            }
            return ok
        },
        
        // Placeholder Functions (backend implements)
        new: function() {}, 
        rename: function(wksp) {}, 
        delete: function(wksp) {}, 
        getWorkspaces: function() {return []},  // Placeholder
        getWorkspace: function(name) {},  // Placeholder
        saveWorkspace: function(name,settings) {}, // Placeholder
        
    },
    
    
    //---
    //---Placeholders Functions (backend implements)
    openFile: function(filename, line_no) {},
    saveFile: function(uuid) {},
    saveFileAs: function(uuid) {},
    exit: function() {
        var evt = new CustomEvent('ideExit',{})
        window.dispatchEvent(evt)
    },
    getSettings: function() {},
    editSettings: function() {},
    browseFiles: function() {},
    runCommand: function(start_only,current_tab) {},
    compileCommand: function(start_only,current_tab) {},
    viewDocs: function(page) {
        if (!(IDE.doc_page && IDE.doc_page in IDE.RightWindow.pages)) {
            let qry = ''
            if (page !== undefined) {qry = '?file='+page}
            IDE.doc_page = IDE.RightWindow.addPage({src:'docs/doc_viewer.html'+qry,iframe:1,sandbox:0})
        }
        IDE.RightWindow.showPage(IDE.doc_page)
    },
    
}


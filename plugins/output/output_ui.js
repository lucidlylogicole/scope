//---o:
OutputPlugin = {
    
    files:{},
    processes:{},
    
    //---button functions
    run:function(start_only,current_tab,compile_mode) {
        if (current_tab == 1) {
            // Run Current Bottom Tab
            let btn = $P.query(`#bt_page_${IDE.BottomTab.currentTab } .btn-run`)
            if (btn.length > 0) {
                btn[0].click()
            }
            return
        }
        
        if (IDE.CURRENT_FILE) {
            // Save
            if (IDE.files[IDE.CURRENT_FILE].changed) {
                IDE.saveFile(IDE.CURRENT_FILE)
            }
            
            if (IDE.CURRENT_FILE in OutputPlugin.files) {
                // Run
                let out_uuid = OutputPlugin.files[IDE.CURRENT_FILE]
                if (OutputPlugin.processes[out_uuid].type == 'preview' || OutputPlugin.processes[out_uuid].type == 'preview cmd') {
                    // Update preview
                    OutputPlugin.updatePreview(out_uuid)
                } else {
                    // Toggle Run
                    OutputPlugin.toggleRun(out_uuid)
                }
                if (IDE.BottomTab.currentTab != OutputPlugin.processes[out_uuid].count) {
                    // Select Current Tab
                    IDE.BottomTab.selectTab(OutputPlugin.processes[out_uuid].count)
                }
            } else {
                // Add Output
                OutputPlugin.addEditorOutput(IDE.CURRENT_FILE,!start_only,compile_mode)
            }
            
        } else {
            IDE.setStatusBar('no file to run','warning')
        }
        
    },
    
    compile: function(start_only,current_tab) {
        OutputPlugin.run(start_only,current_tab,1)
    },
    
    //---Add Output
    addEditorOutput:function(file_uuid,start_running,compile_mode) {
        // Add output or preview for editor file
        let fD = IDE.currentFile()
        
        if (!fD.path) {
            IDE.setStatusBar('please save file first before running','warning')
            return
        }
        
        // Get Run/Compile Command from mode in settings
        let mode_run = ''
        if (!compile_mode) {
            // Run Mode
            if (!(fD.mode in IDE.settings.modes && 'run' in IDE.settings.modes[fD.mode])) {
                if (start_running) {
                    IDE.setStatusBar(`The mode, <b>${fD.mode}</b>, does not have any run commands associated with it.`,'warning')
                    return
                }
                
            } else {
                mode_run = IDE.settings.modes[fD.mode]['run']
            }
        } else {
            // Compile Mode
            if (!(fD.mode in IDE.settings.modes && 'compile' in IDE.settings.modes[fD.mode])) {
                if (start_running) {
                    IDE.setStatusBar(`The mode, <b>${fD.mode}</b>, does not have any compile commands associated with it.`,'warning')
                    return
                }
                
            } else {
                mode_run = IDE.settings.modes[fD.mode]['compile']
            }
        }
        
        // Setup Output info
        let out_uuid = $P.uuid(0)
        OutputPlugin.files[file_uuid] = out_uuid
        OutputPlugin.processes[out_uuid]={file_uuid:file_uuid,
            path:fD.path,
            status:0,
            run_status:0,
            name:fD.name,
            type:'output',
            mode:fD.mode,
            history:[],
            history_pointer:0,
        }
        
        // Run Command
        let run_cmd = ''
        
        // Load Default Run Command
        if (mode_run.includes('{{filename}}')) {
            run_cmd = $P.replaceAll(mode_run,'{{filename}}','"'+fD.path+'"')
        } else {
            run_cmd = mode_run+' '+fD.path
        }
        if (run_cmd.includes('{{scope}}')) {
            run_cmd = $P.replaceAll(run_cmd,'{{scope}}',IDE.path)
        }
        // Replace any path settings
        for (let spth in IDE.settings.paths) {
            if (run_cmd.includes('{{'+spth+'}}')) {
                run_cmd = $P.replaceAll(run_cmd,'{{'+spth+'}}','"'+IDE.settings.paths[spth]+'"')
            }
        }
        // if (run_cmd.includes('{{electron}}')) {
        //     run_cmd = $P.replaceAll(run_cmd,'{{electron}}',IDE.settings.plugins['output'].electron_path)
        // }
        OutputPlugin.processes[out_uuid].dflt_run_cmd = run_cmd
        
        // Load Run Command from file settings
        if (IDE.files[file_uuid].settings.run_cmd) {
            run_cmd = IDE.files[file_uuid].settings.run_cmd
        }
        // Check For Special Modes
        if (mode_run == 'preview') {
            // Preview HTML
            pg_uuid = IDE.RightWindow.addPage({src:IDE.files[file_uuid].path,sandbox:0,close:()=>{return OutputPlugin.closePreview(out_uuid)}})
            OutputPlugin.processes[out_uuid].type = 'preview'
            OutputPlugin.processes[out_uuid].pg_uuid = pg_uuid
            IDE.RightWindow.showPage(pg_uuid)
            return
            
        } else if (mode_run.startsWith('preview ')) {
            //---Preview Output CMD to HTML
            OutputPlugin.processes[out_uuid].type = 'preview cmd'
            
            let output
            if (mode_run == 'preview markdown') {
                //---Markdown
                output = window.electron_api.sendSync('plugin',{plugin:'output',function:'markdownRender',
                    details:{filename:IDE.files[file_uuid].path}})
            } else if (mode_run == 'preview kite') {
                //---Kite
                output = window.electron_api.sendSync('plugin',{plugin:'output',function:'kiteRender',
                    details:{filename:IDE.files[file_uuid].path}})
            } else {
                //---CMD
                output = window.electron_api.sendSync('plugin',{plugin:'output',function:'processSync',
                    details:run_cmd.slice(8)})
            }
            
            // New
            pg_uuid = IDE.RightWindow.addPage({iframe:1,close:()=>{return OutputPlugin.closePreview(out_uuid)}})
            OutputPlugin.processes[out_uuid].pg_uuid = pg_uuid
            
            let base = document.createElement('base')
            base.href = 'file://'+IDE.files[file_uuid].path
            IDE.RightWindow.pages[pg_uuid].element.contentWindow.document.head.appendChild(base)
            IDE.RightWindow.pages[pg_uuid].element.contentWindow.document.body.innerHTML = output.output
            IDE.RightWindow.showPage(pg_uuid)
            
            return
        }

        let tab_index = OutputPlugin.addOutputTab(fD.name,out_uuid,run_cmd,
            {start_running:start_running, tooltip:fD.path})
        IDE.BottomTab.selectTab(tab_index)
    },
    
    addConsole: function(name,run_cmd,options) {
        // Add a console output
        if (options === undefined) {options = {}}
        let out_uuid = $P.uuid(0)
        OutputPlugin.processes[out_uuid]={status:0, name:name,type:'console',history:[],history_pointer:0}
        OutputPlugin.addOutputTab(name,out_uuid,run_cmd,options)
        return out_uuid
    },
    
    addOutputTab: function(tab_name,out_uuid,run_cmd,options){
        // Add the output tab to UI
        if (options === undefined){options = {}}
        
        let icon = 'plugins/output/prompt.svg'
        if ('icon' in options) {
            icon = options.icon
        }
        let close_f = `OutputPlugin.closeTab('${out_uuid}',event)`
        if ('close' in options) {
            close_f = options.close
        }
        
        // Create Output Page/Window
        let out_tab = IDE.BottomTab.addTab({
            title:tab_name,
            tab_class:'output-tab',
            tooltip:options.tooltip,
            icon:icon,
            close:close_f,
        })
        $P.attr(out_tab.tab,'data-out-uuid',out_uuid)
        let tab_index = out_tab.index
        let output_elm = $P.create({
            h:`.output-page`, 'data-out-uuid':out_uuid, c:[
                {h:`#d_output_${tab_index}_buttons.output_buttons flex-col`, c:[
                    {h:`#b_output${tab_index}_run.btn-run btn-img btn-clear -margin-l`, title:'run/stop',
                        onclick:`OutputPlugin.toggleRun('${out_uuid}')`, c:[{h:'img',src:'icons/run.svg'}]},
                    {h:'.flex'},
                    {h:`#b_output${tab_index}_menu.btn-img btn-clear -margin-l`, title:'view menu', style:'margin-top:10px;',
                        onclick:`$P.menu.show('output${tab_index}_menu')`, c:[{h:'img',src:'icons/menu.svg'}]},
                    // {h:`#b_output2${tab_index}_run.btn-run-ext btn-img btn-clear -margin-l`,  title:'run external',
                        // onclick:`OutputPlugin.runExternal('${out_uuid}')`, c:[{h:'img',src:'plugins/launchers/console.svg'}]},
                ]},
                {h:'.flex-col flex', c:[
                    {h:'.output-cmd-row flex-row-c', c:[
                        {h:`input#output${tab_index}_cmd.flex`, placeholder:'command', value:run_cmd},
                        {h:`#output${tab_index}_start.output-time-text`},
                        {h:`#output${tab_index}_end.output-time-text`},
                    ]},
                    {h:`#output${tab_index}_console.output-console`, oncontextmenu:`$P.menu.show('output${tab_index}_menu')`},
                    {h:'.flex-row-c output-input hidden', c:[
                        {text:'&rsaquo;'},
                        {h:`input#output${tab_index}_input.flex`,'type':'text',onkeydown:`OutputPlugin.inputKeyDown(event,'${out_uuid}')`},
                    ]},
                ]},
                // Context Menu
                {h:`#output${tab_index}_menu.menu-container`, onclick:'$P.menu.close(this)', c:[
                    {h:'.menu-item', text:'Clear', onclick:`$P.clear('output${tab_index}_console')`},
                    {h:'.menu-separator'},
                    {h:'.menu-item', text:'Copy', onclick:"$P.copyToClipboard(window.getSelection().toString())"},
                    {h:'.menu-item', text:'Copy All Text', onclick:`$P.copyToClipboard($P.elm('output${tab_index}_console').innerText)`},
                    {h:'.menu-separator'},
                    {h:'.menu-item', text:'Show/Hide Command', onclick:`$P.toggle($P.parent('output${tab_index}_cmd'))`},
                    {h:'.menu-item', text:'Show/Hide Input', onclick:`$P.toggle($P.parent('output${tab_index}_input'))`},
                    {h:'.menu-item', text:'Show/Hide Buttons', onclick:`$P.toggle('d_output_${tab_index}_buttons')`},
                ]},
            ],parent:out_tab.page})
        
        OutputPlugin.processes[out_uuid].count = tab_index
        
        if (options.start_running) {
            OutputPlugin.toggleRun(out_uuid)
        }
        return tab_index
    },
    
    //---Output Tab Functions
    input: function(out_uuid,cmd) {
        if (OutputPlugin.processes[out_uuid].status == 0) {
            IDE.setStatusBar('process is not running','warning')
        } else {
            let tab_index = OutputPlugin.processes[out_uuid].count
            $P.val(`output${tab_index}_input`,'')
            
            if (cmd == 'clear') {
                OutputPlugin.clearOutput(out_uuid)
            } else {
                
                // Add Output Text
                if (OutputPlugin.processes[out_uuid].type == 'console') {
                    // Replace input prompt in output
                    let out_elm = $P.elm(`output${tab_index}_console`)
                    if (out_elm.lastChild) {
                        let last_txt = out_elm.lastChild.innerText
                        if (last_txt == '>>> ') {
                            out_elm.removeChild(out_elm.lastChild)
                        } else if (last_txt.endsWith('\n> ')) {
                            out_elm.lastChild.innerText = out_elm.lastChild.innerText.slice(0,-3)
                        }
                    }
                }
                OutputPlugin.addOutputText(out_uuid,cmd,'intext')
                
                window.electron_api.send('plugin',{plugin:'output',function:'processWrite',
                    details:{uuid:out_uuid,data:cmd}})
            }
            
            
            // Add to command history
            if (OutputPlugin.processes[out_uuid].history.includes(cmd)) {
                // Remove duplicate command
                $P.array.remove(OutputPlugin.processes[out_uuid].history,cmd)
            }
            if (cmd != 'clear') {
                OutputPlugin.processes[out_uuid].history.push(cmd)
                OutputPlugin.processes[out_uuid].history_pointer = OutputPlugin.processes[out_uuid].history.length
            }
        }
    },
    
    inputKeyDown:function(event, out_uuid) {
        let tab_index = OutputPlugin.processes[out_uuid].count
        let keycode = event.keyCode
        if (keycode == 13) {
            // Return
            OutputPlugin.input(out_uuid,$P.val(`output${tab_index}_input`))
        } else if (keycode == 38) {
            // Up Arrow
            event.preventDefault()
            let txt = $P.val(`output${tab_index}_input`)
            OutputPlugin.historyCommand(out_uuid,-1,txt)
        } else if (keycode == 40) {
            // Down Arrow
            event.preventDefault()
            let txt = $P.val(`output${tab_index}_input`)
            OutputPlugin.historyCommand(out_uuid,1,txt)
        }
        
    },
    
    historyCommand: function(out_uuid,increment,cmd_txt) {
        let history = OutputPlugin.processes[out_uuid].history
        OutputPlugin.processes[out_uuid].history_pointer += increment
        let hptr = OutputPlugin.processes[out_uuid].history_pointer
        let history_txt = ''
        
        // Check history bounds
        if (hptr >= history.length) {
            OutputPlugin.processes[out_uuid].history_pointer = history.length-1
            history_txt = ''
        } else if (hptr < 0) {
            OutputPlugin.processes[out_uuid].history_pointer = 0
            history_txt = history[0]
        } else {
            history_txt = history[hptr]
        }
        $P.val(`output${OutputPlugin.processes[out_uuid].count}_input`,history_txt)
        // let strlength = history_txt.length*2
        // $P.elm(`output${OutputPlugin.processes[out_uuid].count}_input`).setSelectionRange(strlength, strlength);
        $P.elm(`output${OutputPlugin.processes[out_uuid].count}_input`).focus()
        
    },
    
    toggleRun:function(out_uuid) {
        // Get Command
        let cnt = OutputPlugin.processes[out_uuid].count
        let run_cmd = $P.val(`output${cnt}_cmd`)
        
        if (run_cmd == '') {
            run_cmd = OutputPlugin.processes[out_uuid].dflt_run_cmd
            $P.val(`output${cnt}_cmd`,run_cmd)
        }
        
        let stat = OutputPlugin.processes[out_uuid].status
        // let run_btn = $P.query(`#bt_page_${cnt} .btn-run img`)[0]
        // console.log('togglerun',cnt,stat)
        // if (stat != 1) {
        if (stat == 0) {
            // Run (previously stopped)
            OutputPlugin.clearOutput(out_uuid)
            OutputPlugin.processes[out_uuid].run_status = 1
            
            // Save Run Command for file (if different)
            let file_uuid = OutputPlugin.processes[out_uuid].file_uuid
            if (file_uuid in IDE.files) {
                IDE.files[file_uuid].settings.run_cmd = undefined
                if (OutputPlugin.processes[out_uuid].dflt_run_cmd != run_cmd) {
                    IDE.files[file_uuid].settings.run_cmd = run_cmd
                }
            }
            
            // Start Time
            let d = new Date()
            let ts = d.toLocaleTimeString().slice(0,-3)+(d.getMilliseconds()/1000).toString().slice(1,4)
            $P.html(`output${cnt}_start`,'Started: '+ts)

        } else {
            // Stop
            OutputPlugin.processes[out_uuid].run_status = 0
        }
        
        let nstat = window.electron_api.sendSync('plugin',{plugin:'output',function:'toggleProcess',
            details:{uuid:out_uuid,cmd:run_cmd,filepath:OutputPlugin.processes[out_uuid].path}})

        OutputPlugin.processes[out_uuid].status = nstat
        OutputPlugin.updateStatus(out_uuid)
    },
    
    stop:function(uuid) {
        // THIS FUNCTION IS NOT USED
        window.electron_api.send('plugin',{plugin:'output',function:'stopProcess',details:{uuid:uuid}})
    },
    
    runExternal:function(out_uuid) {
        
    },
    
    updateStatus: function(out_uuid) {
        let cnt = OutputPlugin.processes[out_uuid].count
        let run_btn = $P.query1(`#bt_page_${cnt} .btn-run img`)
        
        if (OutputPlugin.processes[out_uuid].status == 1) {
            // Running
            $P.class.add(`bt_tab_${cnt}`,'running')
            run_btn.src = 'icons/stop.svg'
            $P.html(`output${cnt}_end`,'')
        } else {
            // Stopped
            let d = new Date()
            let ts = d.toLocaleTimeString().slice(0,-3)+(d.getMilliseconds()/1000).toString().slice(1,4)
            $P.html(`output${cnt}_end`,'Finished: '+ts)
            run_btn.src = 'icons/run.svg'
            $P.class.remove(`bt_tab_${cnt}`,'running')
        }
    },
    
    
    update: function(resp) {
        let mode = resp.mode
        if (mode == 'stdout') {
            // Write stdout
            OutputPlugin.addOutputText(resp.uuid,resp.data)
        } else if (mode == 'stderr') {
            // Error
            
            // Update any file paths with links to open:
            let txt = resp.data
            
            // if (OutputPlugin.processes[resp.uuid].type == 'console' && (txt == '>>> ' || txt == '> ')) {
            //     return
            // }
            
            if (OutputPlugin.processes[resp.uuid].mode == 'py') {
                txt = txt.replace(/File "([^"]*)", line (\d+)/g,'File <span class="output-filelink" onclick="IDE.openFile(String.raw`$1`, $2)">$1</span>, line $2')
            } else if (OutputPlugin.processes[resp.uuid].mode == 'js') {
                txt = txt.replace(/\(([^:]*):(\d+):(\d+)\)/g,'(<span class="output-filelink" onclick="IDE.openFile(String.raw`$1`, $2)">$1</span>:$2:$3)')
            }
            
            OutputPlugin.addOutputText(resp.uuid,txt,'error')
        } else if (mode == 'close') {
            // Process is stoped
            if (resp.uuid in OutputPlugin.processes) {
                OutputPlugin.processes[resp.uuid].status = 0
                OutputPlugin.updateStatus(resp.uuid)
            }
        }
    },
    
    clearOutput: function(uuid) {
        $P.html(`output${OutputPlugin.processes[uuid].count}_console`,'')
    },
    

    
    addOutputText: function(uuid,txt,mode) {
        if (uuid in OutputPlugin.processes) {
            
            // Return if process is stopped
            if (OutputPlugin.processes[uuid].run_status == 0) {return}
            
            let newline = document.createElement('div')
            if (mode == 'error') {
                newline.innerHTML = txt
            } else {
                // newline.innerText = $P.sanitize(txt,'html')
                newline.innerText = txt
            }
            if (mode === undefined) {mode = 'outtext'}
            newline.className = mode
            
            let out_elm = $P.elm(`output${OutputPlugin.processes[uuid].count}_console`)
            out_elm.appendChild(newline)
            
            // added scroll timeout
            if (!OutputPlugin.processes[uuid].scroll_timeout) {
                OutputPlugin.processes[uuid].scroll_timeout = setTimeout(()=>{
                    out_elm.scrollTo(0,out_elm.scrollHeight)
                    OutputPlugin.processes[uuid].scroll_timeout = undefined
                },10)
            }
            
            
            // newline.scrollIntoView()
            // out_elm.scrollTop = out_elm.scrollHeight
            
        }
    },

    closeTab: function(out_uuid, event) {
        
        if (event) {
            event.stopPropagation()
        }
        
        //---Close Preview Page
        if (OutputPlugin.processes[out_uuid].type.startsWith('preview')) {
            IDE.RightWindow.closePage(OutputPlugin.processes[out_uuid].pg_uuid)
            return
        }
        
        // Stop Process
        if (OutputPlugin.processes[out_uuid].status == 1) {
            OutputPlugin.stop(out_uuid)
        }
        $P.elm('b_btm_hide').click()
        
        // Remove objects
        let file_id = ''+OutputPlugin.processes[out_uuid].file_uuid
        let cnt = OutputPlugin.processes[out_uuid].count
        delete OutputPlugin.processes[out_uuid]
        delete OutputPlugin.files[file_id]
        
        // Remove Page and Tab
        IDE.BottomTab.closeTab(cnt)
    },
    
    //---Preview Right Window
    updatePreview: function(out_uuid) {
        // Update the preview page
        
        let pg_uuid = OutputPlugin.processes[out_uuid].pg_uuid
        if (OutputPlugin.processes[out_uuid].type == 'preview cmd') {
            // Recompile and update
            // let output = window.electron_api.sendSync('plugin',{plugin:'output',function:'processSync',
            //     details:OutputPlugin.processes[out_uuid].dflt_run_cmd.slice(8)})
            
            let fD = IDE.files[OutputPlugin.processes[out_uuid].file_uuid]
            
            let output
            if (fD.mode == 'md') {
                //---Markdown
                output = window.electron_api.sendSync('plugin',{plugin:'output',function:'markdownRender',
                    details:{filename:fD.path}})
            } else {
                //---CMD
                output = window.electron_api.sendSync('plugin',{plugin:'output',function:'processSync',
                    details:OutputPlugin.processes[out_uuid].dflt_run_cmd.slice(8)})
            }

            // $P.html(IDE.RightWindow.pages[pg_uuid].element,output.output)
            // IDE.RightWindow.pages[pg_uuid].element.contentWindow.document.write(output.output)
            // IDE.RightWindow.pages[pg_uuid].element.contentWindow.location.reload(true)
            IDE.RightWindow.pages[pg_uuid].element.contentWindow.document.body.innerHTML = output.output
        } else {
            // Refresh the iframe
            IDE.RightWindow.pages[pg_uuid].element.contentWindow.location.reload(true)
        }
        IDE.RightWindow.showPage(pg_uuid)
    },

    closePreview: function(out_uuid) {
        // Remove objects
        let file_id = ''+OutputPlugin.processes[out_uuid].file_uuid
        delete OutputPlugin.processes[out_uuid]
        delete OutputPlugin.files[file_id]
        return 1
    },
    
    //---Events
    IDETabChangedEvent: function (event) {
        // Select bottom output on editor tab change
        let file_uuid = event.detail.uuid
        if (file_uuid in OutputPlugin.files) {
            let out_uuid = OutputPlugin.files[file_uuid]
            if (OutputPlugin.processes[out_uuid].type.startsWith('preview')) {
                if (!$P.class.has('right_window','hidden')) {
                    IDE.RightWindow.showPage(OutputPlugin.processes[out_uuid].pg_uuid)
                }
            } else if (!$P.class.has('bottom_pages','hidden')) {
                IDE.BottomTab.selectTab(OutputPlugin.processes[out_uuid].count)
            }
        }
        
    },
    
    editorClosedEvent: function (event) {
        // close output on editor tab change
        let file_uuid = event.detail.uuid
        if (file_uuid in OutputPlugin.files) {
            let ok = 1
            let out_uuid = OutputPlugin.files[file_uuid]
            
            // Ask to Close if running
            if (OutputPlugin.processes[out_uuid].status ==1) {
                ok = 0
                $P.dialog.message({text:'Do you want to close the output, <br>'+OutputPlugin.processes[out_uuid].name,
                    btns:['yes','no']
                }).then(resp =>{
                    if (resp == 'yes') {
                        OutputPlugin.closeTab(out_uuid)
                    }
                })
            }
            
            if (ok) {
                OutputPlugin.closeTab(out_uuid)
            }
        }
    },
    
    IDEClosedEvent: function(event) {
        for (proc_uuid in OutputPlugin.processes) {
            let pD = OutputPlugin.processes[proc_uuid]
            if (pD.status == 1) {
                OutputPlugin.closeTab(proc_uuid)
            }
        }
    }
    
}

function createOutput() {
    // startup output function
    $P.create({h:'.space',parent:'sidebar_plugin_buttons'})
    
    // Add Run Button
    let run_btn = $P.create({h:'button#b_run.btn-clear', onclick:"OutputPlugin.run()",
        title:"run file (F5)", c:[{h:'img',src:'icons/run.svg'}],
        parent:'sidebar_plugin_buttons'
    })
    
    // Add Compile Button
    let compile_btn = $P.create({h:'button#b_compile.btn-clear', onclick:"OutputPlugin.compile()",
        title:"compile file (F6)", c:[{h:'img',src:'icons/compile.svg'}],
        parent:'sidebar_plugin_buttons'
    })
    
    
    // Set IDE Run Command
    IDE.runCommand = OutputPlugin.run
    IDE.compileCommand = OutputPlugin.compile
    
    // Listen for Tab close
    window.addEventListener('tabClosed', OutputPlugin.editorClosedEvent)
    window.addEventListener('tabChanged', OutputPlugin.IDETabChangedEvent)
    window.addEventListener('ideExit', OutputPlugin.IDEClosedEvent)
    
    // Register output plugin
    IDE.plugins['output'] = OutputPlugin
    
}


createOutput()
const path = require('path')
const fs = require('fs')

//---o:
let Plugin = {
    js:['output_ui.js'],
    css:['output.css'],
    settings:{
        // python_path:'python3',
        // electron_path:require('electron').app.getPath("exe"),
    },
}

function load(IDE) {
    OutputProc.IDE = IDE

    // Platform Settings
    if (OutputProc.IDE.platform == "win32") {
        OutputProc.detached = false
    }
    
    return OutputProc
}


//---o:
let OutputProc = {
    detached:true,
    processes:{},
    
    toggleProcess: function(details) {
        let uuid = details.uuid
        let command = details.cmd
        
        if (uuid in OutputProc.processes) {
            let proc = OutputProc.processes[uuid].process
            // Check if Running
            if (OutputProc.processes[uuid].status) {
                // Still Running
                // if (OutputProc.IDE.platform == "win32") {
                    OutputProc.processes[uuid].process.kill('SIGTERM')
                // } else {
                if (OutputProc.IDE.platform != "win32") {
                    require('process').kill(-OutputProc.processes[uuid].process.pid, 'SIGINT')
                }
                return 2
            }
        }
        
        // Start New Process
        OutputProc.newProcess(uuid,command,details.filepath)
        return 1
    },
    
    stopProcess: function(details) {
        if (details.uuid in OutputProc.processes && OutputProc.processes[details.uuid].process) {
            // if (OutputProc.IDE.platform == "win32") {
                OutputProc.processes[details.uuid].process.kill('SIGTERM')
            // } else {
            if (OutputProc.IDE.platform == "win32") {
                require('process').kill(-OutputProc.processes[details.uuid].process.pid, 'SIGINT')
            }
            return 2
        }
    },
    
    splitCommand: function(command){
        let splt_cmds = command.match( /[^"\s]+|"(?:\\"|[^"])*"/g )
        for (let i in splt_cmds) {
            if (splt_cmds[i].startsWith('"') && splt_cmds[i].endsWith('"')) {
                splt_cmds[i] = splt_cmds[i].slice(1,-1)
            }
        }
        
        return splt_cmds

    },
    
    newProcess: function(uuid,command,filepath) {
        let win = OutputProc.IDE.win
        
        // Split command into args
        let command_split = OutputProc.splitCommand(command)
        let cmd = command_split[0]
        let args = []
        if (command_split.length > 1) {
            args = command_split.slice(1)
        }

        // Check for preview
        if (cmd == 'preview') {
            //....
            //<iframe sandbox="allow-scripts allow-popups">
        }
        
        // Set cwd to path
        let cwd = undefined
        if (filepath) {
            cwd = path.dirname(filepath)
        }
        
        proc = require('child_process').spawn(cmd,args,{
          shell: false, detached: OutputProc.detached, 
          cwd:cwd
        })
        
        OutputProc.processes[uuid] = {process:proc,status:1}
        let data_format = 'utf8'
        
        if (OutputProc.IDE.platform == "win32") {
            data_format = 'binary'
        }
        
        // Process Connectors
        proc.stdout.on('data',function(data){
            win.webContents.send("plugin", {plugin:'output', function:'update', details:{uuid:uuid, mode:'stdout', data:data.toString(data_format)}})
        })
        proc.stderr.on('data',function(data){
            win.webContents.send("plugin", {plugin:'output', function:'update', details:{uuid:uuid, mode:'stderr', data:data.toString(data_format)}})
        })
        proc.on('error', (error) => {
            win.webContents.send("plugin", {plugin:'output', function:'update', details:{uuid:uuid, mode:'stderr', data:error.message}})
        })
        proc.on("close", code => {
            OutputProc.processes[uuid].status = 0
            win.webContents.send("plugin", {plugin:'output', function:'update', details:{uuid:uuid, mode:'close'}})
        })
    },

    processWrite: function(resp) {
        if (resp.uuid in OutputProc.processes && OutputProc.processes[resp.uuid].status ==1) {
            let proc = OutputProc.processes[resp.uuid].process
            proc.stdin.write(resp.data, 'utf8')
            proc.stdin.write('\n')
        }
    },
    
    processSync: function(command) {
        // Parse command and args
        let command_split = OutputProc.splitCommand(command)
        let cmd = command_split[0]
        let args = []
        if (command_split.length > 1) {
            args = command_split.slice(1)
        }

        proc = require('child_process').spawnSync(cmd,args,{
          shell: false, detached: OutputProc.detached, encoding : 'utf8' 
        })
        return {'output':proc.stdout,'status':proc.status}
    },
    
    markdownRender: function(details) {
        let md_txt = fs.readFileSync(details.filename,'utf8')
        let cmarkd_path = path.join(OutputProc.IDE.path,'plugins','cmarkd','cmarkd.js')
        let cmarkd = require(cmarkd_path)
        let html = cmarkd.cmarkdDocument(md_txt)
        return {'output':html, 'status':1}
    },

    kiteRender: function(details) {
        let kite_text = fs.readFileSync(details.filename,'utf8')
        let kite_path = path.join(OutputProc.IDE.path,'plugins','cmarkd','kite.js')
        let kite = require(kite_path)
        let html = kite.KiteParser(kite_text).toHTML({use_style:1})
        return {'output':html, 'status':1}
    },

}

module.exports = {load,Plugin}
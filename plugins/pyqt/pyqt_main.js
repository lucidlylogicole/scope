const path = require('path')

//---o:
let Plugin = {
    js:['pyqt_ui.js'], // ui javascript to load into app
    css:[],  // css files to load into app
    settings:{
        'qt_versions':[{'name':'Qt5',
            'designer_path':'/usr/lib/x86_64-linux-gnu/qt5/bin/designer',
            'pyuic_cmd':'{{python}} -m PyQt5.uic.pyuic',
            'help_url':'https://doc.qt.io/qt-5/{{search}}.html',
        }]
    },
}

function load(IDE) {
    PyQt.IDE = IDE
    return PyQt
}


//---o:
let PyQt = {
    convertFile: function(details) {
        try {
            let bname = path.basename(details.filename)
            let cmd = PyQt.IDE.settings.plugins['pyqt'].qt_versions[details.qt_index].pyuic_cmd.replace('{{python}}',PyQt.IDE.settings.paths.python)+ ' "' + bname + '" > "'+bname.slice(0,-3) + '_ui.py"'
            proc = require('child_process').execSync(cmd, {cwd:path.dirname(details.filename)})
            PyQt.IDE.win.webContents.send("status", {text:'ui file converted!'})
        } catch(e) {
            PyQt.IDE.win.webContents.send("status", {text:'ui file NOT converted!', mode:'warning'})
            PyQt.IDE.win.webContents.send("errorLog", {error:e})
        }

    },
    
    launchDesigner: function(details) {
        try {
            let opts = {}
            if (details.filename) {
                opts.cwd=path.dirname(details.filename)
            }
            require('child_process').exec(PyQt.IDE.settings.plugins['pyqt'].qt_versions[details.qt_index].designer_path,opts)
        } catch(e) {
            PyQt.IDE.win.webContents.send("status", {text:'Could not launch Qt Designer.<br>Check the path in settings', mode:'warning'})
            PyQt.IDE.win.webContents.send("errorLog", {error:e})
        }
    },
    
}

module.exports = {load,Plugin}
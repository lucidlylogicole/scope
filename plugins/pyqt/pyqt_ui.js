
//---o:
let PyQtPlugin = {
    // UI Template object
    
    getCurrentFile: function() {
        let filename = ''
        let cur_file = IDE.currentFile()
        if (cur_file) {
            filename = cur_file.path
        }
        return filename
    },
    
    browseFile: function() {
        // Browse Files
        let resp = window.electron_api.sendSync('browseFile',{filename:PyQtPlugin.getCurrentFile(),
            filters:[{ name: 'Qt UI', extensions: ['ui'] },]})
        $P.val('le_pyqt_convert_filename',resp[0].filename)
        // Set filename to lineedit
    },
    
    launchDesigner: function() {
        window.electron_api.sendSync('plugin',{plugin:'pyqt',function:'launchDesigner',
            details:{filename:PyQtPlugin.getCurrentFile(), qt_index:$P.val('cb_qt_vers')}
        })
    },
    
    convert: function() {
        window.electron_api.sendSync('plugin',{plugin:'pyqt',function:'convertFile',
            details:{filename:$P.val('le_pyqt_convert_filename'), qt_index:$P.val('cb_qt_vers')}
        })
   
    },
    
    viewHelp: function(event) {
        if (event.key == 'Enter') {
            let txt = $P.val('le_pyqt_help_search').toLowerCase()
            let qv = $P.val('cb_qt_vers')
            if (!txt.startsWith('q')) {txt = 'q'+txt}
            window.open(IDE.settings.plugins['pyqt'].qt_versions[qv].help_url.replace('{{search}}',txt),'_blank')
        }
    },
    
    focusHelp: function(event) {
        IDE.BottomTab.selectTab(PyQtPlugin.tab_index)
        $P.elm('le_pyqt_help_search').focus()
        $P.elm('le_pyqt_help_search').select()
    },
    
}

function createPlugin() {
    // UI Plugin Startup Code
    
    // Add Bottom Window Plugin
    let pyqt_elm = {
        h:'#pyqt_page.flex-col flex', c:[
            {h:'.flex-row-c', c:[
                {h:'.text pad',text:'Qt .ui to Python Converter'},
                {h:'select#cb_qt_vers.scope-input margin-l margin-r',c:[]},
                {h:'button.scope-btn',text:'Qt Designer',onclick:"PyQtPlugin.launchDesigner()"},
                {h:'.flex'},
                {h:'input#le_pyqt_help_search.scope-input', placeholder:'view qwidget help', 
                    title:'enter the name of a widget and press return to view Qt Help', onkeydown:'PyQtPlugin.viewHelp(event)'}
            ]},
            {h:'.flex-row-c', c:[
                {h:'button.btn-img btn-clear pad', onclick:"PyQtPlugin.browseFile()", c:[{h:'img',src:'icons/file_open.svg'}]},
                {h:'input#le_pyqt_convert_filename.scope-input flex', placeholder:'<- select .ui file'},
            ]},
            {h:'.flex-row-c', c:[
                {style:'width:40px;'},
                {h:'button.scope-btn margin-l-lg',text:'convert',onclick:"PyQtPlugin.convert()"},
                {h:'.flex'},
            ]},
            
        ],
        
        style2:`display:flex;
            flex:1;
            background:rgb(30,30,30);
            padding:12px;
            color:rgb(150,150,150);
            border:0px;
            outline:none;
            resize: none;
            `
    }

    // Load Versions
    let qi=-1
    for (let qvers of IDE.settings.plugins['pyqt'].qt_versions) {
        qi++
        pyqt_elm.c[0].c[1].c.push({h:'option', text:qvers.name,value:qi})
    }

    let plugin_tabD = IDE.BottomTab.addTab({
        tooltip:'PyQt Converter',
        icon:'plugins/pyqt/qt.svg',
        close:0,
        page:pyqt_elm,
    })
    
    PyQtPlugin.tab_index = plugin_tabD.index
    
    IDE.keymap.add('q',PyQtPlugin.focusHelp,{ctrl:1}) 
    
    IDE.plugins['pyqt'] = PyQtPlugin
    
}

createPlugin()
function createPlugin() {
    IDE.addTab({
        filename:'',
        name:'Home',
        icon:'plugins/home/home.svg',
        // tabname:'Tab Test 2',
        type:'app',
        src:'plugins/home/home.html',
        ext:'',
        closeable:0,
    }).then(uuid=>{
        
        // Add IDE to iframe namespace
        IDE.files[uuid].element.contentWindow.IDE = IDE
        
        $P.html($P.query1(IDE.files[uuid].element.contentWindow.document.body,'#home_version'),IDE.VERSION)
        
        // Load Workspaces
        let wkspc_elm = $P.query1(IDE.files[uuid].element.contentWindow.document.body,'#workspaces')
        window.addEventListener('workspaceLoaded',function(){
            IDE.files[uuid].element.contentWindow.loadWorkspace()
        })
        IDE.files[uuid].element.contentWindow.loadWorkspace()
        
        // Move to beginning tab
        // $P.elm('tabbar').insertBefore($P.query1(`.tab[data-uuid="${uuid}"]`),$P.elm('tabbar').firstChild)
        
    })
    
}

createPlugin()
// Check for nodejs
let kite_is_nodejs = typeof process !== 'undefined' && process.versions != null && process.versions.node != null

if (kite_is_nodejs) {
    let cmarkd = require('./cmarkd.js')
    cmarkdRender = cmarkd.cmarkdRender
}

function KiteParser(text) {

//---o:
let KiteParser = {
    VERSION:'2.0.1',
    doc:{},
    markdownParser:undefined,
    src:'',
    
    parse: function(text) {
        // Parse text source into a document object
        this.src = text
        let doc = []
        let title = ''
        let cur = {
            header:-1,
            section:-1,
            option:-1,
            object:undefined,
        }
        
        let line_cnt = 0
        
        for (let line of text.split('\n')) {
            line_cnt++
            let l_line_trim = line.trimStart()          // left trim of line
            let l_st = line.length - l_line_trim.length // start of text
            
            // if (l_line_trim.startsWith('#') && cur.object === undefined) {
                // ---Comment
            // } else if (l_line_trim.startsWith('title>')) {
            if (l_line_trim.startsWith('title>')) {
        //---Title
                title = l_line_trim.slice(6).trim()
            } else if (l_line_trim.startsWith('h>')) {
        //---Header
                hdr = line.slice(l_st+2).trim()
                cur.header++
                doc.push({title:hdr,sections:[]})
                // Reset current parts
                cur.section = -1
                cur.option = -1
                cur.object = undefined
            } else if (l_line_trim.startsWith('md>')) {
        //---Markdown Section

                // Check if there is a current header (there must be)
                if (cur.header == -1) {
                    console.error("Header (h>) must be specified before markdown (md>): line "+line_cnt)
                }
                
                cur.section++
                cur.option = -1
                doc[cur.header].sections.push({markdown:line.slice(l_st+3)})
                cur.object = 'md>'
                cur.line_start = l_st
            } else if (l_line_trim.startsWith('c>')) {
        //---Command
                
                // Check if there is a current header (there must be)
                if (cur.header == -1) {
                    console.error("Header (h>) must be specified before command (c>): line "+line_cnt)
                }
                
                cmd = l_line_trim.slice(2).trim()
                cur.section++
                cur.option = -1
                doc[cur.header].sections.push({cmd:cmd})
                cur.object = 'md>'
            } else if (l_line_trim.startsWith('ce>')) {
        //---Command Extra content (not bold)
                
                // Check if there is a current command (there must be)
                if (cur.section == -1) {
                    console.error("Command (c>) must be specified before Command Extra Content (ce>): line "+line_cnt)
                }
                
                doc[cur.header]['sections'][cur.section]['extra'] = l_line_trim.slice(3).trim()
                
            } else if (l_line_trim.startsWith('d>') || l_line_trim.startsWith('dm>')) {
        //---Command Description
                
                // Check if there is a current command (there must be)
                if (cur.section == -1) {
                    console.error("Command (c>) must be specified before Description (d>): line "+line_cnt)
                    return
                }
                
                doc[cur.header]['sections'][cur.section]['description'] = line.slice(l_st+2).trim()
                
                // Markdown Description
                if (l_line_trim.startsWith('dm>')) {
                    doc[cur.header]['sections'][cur.section]['md'] = 1
                    doc[cur.header]['sections'][cur.section]['description'] = line.slice(l_st+3)
                }
                cur.object = 'd>'
                cur.line_start = l_st
            } else if (l_line_trim.startsWith('n>') || l_line_trim.startsWith('nm>')) {
        //---Notes
                
                // Check if there is a current command (there must be)
                if (cur.section == -1) {
                    console.error("Command (c>) must be specified before Notes (n>): line "+line_cnt)
                }
                
                doc[cur.header]['sections'][cur.section]['notes'] = line.slice(l_st+2).trim()
                
                // Markdown Description
                if (l_line_trim.startsWith('nm>')) {
                    doc[cur.header]['sections'][cur.section]['md'] = 1
                    doc[cur.header]['sections'][cur.section]['notes'] = line.slice(l_st+3).trim()
                }
                cur.object = 'n>'
                cur.line_start = l_st
            } else if (l_line_trim.startsWith('ex>')) {
        //---Example
                
                // Check if there is a current command (there must be)
                if (cur.section == -1) {console.error("Command (c>) must be specified before Example (ex>): line "+line_cnt)}
                
                if (doc[cur.header]['sections'][cur.section].examples === undefined){doc[cur.header]['sections'][cur.section].examples=[]}
                doc[cur.header]['sections'][cur.section]['examples'].push(line.slice(l_st+3).trimStart())
                
                cur.object = 'ex>'
                cur.line_start = l_st
            } else if (l_line_trim.startsWith('o>')) {
        //---Option
                
                // Check if there is a current command (there must be)
                if (cur.section == -1) {console.error("Command (c>) must be specified before Option (o>): line "+line_cnt)}
                
                cur.option++
                if (doc[cur.header]['sections'][cur.section].options === undefined){doc[cur.header]['sections'][cur.section].options=[]}
                doc[cur.header]['sections'][cur.section]['options'].push({'option':line.slice(l_st+2).trim(),'description':''})
                cur.object = 'o>'
                cur.line_start = l_st
            } else if (l_line_trim.startsWith('od>') || l_line_trim.startsWith('odm>')) {
        //---Option Description
                
                // Check if there is a current command (there must be)
                if (cur.option == -1) {console.error("Option (o>) must be specified before Option Description (od>): line "+line_cnt)}
                
                doc[cur.header]['sections'][cur.section]['options'][cur.option].description = line.slice(l_st+3)
                if (l_line_trim.startsWith('odm>')) {
                    doc[cur.header]['sections'][cur.section]['options'][cur.option]['md'] = 1
                    doc[cur.header]['sections'][cur.section]['options'][cur.option].description = line.slice(l_st+4)
                }
                cur.object = 'od>'
                cur.line_start = l_st
            } else if (cur.object !== undefined) {
        //---Append to objects
                // Command not closed, so append to last object
                let l_min_st = Math.min(l_st,cur.line_start) // find min starting place (don't start after current line)
                if (cur.object == 'd>') {
                    doc[cur.header]['sections'][cur.section]['description'] += '\n'+line.slice(l_min_st).trimEnd()
                } else if (cur.object == 'n>') {
                    doc[cur.header]['sections'][cur.section]['notes'] += '\n'+line.slice(l_min_st).trimEnd()
                } else if (cur.object == 'ex>') {
                    let ex_len = doc[cur.header]['sections'][cur.section]['examples'].length
                    doc[cur.header]['sections'][cur.section]['examples'][ex_len-1]+= '\n'+line.slice(l_min_st).trimEnd()
                } else if (cur.object == 'od>') {
                    doc[cur.header]['sections'][cur.section]['options'][cur.option]['description']+= '\n'+line.slice(l_min_st).trimEnd()
                } else if (cur.object == 'md>') {
                    doc[cur.header]['sections'][cur.section]['markdown']+= '\n'+line.slice(l_min_st)
                }
                
            }
            
        
        }
        
        // Return document object
        this.doc = {'title':title,'sections':doc}
        return this.doc
        
    },
    
    //---f:
    stylesheet:`
body {
    font-family:ubuntu,dejavu sans,calibre;
}
.section {
    border:1px solid rgb(220,220,220);
    padding:10px;
    margin-top:8px;
    border-radius:3px;
}
.section h2 {
    margin-top:2px;
    margin-bottom:2px;
}
.cmd-sec {
    margin-top:8px;
}
.desc {
    font-size:0.9rem;
    opacity:0.8;
}

.notes {
    margin:4px 8px;
    font-size:0.9rem;
    opacity:0.8;
}
pre {
    border:1px solid rgb(220,220,220);
    border-radius:3px;
    padding:5px;
    background:rgb(245,245,245);
    margin:2px;
    font-size:0.8em;
    overflow-x: auto;
}
`,
    
    toHTML: function(options) {
        // Parse text if provided, otherwise use 
        if (options === undefined){options ={}}
        if (options.text !== undefined) {
            this.parse(options.text)
        }
        
        // Convert doc object to html
        let html = `<h1>${this.doc.title}</h1>`
        
        for (let contD of this.doc.sections) {
            html += `\n<div class="section">`
            html += `\n<h2>${contD.title}</h2>`
            
            // Add Section Content
            for (let secD of contD.sections) {
                if (secD.cmd !== undefined) {
                    // Command
                    html += `\n<div class="cmd-sec">\n    <b class="cmd">${secD.cmd}</b>`
                    // Command Extra
                    if (secD.extra !== undefined) {
                        html += ` <i class="extra">${secD.extra}</i>`
                    }
                    // Description
                    if (secD.description !== undefined) {
                        let desc = secD.description.replaceAll('\n','<br>').replaceAll('  ',' &nbsp;')
                        if (secD.md) {
                            desc = this.renderMarkdown(secD.description)
                        }
                        html += ` <span class="desc">${desc}</span>`
                    }
                    // Options
                    if (secD.options !== undefined) {
                        html += `\n  <ul class="options">`
                        for (let opt of secD.options) {
                            let desc = opt.description
                            if (opt.md) {
                                desc = this.renderMarkdown(opt.description)
                             }
                            
                            html += `\n    <li><i class="opt">${opt.option}</i> &nbsp;${desc}</li>`
                        }
                        html += `\n  </ul>`
                    }
                    // Examples
                    if (secD.examples !== undefined) {
                        for (let ex of secD.examples) {
                            html += `\n  <pre><code>${ex}</code></pre>`
                        }
                    }
                    // Notes
                    if (secD.notes !== undefined) {
                        html += '\n'
                        let desc = secD.notes.replaceAll('\n','<br>').replaceAll('  ',' &nbsp;')
                        if (secD.md) {
                            desc = this.renderMarkdown(secD.notes)
                        }
                        html += `<div class="notes">${desc}</div>`
                    }
                    
                    html += '\n</div>'
                } else if (secD.markdown !== undefined) {
                    // Markdown Section
                    html += `\n<div class="cmd-sec">\n`+this.renderMarkdown(secD.markdown)+'\n</div>'
                }
            }
            
            html += '</div>'
        }
        
        // Add Stylesheet
        if (options.use_style) {
            html = `<style>${this.stylesheet}</style>`+html
        }
        
        return html
        
    },
    
    renderMarkdown: function(txt) {
        if (cmarkdRender) {
            let md = cmarkdRender(txt.trim()).trim()
            // hack to remove <p>
            if (md.startsWith('<p>') && md.endsWith('</p>') ) {
                md = md.slice(3,md.length-4)
            }
            return md
        } else {
            return txt
        }
    },

}

if (text !== undefined) {
    KiteParser.parse(text)
}

return KiteParser

}

//---Nodejs
if (kite_is_nodejs) {
    let args = require('process').argv
    if (require.main === module && args.length > 2 && !args.slice(-1)[0].endsWith('.js')) {
        let fs = require('fs')
        let path = require('path')
        let ch_file = args[2]
        
        // Check for file argument
        let write_file
        let file_index = args.indexOf('-f')
        if (file_index > -1) {
            if (file_index < args.length-1 && args[file_index+1] !='-d' ) {
                write_file = args[file_index+1]
            } else {
                write_file = path.join(path.dirname(ch_file),path.basename(ch_file).split('.')[0]+'.html')
            }
        }
        
        // Parse and write to output
        let file_text = fs.readFileSync(ch_file,'utf8')
        let cp_text = KiteParser(file_text).toHTML({use_style:1})
        if (write_file) {
            // render a file
            fs.writeFileSync(write_file,cp_text)
        } else {
            // render text
            console.log(cp_text)
        }
    }
    // Module Exports
    module.exports.KiteParser = KiteParser
}
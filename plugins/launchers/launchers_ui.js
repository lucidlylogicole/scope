function createLauncher() {
    
    //---Launcher Buttons
    $P.create({h:'.space',parent:'sidebar_plugin_buttons'})
    
    // Add Folder Button
    let folder_btn = $P.create({h:'button#b_folder.btn-clear', onclick:"LaunchersPlugin.launchFolder()",
        title:"open file manager in current directory", c:[{h:'img',src:'plugins/launchers/file_manager.svg'}],
        parent:'sidebar_plugin_buttons'
    })

    // Add Console Button
    let console_btn = $P.create({h:'button#b_console_launcher.btn-clear', onclick:"$P.menu.show('console_launcher_menu',{loc:'side',align:'right',anchor:'b_console_launcher'})",
        title:"open a console", c:[{h:'img',src:'plugins/launchers/console.svg'}],
        parent:'sidebar_plugin_buttons'
    })

    // Add Git Button
    if (IDE.settings.plugins['launchers']['git_cmd'] != '') {
        let git_gui_btn = $P.create({h:'button#b_git_gui.btn-clear', onclick:"LaunchersPlugin.launchGitGui()",
            title:"open Git Gui", c:[{h:'img',src:'plugins/launchers/git_gui.svg'}],
            parent:'sidebar_plugin_buttons'
        })
    }
    $P.create({h:'.space',parent:'sidebar_plugin_buttons'})
    
    //---Launcher Menu
    let menu_items = [{h:'.menu-item', text:'External Console',onclick:"LaunchersPlugin.launchExternalConsole()"}]
    // Load consoles from settings
    if (IDE.settings.plugins['launchers'].consoles) {
        for (let cslD of IDE.settings.plugins['launchers'].consoles) {
            let uuid = $P.uuid()
            menu_items.push({h:'.menu-item', text:cslD.title, onclick:`LaunchersPlugin.launchConsole('${uuid}')`})
            LaunchersPlugin.consoles[uuid] = cslD
        }
    }
    $P.create({h:'#console_launcher_menu.menu-container', onclick:"$P.menu.close('console_launcher_menu')", 
        c:menu_items,parent:document.body})
    
    
    //---Python Console
    // Add a default python console
    let py_cmd = IDE.settings.paths.python
    if (py_cmd) {
        let py_out_uuid = OutputPlugin.addConsole('',py_cmd+' -i',{tooltip:'python console',icon:'plugins/launchers/python.svg',close:0})
        $P.show($P.query(`.output-page[data-out-uuid="${py_out_uuid}"] .output-input`))
        
        let py_console_running = 0
        OutputPlugin.processes[py_out_uuid].mode = 'python'
        
        // Add onclick
        $P.query1(`.output-tab[data-out-uuid="${py_out_uuid}"]`).addEventListener('click', evt=>{
            // Start python if not running
            if (!py_console_running) {
                py_console_running=1
                OutputPlugin.toggleRun(py_out_uuid)
                
                // Hide Stuff
                let py_pg_elm = $P.query1(`.output-page[data-out-uuid="${py_out_uuid}"]`)
                $P.hide($P.query(py_pg_elm,'.output_buttons'))
                $P.hide($P.query(py_pg_elm,'.output-cmd-row'))
            }
            let tab_index = OutputPlugin.processes[py_out_uuid].count
            $P.elm(`output${tab_index}_input`).focus()
        })
    }

}

//---o:
let LaunchersPlugin = {
    consoles:{},
    
    getCurrentPath: function() {
        // Get Current Path
        let filename = null
        let uuid = IDE.currentTab()
        if (uuid && uuid in IDE.files && IDE.files[uuid].type == 'editor' && IDE.files[uuid].path!== undefined) {
            filename = IDE.getDirName(IDE.files[uuid].path)
        } else {
            filename = IDE.CWD
        }
        return filename
    },
    
    launchFolder: function() {
        window.electron_api.send('plugin',{plugin:'launchers',function:'folder',
            details:LaunchersPlugin.getCurrentPath()})
    },

    launchGitGui: function() {
        window.electron_api.send('plugin',{plugin:'launchers',function:'git_gui',
            details:LaunchersPlugin.getCurrentPath()})
    },

    launchExternalConsole: function() {
        let pth = LaunchersPlugin.getCurrentPath()
        if (IDE.Workspace.current !== undefined){
            pth = IDE.Workspace.current.path
        }
        window.electron_api.send('plugin',{plugin:'launchers',function:'console',
            details:pth})
    },
    
    // launchSettings: function() {
    //     let resp = window.electron_api.sendSync('settingsFile')
    //     IDE.addEditor(resp.path,resp.text).then(uuid => {
    //         IDE.files[uuid].is_settings = 1
    //     })
    // },
    
    launchConsole: function(uuid) {
        let cD = LaunchersPlugin.consoles[uuid]
        LaunchersPlugin.addConsole(cD)
    },
    
    addConsole: function(opt) {
        // Options
        let optD = {
            title:'console',
            icon:'plugins/launchers/console.svg',
            cmd:'',
            autoStart:0,
            mode:'',
            closable:1,
            showTitle:1,
            showInput:1,
        }
        for (let ky in opt) {
            optD[ky] = opt[ky]
        }
        
        let title = ''
        if (optD.showTitle) {
            title = optD.title
        }

        // Parse Run Command
        let run_cmd = optD.cmd
        if (run_cmd.includes('{{filename}}')) {
            run_cmd = $P.replaceAll(run_cmd,'{{filename}}','"'+LaunchersPlugin.getCurrentPath()+'"')
        }
        if (run_cmd.includes('{{scope}}')) {
            run_cmd = $P.replaceAll(run_cmd,'{{scope}}',IDE.path)
        }
        // Replace any path settings
        for (let spth in IDE.settings.paths) {
            if (run_cmd.includes('{{'+spth+'}}')) {
                run_cmd = $P.replaceAll(run_cmd,'{{'+spth+'}}','"'+IDE.settings.paths[spth]+'"')
            }
        }
        // if (run_cmd.includes('{{electron}}')) {
        //     run_cmd = $P.replaceAll(run_cmd,'{{electron}}',IDE.settings.plugins['output'].electron_path)
        // }
        // Ex: LaunchersPlugin.addConsole({cmd:'node -i {{filename}}'})
        
        // Add Console Page
        let out_uuid = OutputPlugin.addConsole(title,run_cmd,{tooltip:optD.title,icon:optD.icon})
        if (optD.showInput) {
            $P.show($P.query(`.output-page[data-out-uuid="${out_uuid}"] .output-input`))
        }
        OutputPlugin.processes[out_uuid].mode = optD.mode
        OutputPlugin.processes[out_uuid].type = 'console'

        // Autostart
        if (optD.autoStart) {
            OutputPlugin.toggleRun(out_uuid)
            let pg_elm = $P.query1(`.output-page[data-out-uuid="${out_uuid}"]`)
            $P.hide($P.query(pg_elm,'.output-cmd-row'))
        }
        // Add onclick
        let tab_index = OutputPlugin.processes[out_uuid].count
        $P.query1(`.output-tab[data-out-uuid="${out_uuid}"]`).addEventListener('click', evt=>{
            $P.elm(`output${tab_index}_input`).focus()
        })
        IDE.BottomTab.selectTab(tab_index)
        $P.elm(`output${tab_index}_input`).focus()
    },

}

createLauncher()
const {app, shell} = require('electron')
const node_path = require('path')

//---o:
let Launcher = {
    
    getPath: function(path,directory) {
        if (!path) {
            path = Launcher.IDE.lastPath
        }
        if (path && directory) {
            path = path
        } else if (path) {
            path = node_path.dirname(path)
        }
        if (!path) {
            path = app.getPath('home')
        }
        return path
    },
    
    file: function(filename) {
        shell.openExternal(filename)
    },
    
    folder: function(path) {
        shell.showItemInFolder(path)
    },
    console: function(path) {
        
        let terminal = 'gnome-terminal'
        if (process.platform == 'win32') {
            terminal = 'cmd.exe'
        }
        
        if ('launchers' in this.IDE.settings.plugins) {
            if ('external_console' in this.IDE.settings.plugins['launchers'] && this.IDE.settings.plugins['launchers'].external_console != 'default') {
                terminal = this.IDE.settings.plugins['launchers']['external_console']
            }
        }
        
        proc = require('child_process').spawn(terminal,[],{
          shell: true, detached: true, 
          cwd:this.getPath(path,true),
        })
    },

    git_gui: function(path) {
        
        let git_cmd = 'git'

        if ('launchers' in this.IDE.settings.plugins) {
            if ('git_cmd' in this.IDE.settings.plugins['launchers']) {
                git_cmd = this.IDE.settings.plugins['launchers']['git_cmd']
            }
        }
        
        proc = require('child_process').spawn(git_cmd,['gui'],{
          shell: false, detached: true, 
          cwd:this.getPath(path,true),
        })
    },

}

//---o:
let Plugin = {
    js:['launchers_ui.js'],
    css:[],
    settings:{
        git_cmd:'git',
        external_console:'default',
        consoles:[
            {title:'Python HTTP Server',cmd:'{{python}} -u -m http.server 8000 --bind 127.0.0.1 --directory {{filename}}',mode:'python',"showInput":0},
            {title:'NodeJS',cmd:'{{node}} -i',icon:'plugins/launchers/nodejs.svg',showTitle:0,autoStart:1,mode:'javascript'},
            {title:'Python',cmd:'{{python}} -i',icon:'plugins/launchers/python.svg',showTitle:0,autoStart:1,mode:'python'},
            {title:'Blank Console',cmd:'',icon:'plugins/launchers/console.svg',showTitle:0},
        ],
    }
}

function load(IDE) {
    Launcher.IDE = IDE
    return Launcher
}

module.exports = {load,Plugin}
const fs = require('fs')

//---o:
let Plugin = {
    js:['snippets_ui.js'], // ui javascript to load into app
    css:[],  // css files to load into app
    settings:{
        path:''
    },
}

function load(IDE) {
    Snippets.IDE = IDE
    return Snippets
}


//---o:
let Snippets = {

    readFile: function(filename) {
        let text = 'not found'
        if (fs.existsSync(filename)) {
            text = fs.readFileSync(filename,'utf8')
        }
        return text
    }
    
}

module.exports = {load,Plugin}

//---o:
let Snippets = {

    loadDirectory: function(){
        let pth = IDE.settings.plugins['snippets']['path']
        if (pth != '') {
            let dirD = window.electron_api.sendSync('getDirInfo',{path:pth,hidden:1})
            
            $P.clear('snippets_files')
            let elms = []
            for (let file of dirD.files) {
                elms.push({h:'.fb-file ext-'+file.ext,text:file.name, onclick:`Snippets.viewCode('${file.name}')`})
            }
            $P.create(elms,{parent:'snippets_files'})
        }
    },
    
    viewCode: function(filename) {
        let filepth = IDE.settings.plugins['snippets']['path']+IDE.path_delim+filename
        let text = window.electron_api.sendSync('plugin',{plugin:'snippets',function:'readFile',
            details:filepth
        })
        $P.val('snippets_code',text)
    },
    
    edit: function(filename) {
        if (filename === undefined) {
            filename = Snippets.MENU_ITEM
        }
        if (filename) {
            let filepth = IDE.settings.plugins['snippets']['path']+IDE.path_delim+filename
            IDE.openFile(filepth)
        }
    },
    
    openDirectory: function() {
        window.electron_api.send('plugin',{plugin:'filebrowser',function:'openExternal',details:IDE.settings.plugins['snippets']['path']})
    },
    
    newSnippet: function() {
        resp = window.electron_api.sendSync('newFile',{filename:IDE.settings.plugins['snippets']['path'],text:''})
        if (resp.ok) {
            IDE.addEditor(resp.new_filename,'')
            Snippets.loadDirectory()
        }
    },
    
    search: function() {
        let srch_val = $P.val('i_snippet_search').toLowerCase()
        for (let elm of $P.query('#snippets_files .fb-file')) {
            if (elm.innerText.toLowerCase().indexOf(srch_val)> -1 ) {
                $P.show(elm)
            } else {
                $P.hide(elm)
            }
        }
    }
    
}

function createPlugin() {
    // UI Plugin Startup Code
    IDE.plugins['snippets'] = Snippets
    
    // Create Snippets HTML Elements
    let snippets_elm = {
        h:'#snippets_page',
        style:`display:flex;flex:1;font-size:0.8rem`,
        c:[
            {h:'flex-col',c:[
                {h:'input#i_snippet_search.scope-input margin-lg',placeholder:'search snippets',type:'text',onkeyup:"Snippets.search()"}
            ]},
            {h:'#snippets_files.show-color show_icons scroll-y',style:'min-width:250px;'},
            {h:'textarea#snippets_code.scope-input flex',placeholder:'code', readonly:'readonly'},
            
            // Snippets Context Menu
            {h:'#menu_snippets.menu-container', onclick:"$P.menu.close('menu_snippets')", c:[
                {h:'.menu-item',text:'Edit', onclick:'Snippets.edit()'},
                {h:'.menu-separator'},
                {h:'.menu-item',text:'Reload', onclick:'Snippets.loadDirectory()'},
                {h:'.menu-item',text:'Open Folder', onclick:'Snippets.openDirectory()'},
                {h:'.menu-separator'},
                {h:'.menu-item',text:'New Snippet', onclick:'Snippets.newSnippet()'},
            ], parent:'snippets_page'},

        ]
    }

    IDE.BottomTab.addTab({
        tooltip:'snippets',
        icon:'plugins/snippets/snippets.svg',
        close:0,
        page:snippets_elm,
    })

    // Snippets Menu Event
    $P.elm('snippets_files').addEventListener('contextmenu', event => {
        event.preventDefault()
        Snippets.MENU_ITEM = event.target.innerText
        $P.menu.show('menu_snippets')
    })
    
    // Load the Directory
    Snippets.loadDirectory()
    
}

createPlugin()
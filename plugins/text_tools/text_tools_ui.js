
//---o:
let TextToolsPlugin = {
    
    view: function() {
        IDE.BottomTab.selectTab(TextToolsPlugin.tab_index)
        $P.elm('i_find_replace_find').focus()
    },
    
    //---Find / Replace
    getInputs: function() {
        return {
            f_txt: $P.val('i_find_replace_find'),
            r_txt: $P.val('i_find_replace_replace'),
            cs: $P.val('ckbx_find_replace_casesensitive'),
            ww: $P.val('ckbx_find_replace_wholeword'),
            re: $P.val('ckbx_find_replace_regex'),
        }
    },
    
    find: function() {
        let iD = TextToolsPlugin.getInputs()
        IDE.currentEditor().find(iD)
    },
    
    replace: function() {
        let iD = TextToolsPlugin.getInputs()
        IDE.currentEditor().replace(iD)
    },
    
    replaceAll: function() {
        let iD = TextToolsPlugin.getInputs()
        IDE.currentEditor().replaceAll(iD)
    },
    
    replaceAllNewLines: function() {
        let cur_editor = IDE.currentEditor()
        let txt = cur_editor.getText()
        let f_txt = $P.val('i_find_replace_find')
        let r_txt = $P.val('i_find_replace_replace')
        if (f_txt.includes('\\n')){f_txt = f_txt.replaceAll('\\n','\n')}
        if (r_txt.includes('\\n')){r_txt = r_txt.replaceAll('\\n','\n')}
        let ntxt = txt.replaceAll(f_txt,r_txt)
        cur_editor.replaceText(ntxt)
        // cur_editor.ace_editor.selectAll()
        // cur_editor.replaceSelectedText(ntxt)
        // cur_editor.setText(ntxt,1)
    },
    
    //---Utils
    prettyJSON: function() {
        let cur_editor = IDE.currentEditor()
        
        if (cur_editor && cur_editor.mode == 'json') {
            $P.dialog.input({text:'number of spaces to indent?',val:2}).then(indent=>{
                if (indent !== undefined) {
                    cur_editor.replaceText(JSON.stringify(JSON.parse(cur_editor.getText()),null,parseInt(indent)))
                }
                this.ace_editor.focus()
            })
        }
    },
    
}

//---
//---o:
let SpellCheckPlugin = {

    show: function() {
        
        let editor = IDE.currentEditor()
        if (!editor) {
            IDE.setStatusBar('The current file cannot be spellchecked','warning')
            return
        }
        
        let txt = IDE.currentEditor().getText()
        
        $P.val('ta_spellcheck',txt)
        $P.dialog.show('dlg_spellcheck')
        $P.elm('ta_spellcheck').setSelectionRange(0,0);
        $P.elm('ta_spellcheck').focus()
    },
    
    applyChanges: function() {
        let editor = IDE.currentEditor()
        if (editor) {
            let txt = $P.val('ta_spellcheck')
            // editor.setText(txt, true)
            editor.replaceText(txt)
        }
    },

}

//---
function createPlugin() {

    //---Find Files Element
    let find_elm = {
        h:'.flex-row flex', c:[
            {h:'.flex'},
            // Find / Replace
            {h:'.pad',c:[
                {h:'.flex-row-c text-sm',c:[
                    {h:'.pad align-r',text:'Search for', style:'width:120px'},
                    {h:'input#i_find_replace_find.scope-input'},
                ]},
                {h:'.flex-row-c text-sm',c:[
                    {h:'.pad align-r',text:'Replace With', style:'width:120px'},
                    {h:'input#i_find_replace_replace.scope-input'},
                ]},
                {h:'.flex-row2 align-r',c:[
                    {h:'button.scope-btn',text:'Find', onclick:'TextToolsPlugin.find()'},
                    {h:'button.scope-btn margin-sm',text:'Replace', onclick:'TextToolsPlugin.replace()'},
                    {h:'button.scope-btn',text:'Replace All', onclick:'TextToolsPlugin.replaceAll()'},
                    
                ]},
            ]},
            {h:'.pad border-r scope-border',c:[
                {h:'.flex-row-c',c:[
                    {h:'input#ckbx_find_replace_casesensitive.scope-input', type:'checkbox'},
                    {h:'label.flex text-sm',text:'Case Sensitive', 'for':'ckbx_find_replace_casesensitive'},
                ]},
                {h:'.flex-row-c',c:[
                    {h:'input#ckbx_find_replace_wholeword.scope-input', type:'checkbox'},
                    {h:'label.flex text-sm',text:'Whole Word', 'for':'ckbx_find_replace_wholeword'},
                ]},
                {h:'.flex-row-c',c:[
                    {h:'input#ckbx_find_replace_regex.scope-input', type:'checkbox'},
                    {h:'label.flex text-sm',text:'Reg Exp', 'for':'ckbx_find_replace_regex'},
                ]},
                // {h:'.flex-row-c',title:'replace all newline',c:[
                //     {h:'input#ckbx_find_replace_newline.scope-input', type:'checkbox'},
                //     {h:'label.flex text-sm',text:'New Line (all)', 'for':'ckbx_find_replace_newline'},
                // ]},
                {h:'.spacer'},
                {h:'button.scope-btn',text:'Replace New Lines', title:'Replace All New Lines', onclick:'TextToolsPlugin.replaceAllNewLines()'},
            ]},
            
            //---Other Tool buttons
            // {h:'.pad border-r scope-border',c:[
            {h:'#text_tools_more_container.pad',c:[
                {h:'button.scope-btn flex-row-c', onclick:'SpellCheckPlugin.show()', c:[
                    {h:'img', style:'height:1.1rem', src:'plugins/text_tools/spellcheck.svg'},
                    {h:'.pad-l', text:'Spellcheck'},
                ]},
                {h:'.margin-sm-t'},
                {h:'button.scope-btn',text:'File Stats', onclick:'IDE.fileStats()'},
                {h:'.margin-sm-t'},
                {h:'button.scope-btn',text:'JSON Prettify', onclick:'TextToolsPlugin.prettyJSON()'},
                
            ]},
                
            // Space
            {h:'.flex'},
        ]
    }

    //---Spellcheck Element
    $P.create({h:'#dlg_spellcheck.dlg-bg flex-row', 'data-display':'flex', c:[
        {h:'#frm_spellcheck.dlg-frame flex-col flex', c:[
            {h:'h3.margin-n pad',text:'Spellcheck'},
            {h:'textarea#ta_spellcheck.flex scope-input'},
            {h:'.flex-row-c pad', c:[
                {h:'button.scope-btn', text:'apply changes', onclick:"SpellCheckPlugin.applyChanges();$P.dialog.close('dlg_spellcheck')"},
                {h:'button.scope-btn', text:'cancel', onclick:"$P.val('ta_spellcheck','');$P.dialog.close('dlg_spellcheck')"},
            ]},
        ], style:"margin:auto;height:90vh;"},
    ],parent:document.body})


    // Add Button to Toolbar
    // let ff_btn = $P.create({h:'button#b_spellcheck.btn-clear', onclick:"SpellCheckPlugin.show()",
    //     title:"spellcheck", c:[{h:'img',src:'plugins/spellcheck/spellcheck.svg'}],
    //     parent:'sidebar_plugin_buttons'
    // })

    TextToolsPlugin.tab_index = IDE.BottomTab.addTab({
        tooltip:'Text tools and find / replace',
        icon:'plugins/text_tools/text_tools.svg',
        close:0,
        page:find_elm,
    }).index
    
    // Add Key Shortcut
    IDE.keymap.add('R',TextToolsPlugin.view,{ctrl:1, shift:1})
    IDE.keymap.add('F',TextToolsPlugin.view,{ctrl:1, shift:1})
    // console.log(IDE.keymap)
    
    IDE.plugins['text_tools'] = TextToolsPlugin
    
}

createPlugin()
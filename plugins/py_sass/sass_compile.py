import sass, sys, os

##style = 'compressed'
##if '-n' in sys.argv:
##    style='expanded'

style='expanded'
if '-c' in sys.argv:
    style='compressed'

css = sass.compile(filename=sys.argv[1],output_style=style)

if 'print_html' in sys.argv:
    # print('<div style="white-space:pre;">'+css+'</div>')
    print(css)
else:
    if len(sys.argv) > 2 and sys.argv[2] != '-c':
        newfile = sys.argv[2]
    else:
        newfile = os.path.splitext(sys.argv[1])[0]+'.css'

    with open(newfile,'w') as f:
        f.write(css)


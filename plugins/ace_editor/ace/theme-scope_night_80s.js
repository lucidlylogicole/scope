ace.define("ace/theme/scope_night_80s",["require","exports","module","ace/lib/dom"], function(require, exports, module) {

exports.isDark = true;
exports.cssClass = "ace-scope-night-80s";
exports.cssText = ".ace-scope-night-80s .ace_gutter {\
background: rgb(26,26,26);\
color: rgb(150,150,150);\
}\
.ace-scope-night-80s .ace_print-margin {\
width: 1px;\
background: #272727\
}\
.ace-scope-night-80s {\
background-color: #141414;\
color: #CCCCCC\
}\
.ace-scope-night-80s .ace_constant.ace_other,\
.ace-scope-night-80s .ace_cursor {\
color: #CCCCCC\
}\
.ace-scope-night-80s .ace_marker-layer .ace_selection {\
background: #515151\
}\
.ace-scope-night-80s.ace_multiselect .ace_selection.ace_start {\
box-shadow: 0 0 3px 0px #2D2D2D;\
}\
.ace-scope-night-80s .ace_marker-layer .ace_step {\
background: rgb(102, 82, 0)\
}\
.ace-scope-night-80s .ace_marker-layer .ace_bracket {\
margin: -1px 0 0 -1px;\
border: 1px solid #6A6A6A\
}\
.ace-tomorrow-night-bright .ace_stack {\
background: rgb(66, 90, 44)\
}\
.ace-scope-night-80s .ace_marker-layer .ace_active-line {\
background: #1d242a\
}\
.ace-scope-night-80s .ace_gutter-active-line {\
background-color: #212930\
}\
.ace-scope-night-80s .ace_marker-layer .ace_selected-word {\
border: 1px solid #515151\
}\
.ace-scope-night-80s .ace_invisible {\
color: #6A6A6A\
}\
.ace-scope-night-80s .ace_keyword,\
.ace-scope-night-80s .ace_meta,\
.ace-scope-night-80s .ace_storage,\
.ace-scope-night-80s .ace_storage.ace_type,\
.ace-scope-night-80s .ace_support.ace_type {\
color: #CC99CC\
}\
.ace-scope-night-80s .ace_keyword.ace_operator {\
color: #66CCCC\
}\
.ace-scope-night-80s .ace_constant.ace_character,\
.ace-scope-night-80s .ace_constant.ace_language,\
.ace-scope-night-80s .ace_constant.ace_numeric,\
.ace-scope-night-80s .ace_keyword.ace_other.ace_unit,\
.ace-scope-night-80s .ace_support.ace_constant,\
.ace-scope-night-80s .ace_variable.ace_parameter {\
color: #F99157\
}\
.ace-scope-night-80s .ace_list {\
color:#b5e0ff\
}\
.ace-scope-night-80s .ace_invalid {\
color: #CDCDCD;\
background-color: #F2777A\
}\
.ace-scope-night-80s .ace_invalid.ace_deprecated {\
color: #CDCDCD;\
background-color: #CC99CC\
}\
.ace-scope-night-80s .ace_fold {\
background-color: #6699CC;\
border-color: #CCCCCC\
}\
.ace-scope-night-80s .ace_entity.ace_name.ace_function,\
.ace-scope-night-80s .ace_support.ace_function,\
.ace-scope-night-80s .ace_variable {\
color: #6699CC\
}\
.ace-scope-night-80s .ace_support.ace_class,\
.ace-scope-night-80s .ace_support.ace_type {\
color: #FFCC66\
}\
.ace-scope-night-80s .ace_heading,\
.ace-scope-night-80s .ace_markup.ace_heading {\
color: #f1a777\
}\
.ace-scope-night-80s .ace_string {\
color: #99CC99\
}\
.ace-scope-night-80s .ace_comment {\
color: #999999\
}\
.ace-scope-night-80s .ace_entity.ace_name.ace_tag,\
.ace-scope-night-80s .ace_entity.ace_other.ace_attribute-name,\
.ace-scope-night-80s .ace_meta.ace_tag,\
.ace-scope-night-80s .ace_variable {\
color: #F2777A\
}\
.ace-scope-night-80s .ace_indent-guide {\
background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAAEklEQVQImWPQ09NrYAgMjP4PAAtGAwchHMyAAAAAAElFTkSuQmCC) right repeat-y\
}";

var dom = require("../lib/dom");
dom.importCssString(exports.cssText, exports.cssClass);
});                (function() {
                    ace.require(["ace/theme/tomorrow_night_eighties"], function(m) {
                        if (typeof module == "object" && typeof exports == "object" && module) {
                            module.exports = m;
                        }
                    });
                })();
            
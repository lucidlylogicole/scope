ace.define("ace/theme/scope",["require","exports","module","ace/lib/dom"], function(require, exports, module) {

exports.isDark = true;
exports.cssClass = "ace-scope";
exports.cssText = `.ace-scope .ace_gutter {
    background: rgb(26,26,26);
    color: rgb(150,150,150);
}
.ace-scope .ace_print-margin {
    width: 1px;
    background: #232323;
}
.ace-scope {
    background-color: rgb(20,20,20);
    color: #F8F8F8;
}
.ace-scope .ace_cursor {
    color: #A7A7A7;
}
.ace-scope .ace_marker-layer .ace_selection {
    background: rgba(221, 240, 255, 0.20);
}
.ace-scope.ace_multiselect .ace_selection.ace_start {
    box-shadow: 0 0 3px 0px #141414;
}
.ace-scope .ace_marker-layer .ace_step {
    background: rgb(113,115,0);
}
.ace-scope .ace_marker-layer .ace_bracket {
    margin: -1px 0 0 -1px;
    border: 1px solid rgba(255, 255, 255, 0.25);
}
.ace-scope .ace_marker-layer .ace_active-line {
    background: rgba(105,184,221,0.06);
}
.ace-scope .ace_gutter-active-line {
    background-color: rgba(255, 255, 255, 0.031);
}
.ace-scope .ace_marker-layer .ace_selected-word {
    border: 1px solid rgba(221, 240, 255, 0.20);
}
.ace-scope .ace_invisible {
    color: rgba(255, 255, 255, 0.25);
}
.ace-scope .ace_keyword,
.ace-scope .ace_meta {
    color: #CDA869;
}
.ace-scope .ace_constant,
.ace-scope .ace_constant.ace_character,
.ace-scope .ace_constant.ace_character.ace_escape,
.ace-scope .ace_constant.ace_other,
.ace-scope .ace_heading,
.ace-scope .ace_markup.ace_heading,
.ace-scope .ace_support.ace_constant {
    color: #CF6A4C;
}
.ace-scope .ace_invalid.ace_illegal {
    color: #F8F8F8;
    background-color: rgba(86, 45, 86, 0.75)
}
.ace-scope .ace_invalid.ace_deprecated {
    text-decoration: underline;
    font-style: italic;
    color: #D2A8A1;
}
.ace-scope .ace_support {
    color: #9B859D;
}
.ace-scope .ace_fold {
    background-color: #AC885B;
    border-color: #F8F8F8;
}
.ace-scope .ace_support.ace_function {
    color: #DAD085;
}
.ace-scope .ace_list,
.ace-scope .ace_markup.ace_list,
.ace-scope .ace_storage {
    color: #F9EE98;
}
.ace-scope .ace_entity.ace_name.ace_function,
.ace-scope .ace_meta.ace_tag,
.ace-scope .ace_variable {
    color: #AC885B;
}
.ace-scope .ace_string {
    color: #99CC99;
}
.ace-scope .ace_string.ace_regexp {
    color: #E9C062;
}
.ace-scope .ace_comment {
    font-style: italic;
    color: #6d6c54;
}
.ace-scope .ace_variable {
    color: #6699CC;
}
.ace-scope .ace_xml-pe {
    color: #494949;
}
.ace-scope .ace_indent-guide {
    background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAAEklEQVQImWMQERFpYLC1tf0PAAgOAnPnhxyiAAAAAElFTkSuQmCC) right repeat-y
};`

var dom = require("../lib/dom");
dom.importCssString(exports.cssText, exports.cssClass);
});(function() {
    ace.require(["ace/theme/scope"], function(m) {
        if (typeof module == "object" && typeof exports == "object" && module) {
            module.exports = m;
        }
    });
})();


class AceEditor {
    //---o:
    default_settings = {
        element:'editor',
        theme:'scope',
        mode:'text',
        options:{
            wrap:false,
            behavioursEnabled:1,
            wrapBehavioursEnabled:1,
            newLineMode:'unix',
            showInvisibles:0,
            fontSize:14,
            showPrintMargin:0,
            dragEnabled:true,
            fontFamily:'DejaVu Sans Mono,Consolas,Courier',
            enableSnippets:true,
            enableBasicAutocompletion:true,
            scrollPastEnd:0.5,  // overscroll
            autoScrollEditorIntoView: true,
            // fixedWidthGutter:true,
            // indentedSoftWrap: true,
            // wrapLimit:'none',  // use wrap:80 instead
            // showFoldWidgets:false,
        }
    }

    constructor(options) {
        
        this.modelist = ace.require("ace/ext/modelist")
        
        // Load options and defaults
        let opt = this.default_settings
        if (options === undefined) {
            options = {}
        }
        for (ky in options) {
            opt[ky] = options[ky]
        }
        
        // Setup Editor
        this.ace_editor = ace.edit(opt.element)
        this.element = document.getElementById(opt.element)
        let ace_editor = this.ace_editor
        
        ace_editor.getSession().on('change',this.textChanged.bind(this))
        let CodeEditor = this
        
        // Turn off semi colon check
        ace_editor.session.on('changeMode', function(e, session){
            CodeEditor.mode = session.getMode().$id.split('/').slice(-1)[0]
            if (CodeEditor.mode == 'javascript') {
                ace_editor.session.$worker.send("changeOptions", [{asi: true}])
            } else if (CodeEditor.mode == 'python') {
                $P.show('menu_item_decorators')
            }
        })
        
        // this.mode = opt.mode
        ace_editor.$blockScrolling = Infinity
        
        // this.editor.getSession().on('changeScrollTop',function (e) {
        //     window.visibleLinesChanged([editor.getFirstVisibleRow(),editor.getLastVisibleRow()])
        // })
        
        this.PREV_TEXT = ''
        this.CHANGED = 0
        this.WRITER = 0
        this.WORDWRAP = 0
        // this.loadOptions(options)
        this.loadKeyboardCommands()
        
    }
    
    //---f:
    load(txt,mode,settings) {
        
        this.settings = settings
        this.setMode(mode)
        this.loadOptions(this.settings)
        
        // Set Filename Info
        if (txt !== undefined) {
            this.setText(txt)
            this.outline.update()
            this.CHANGED=0
        }
        
        document.getElementById('sidepane').style['flex-basis'] = settings.outline.width
    }
    
    //---f:
    loadOptions(settings) {
        let opt = this.default_settings.options
        if (!settings) {
            settings = {}
        }
        for (let ky in settings.options) {
            opt[ky] = settings.options[ky]
        }
        let theme = this.default_settings.theme
        if (settings.theme) {theme = settings.theme}
        this.ace_editor.setTheme('ace/theme/'+theme)
        
        this.ace_editor.setOptions(opt)
        
        // Wrap
        if ('wrap' in opt) {
            this.WORDWRAP = opt['wrap']
        }
        // Writer Mode
        if (settings.writerMode) {
            this.writerMode(1)
        }
        
        // Hide Outline if not in mode
        if (!(CodeEditor.mode in OutlineParser) || !settings.outline.show) {
            // $P.toggle('sidepane',0)
            $P.elm('sidepane').style.display='none'
        }
        
        // Additional Outline Settings
        OutlineParser.settings.showDecorators = settings.outline.showDecorators
        
    }
    
    //---f:
    loadKeyboardCommands() {
        
        let ace_editor = this.ace_editor
        let CodeEditor = this
        
        //---Duplicate Line
        ace_editor.commands.addCommand({
            name: 'cpydwn',
            bindKey: {win: 'Ctrl-D',  mac: 'Command-D'},
            exec: function(ace_editor) {
                ace_editor.copyLinesDown()
            },
            readOnly: false
        })
        
        //---Delete Line
        ace_editor.commands.addCommand({
            name: 'rmvline',
            bindKey: {win: 'Ctrl-Delete',  mac: 'Command-Delete'},
            exec: function(ace_editor) {
                ace_editor.removeLines()
            },
            readOnly: false
        })
        
        //---Writer Mode Toggle
        ace_editor.commands.addCommand({
            name: 'writer_mode',
            bindKey: {win: 'Alt-Z',  mac: 'Alt-Z'},
            exec: function(ace_editor) {
                CodeEditor.WRITER = ! CodeEditor.WRITER
                CodeEditor.writerMode(CodeEditor.WRITER)
            },
            readOnly: false
        })

        //---Toggle Comment
        ace_editor.commands.addCommand({
            name: 'toggle_comment',
            bindKey: {win: 'Ctrl-E',  mac: 'Command-E'},
            exec: function(ace_editor) {
                ace_editor.toggleCommentLines()
            },
            readOnly: false
        })

        //---Pin for Outline
        ace_editor.commands.addCommand({
            name: 'pin_outline',
            bindKey: {win: 'Alt-P',  mac: 'Alt-P'},
            exec: function(ace_editor) {
                CodeEditor.pinOutline(0)
            },
            readOnly: false
        })

        //---Insert Print
        ace_editor.commands.addCommand({
            name: 'insprint',
            bindKey: {win: 'Ctrl-P',  mac: 'Command-P'},
            exec: function(ace_editor) {
                var txt = ace_editor.getSelectedText()
                var l = 1
                if (CodeEditor.mode == 'python') {
                    CodeEditor.insertText('print(',')')
                } else {
                    CodeEditor.insertText('console.log(',')')
                }
            },
            readOnly: false
        })

        //---Inline Code
        ace_editor.commands.addCommand({
            name: 'inserttilde',
            bindKey: {win: 'Ctrl-`',  mac: 'Command-`'},
            exec: function(ace_editor) {
                CodeEditor.insertText('`','`')
            },
            readOnly: false // false if this command should not apply in readOnly mode
        })
        
        //---Replace
        ace_editor.commands.addCommand({
            name: 'replace2',
            bindKey: {win: 'Ctrl-R',  mac: 'Command-R'},
            exec: function(ace_editor) {
                CodeEditor.ace_editor.execCommand("replace")
            },
            readOnly: false // false if this command should not apply in readOnly mode
        })

        //---Word Wrap
        ace_editor.commands.addCommand({
            name: 'togglewordwrap',
            bindKey: {win: 'Ctrl-W',  mac: 'Command-W'},
            exec: function(ace_editor) {
                CodeEditor.toggleWordWrap()
            },
            readOnly: false // false if this command should not apply in readOnly mode
        })
        ace_editor.commands.addCommand({
            name: 'togglewordwrap2',
            bindKey: {win: 'Alt-W',  mac: 'Alt-W'},
            exec: function(ace_editor) {
                CodeEditor.toggleWordWrap()
            },
            readOnly: false // false if this command should not apply in readOnly mode
        })

        //---Goto Line
        ace_editor.commands.addCommand({
            name: 'gotoline2',
            bindKey: {win: 'Ctrl-G',  mac: 'Command-G'},
            exec: function(ace_editor) {
                CodeEditor.gotoLine()
            },
            readOnly: false
        })

        // ace_editor.commands.addCommand({
        //     name: 'insprint',
        //     bindKey: {win: 'Ctrl-H',  mac: 'Command-H'},
        //     exec: function(ace_editor) {
        //         ace.config.loadModule("ace/ext/keybinding_menu", function(module) {
        //             module.init(ace_editor)
        //             ace_editor.showKeyboardShortcuts()
        //         })
        //     },
        //     readOnly: false
        // })


    //---Markdown Shortcuts
        //---Insert Link
        ace_editor.commands.addCommand({
            name: 'inslink',
            bindKey: {win: 'Ctrl-L',  mac: 'Command-L'},
            exec: function(ace_editor) {
                if (CodeEditor.mode == 'markdown') {
                    var txt = ace_editor.getSelectedText()
                    var l = 1
                    if (txt) {l = 3}
                    CodeEditor.insertText('[',']()',l)
                }
            },
            readOnly: false
        })
    
        //---Bold
        ace_editor.commands.addCommand({
            name: 'boldtext',
            bindKey: {win: 'Ctrl-B',  mac: 'Command-B'},
            exec: function(ace_editor) {
                if (CodeEditor.mode == 'markdown') {
                    CodeEditor.insertText('**','**')
                }
            },
            readOnly: false
        })
    
        //---Italic
        ace_editor.commands.addCommand({
            name: 'italictext',
            bindKey: {win: 'Ctrl-I',  mac: 'Command-I'},
            exec: function(ace_editor) {
                if (CodeEditor.mode == 'markdown') {
                    CodeEditor.insertText('*','*')
                }
            },
            readOnly: false
        })

        // Replace F2 (doesn't work)
        // ace_editor.commands.addCommand({
        //     name: 'notcollapse',
        //     bindKey: {win: 'F2',  mac: 'F2'},
        //     exec: function(ace_editor) {
        //         return false
        //     },
        //     readOnly: false
        // })

        // Toggle Outline (F3)
        ace_editor.commands.addCommand({
            name: 'toggle_outline',
            bindKey: {win: 'F4',  mac: 'F4'},
            exec: function(ace_editor) {
                CodeEditor.toggleSidepane()
            },
            readOnly: false
        })

        //---Override keydown events
        // remove ctrl+h for replace
        let e_repl = ace_editor.commands.byName['replace']
        e_repl.bindKey = {}
        ace_editor.commands.addCommand(e_repl)
        
        //---f:
        document.onkeydown = function(e){
            if ((e.ctrlKey || e.metaKey) && e.keyCode == 82) {
                // Ctrl-R
                CodeEditor.ace_editor.execCommand("replace")
                e.preventDefault()
            } else if ((e.ctrlKey || e.metaKey) && e.keyCode == 72) {
                // Ctrl-H
                console.log('h')
                e.preventDefault()
            } else if ((e.ctrlKey || e.metaKey) && e.keyCode == 83) {
                // Ctrl-S
                e.preventDefault()
            }
        }

    }
    
//---
//---Text Functions
    //---f:
    gotoLine(line) {
        if (line === undefined) {
            $P.dialog.input({text:'go to line'}).then(g=>{
                if (g) {
                    CodeEditor.ace_editor.gotoLine(parseInt(g))
                }
                this.ace_editor.focus()
            })
        } else {
            // this.outline.scroll_update = 0
            this.ace_editor.gotoLine(parseInt(line)+1)
            this.ace_editor.focus()
        }
    }
    //---f:
    getVisibleLines() {
        return this.ace_editor.getFirstVisibleRow()+','+this.ace_editor.getLastVisibleRow()
    }

    //---f:
    getSelectedText() { 
        return this.ace_editor.session.getTextRange(this.ace_editor.getSelectionRange()) 
    }
    
    //---f:
    replaceSelectedText(txt) { 
        this.ace_editor.session.replace(this.ace_editor.getSelectionRange(),txt) 
    }
    
    //---f:
    replaceText(txt) {
        // Replace all the text and allow for undo
        this.ace_editor.selectAll()
        this.replaceSelectedText(txt)
    }
    
    //---f:
    getCurrentPosition() {
        let cur = this.ace_editor.selection.getCursor()
        return [cur.row,cur.column]
    }
    
    //---f:
    getText() {
        return this.ace_editor.getValue()
    }
    
    //---f:
    setText(text, no_update) {
        this.ace_editor.session.setValue(text)
        this.ace_editor.clearSelection()
        this.ace_editor.moveCursorTo(0,0)
        // if (!no_update) {
        this.PREV_TEXT=this.ace_editor.getValue()
        // }
        this.ace_editor.focus()
    }
    
    //---f:
    textChanged() {
        if (!this.CHANGED) {
            this.CHANGED = 1
            var evt = new CustomEvent('textchanged',{detail:{uuid:this.uuid,changed:this.CHANGED}})
            this.element.dispatchEvent(evt)
        }
        // Debounce and compare text to see if really changed
        clearTimeout(this.compare_timeout)
        this.compare_timeout = setTimeout(this.compareText.bind(this),2000)
        
    }
    
    //---f:
    compareText() {
        let txt = this.getText()
        let text_diff = (this.PREV_TEXT != txt)
        if (text_diff != this.CHANGED) {
            this.CHANGED = text_diff
            var evt = new CustomEvent('textchanged',{detail:{uuid:this.uuid,changed:this.CHANGED}})
            this.element.dispatchEvent(evt)
        }
        this.outline.update(txt)
    }
    
    //---f:
    save() {
        var evt = new CustomEvent('saveevent',{detail:this.uuid})
        this.element.dispatchEvent(evt)
    }
    
    //---f:
    insertText(pretext,posttext,cursor_offset) {
        var selrng = this.ace_editor.getSelectionRange()
        var txt = ''
        txt = CodeEditor.getSelectedText()
        if (posttext === undefined){posttext=''}
        this.ace_editor.insert(pretext+txt+posttext)
        if (cursor_offset === undefined) {
            if (txt == '') {
                cursor_offset = pretext.length
            } else {
                cursor_offset = pretext.length+posttext.length
            }
        }
        this.ace_editor.moveCursorTo(selrng.end.row,selrng.end.column+cursor_offset)
    }

//---Other Functions
    //---f:
    toggleSidepane() {
        let dsp = $P.elm('sidepane').style.display
        if (dsp == 'none') {
            // if (window.innerWidth < 900) {
                $P.elm('sidepane').style.display='flex'
            // } else {
                // $P.elm('sidepane').style.display=''
            // }
        } else {
            $P.elm('sidepane').style.display='none'
        }
    }
    //---f:
    toggleWordWrap(ww) {
        if (ww === undefined) {
            this.WORDWRAP = !this.WORDWRAP
        } else {
            this.WORDWRAP = ww
        }
        this.ace_editor.getSession().setUseWrapMode(this.WORDWRAP)
    }
    
    //---f:
    showSettings() {
        ace.config.loadModule("ace/ext/settings_menu", function(module) {
            module.init(this.ace_editor)
            this.ace_editor.showSettingsMenu()
        }.bind(this))
        
    }
    //---f:
    writerMode(ok) {
        if (ok) {
            // Writer Mode
            this.ace_editor.renderer.setShowGutter(false)
            this.ace_editor.getSession().setWrapLimitRange(0,80)
            this.ace_editor.getSession().setUseWrapMode(true)
            this.ace_editor.setFontSize(this.ace_editor.base_font_size+2)
        } else {
            // Unset Writer Mode
            this.ace_editor.getSession().setWrapLimit(undefined)
            this.ace_editor.renderer.setShowGutter(true)
            this.ace_editor.getSession().setUseWrapMode(false)
            this.ace_editor.setFontSize(this.ace_editor.base_font_size)
        }
        this.WRITER = ok
    }

    //---f:
    setGutterMarkers(items) {
        // items = [{
        //   row: 1, column: 0,
        //   text: "Error Message", 
        //   type: "error" // warning, error //This would give a red x on the gutter
        // }]
        this.ace_editor.getSession().setAnnotations(items)
    }

    //---f:
    find(iD) {
        this.ace_editor.find(iD.f_txt,{
            backwards:false,
            wrap:true,
            caseSensitive:iD.cs,
            wholeWord:iD.ww,
            regExp:iD.re,
        })
    }
    
    //---f:
    replace(iD) {
        this.ace_editor.replace(iD.r_txt)
        this.find(iD) // Find Next
    }
    
    //---f:
    replaceAll(iD) {
        this.find(iD) // Find Next
        this.ace_editor.replaceAll(iD.r_txt)
    }
    
    //---f:
    focus() {
        this.ace_editor.focus()
    }
    
    //---f:
    setMode(mode) {
        let amode = this.getMode(mode)
        this.mode = amode.split('/').slice(-1)[0]
        this.ace_editor.getSession().setMode(amode)
    }

    //---f:
    pinOutline(mode) {
        // mode: 0 - keyboard toggle, 1 - menu pin, 2 - menu unpin
        // let pos = CodeEditor.getCurrentPosition()[0]
        let line_txt, line
        
        // Get mode from toggle
        if (mode == 0) {
            line = CodeEditor.ace_editor.getSelectionRange().start.row
            line_txt = CodeEditor.ace_editor.session.getLine(line)
            if (line_txt.indexOf('---pin:') > -1) {
                mode = 2
            } else {
                mode = 1
            }
        } else {
            if (CodeEditor.outline.MENU_ITEM && $P.class.has(CodeEditor.outline.MENU_ITEM,'outline-item')) {
                line = parseInt(CodeEditor.outline.MENU_ITEM.id.slice(8))
                line_txt = CodeEditor.ace_editor.session.getLine(line)
                CodeEditor.gotoLine(line)
            } else {
                return
            }
        }
        
        if (mode == 1) {
            
            // Pin
            // if (line_txt.trim().length>0) {
                CodeEditor.ace_editor.session.insert({row:line,column:0},'\n')
            // }
            CodeEditor.ace_editor.clearSelection()
            CodeEditor.ace_editor.moveCursorTo(line,0)
            CodeEditor.ace_editor.toggleCommentLines()
            CodeEditor.ace_editor.session.insert({row:line,column:CodeEditor.ace_editor.session.getLine(line).length-1},
                '---pin:')
            CodeEditor.ace_editor.moveCursorTo(line,CodeEditor.ace_editor.session.getLine(line).length)
        } else {
            // Unpin
            if (line_txt.indexOf('---pin:') > -1) {
                CodeEditor.ace_editor.clearSelection()
                CodeEditor.ace_editor.moveCursorTo(line,0)
                CodeEditor.ace_editor.removeLines()
            } else {
                alert('Cannot unpin line '+line)
            }
        }
        
        // Update Outline right away
        this.outline.update(this.getText())
        
    }

//---
//---Utils
    //---f:
    rtrim(txt) {
        return txt.replace(/~+$/g,'')
    }
    
    //---f:
    getMode(ext) {
        let ace_mode = 'ace/mode/text'
        if (ext) {
            if (ext in this.settings.modeMap) {
                ace_mode = 'ace/mode/'+this.settings.modeMap[ext]
            } else {
                ace_mode = this.modelist.getModeForPath('file.'+ext).mode
            }
        }
        // console.log(ace_mode)
        return ace_mode
    }
    //---f:
    // getModeByFileExtension(path){
    //     return this.modelist.getModeForPath(path).mode
    // }

}





class Outline {
    constructor(editor_obj,element, pinned_element) {
        this.element = element
        this.element_pinned = pinned_element
        this.CodeEditor = editor_obj
        this.items = []
        this.location_timer = undefined
        this.scroll_update = 1
        
        // Add Scollspy
        editor_obj.ace_editor.getSession().on('changeScrollTop',function (e) {
            this.updateLocation()
        }.bind(this))

        // Add Shortcut
        this.CodeEditor.ace_editor.commands.addCommand({
            name: 'update_output',
            bindKey: {win: 'Alt-O',  mac: 'Alt-O'},
            exec: function(editor) {
                this.update()
            }.bind(this),
            readOnly: false
        })
        
    }
    
    //---f:
    updateLocation(no_scroll) {
        if (this.CodeEditor.mode in OutlineParser) {
            // Debounce timer
            if (this.location_timer) {
                clearTimeout(this.location_timer)
            }
            
            this.location_timer = setTimeout(function(){
                
                let start_line = this.CodeEditor.ace_editor.getFirstVisibleRow()
                let end_line = this.CodeEditor.ace_editor.getLastVisibleRow()

                $P.class.remove($P.query('.outline-item.visible'),'visible')
                
                let vis_items = []
                for (let itm of this.items) {
                    if (itm.line>=start_line && itm.line <=end_line && itm.typ != 'pin') {
                        vis_items.push(itm.line)
                    }
                }
                for (let line of vis_items) {
                    $P.class.add('line-no-'+line,'visible')
                }
                
                // Scroll to last item
                if (vis_items.length > 0 && !no_scroll && this.scroll_update) {
                    let scroll_elm = $P.elm('line-no-'+vis_items.slice(-1)[0])
                    // let line_pos = scroll_elm.getBoundingClientRect()
                    // let outline_h = $P.elm('outline').offsetHeight
                    // let outline_t = $P.elm('outline').scrollTop
                    scroll_elm.scrollIntoView({block:'nearest'})
                }
                this.scroll_update = 1
            
            }.bind(this), 100)
            
        }
    }
    
    //---f:
    update(txt) {
        if (this.CodeEditor.mode in OutlineParser) {
            if (txt == undefined) {
                txt = this.CodeEditor.getText()
            }
            let outline = OutlineParser[this.CodeEditor.mode](txt)
            this.items = outline
            let CodeEditor =  this.CodeEditor
            // Clear outline element
            let outline_elm = document.getElementById(this.element)
            let outline_pinned = document.getElementById(this.element_pinned)
            // outline_elm.innerHTML = ''
            $P.clear(outline_elm)
            $P.clear(outline_pinned)
            
            let c = -1
            let outline_length = outline.length
            for (let itm of outline) {
                c++
                let hdtxt
                if (itm.typ == 'heading') {
                    hdtxt = itm.txt.trim().toLowerCase()
                    if (hdtxt.startsWith('todo')) {
                        itm.typ = 'todo'
                    } else if (hdtxt.startsWith('note:')) {
                        itm.typ = 'note'
                    } else if (hdtxt.startsWith('pin:')) {
                        itm.typ = 'pin'
                    }
                }
                let elm = document.createElement('div')
                itm.txt.slice(200) // Limit 200 characters
                elm.innerText = itm.txt
                elm.setAttribute('class','outline-item '+itm.typ)
                elm.id = 'line-no-'+itm.line
                elm.style.paddingLeft = (4+itm.space*3)+'px'
                elm.onclick = function() {
                    CodeEditor.gotoLine(itm.line)
                }
                if (itm.typ == 'pin') {
                    if (hdtxt == 'pin:') {
                        elm.title = 'line '+(itm.line+1)
                        if (c+1 < outline_length && outline[c+1].line == itm.line+1) {
                            // grab next line if also outline item
                            elm.innerText = outline[c+1].txt
                        } else if (c >0) {
                            // grab previous outline text
                            elm.innerText = outline[c-1].txt
                        } else {
                            elm.innerText = 'line '+(itm.line+1)
                        }
                    } else {
                        elm.innerText = itm.txt.slice(4)
                    }
                    outline_pinned.appendChild(elm)
                } else {
                    outline_elm.appendChild(elm)
                }
            }
            
            this.updateLocation()
        }
    }

    //---f:
    find(txt) {
        let vis_items = []
        let hid_items = []
        for (let itm of this.items) {
            if (txt == '' || itm.txt.toLowerCase().indexOf(txt) > -1) {
                vis_items.push('line-no-'+itm.line)
            } else {
                hid_items.push('line-no-'+itm.line)
            }
        }
        $P.hide(hid_items)
        $P.show(vis_items)
    }

}




//---o:
let OutlineParser = {

    //---o:
    settings: {
        showDecorators:false,
    },
    
    parseLine: function(line) {
        let spc = line.length - line.trim().length
        let tls = line.trim()
        let itmtxt = null
        let typ = null
        return [spc,tls,itmtxt,typ]
    },
    
    rtrim: function(txt) {
        return txt.replace(/~+$/g,'')
    },
    
    javascript: function(txt) {
        
        let txtlines = txt.split('\n')
        let outline = []
        let l = -1
        for (let row of txtlines) {
            l++
            let line = this.rtrim(txtlines[l])
            let [spc,tls,itmtxt,typ] = this.parseLine(line)
            let line_no = l
            if (tls.startsWith('function ')) {
                itmtxt = tls.slice(9)
                typ = 'function'
            } else if (tls.startsWith('async function ')) {
                itmtxt = tls.slice(15)
                typ = 'function'
            } else if (tls.startsWith('export function ')) {
                itmtxt = tls.slice(16)
                typ = 'function'
            } else if (tls.startsWith('static ')) {
                itmtxt = tls.slice(7).split('=')[0]
                typ = 'function'
            } else if (tls.startsWith('constructor(')) {
                itmtxt = tls
                typ = 'function'
            } else if (tls.startsWith('class ')) {
                itmtxt = tls.slice(6)
                typ = 'object'
            } else if (!tls.startsWith('//') && (tls.indexOf(': function')>=0 || tls.indexOf(':function')>=0 ||
                    tls.indexOf(':async function')>=0 || tls.indexOf(': async function')>=0)) {
                itmtxt = tls.split(':')[0]+tls.split('function').slice(-1)[0]
                typ = 'function'
            } else if (tls.startsWith('//---')) {
                if (tls.startsWith('//---f:') && txtlines.length > l+1) {
                    itmtxt = txtlines[l].trim().slice(7).trim()
                    if (itmtxt == '') {itmtxt = txtlines[l+1].trim()}
                    line_no = l+1
                    typ = 'function'
                } else if (tls.startsWith('//---o:') && txtlines.length > l+1) {
                    itmtxt = txtlines[l+1].split('=')[0].trim()
                    typ = 'object'
                    line_no = l+1
                    if (itmtxt.startsWith('let ') || itmtxt.startsWith('var ')) {
                        itmtxt = itmtxt.slice(4)
                    } else if (itmtxt.startsWith('const ')) {
                        itmtxt = itmtxt.slice(6)
                    }
                } else {
                    itmtxt = tls.slice(5)
                    typ = 'heading'
                }
            } else if ((tls.startsWith('const ') || tls.startsWith('let ')) && (tls.endsWith('=> {') || tls.endsWith('=>{'))) {
                // New Functions
                itmtxt = tls.split('=')[0]
                typ = 'function'
                if (itmtxt.startsWith('let ')) {
                    itmtxt = itmtxt.slice(4)
                } else if (itmtxt.startsWith('const ')) {
                    itmtxt = itmtxt.slice(6)
                }
            }
            
            
            if (itmtxt != null) {
                // Remove ending bracket
                if (itmtxt.endsWith('{')) {itmtxt = itmtxt.slice(0,-1)}
                
                outline.push({
                    txt:itmtxt,
                    space:spc,
                    typ:typ,
                    line:line_no,
                })
            }
            
        }
        
        return outline
    },

    c_cpp: function(txt) {
        
        let txtlines = txt.split('\n')
        let outline = []
        let l = -1
        for (let row of txtlines) {
            l++
            let line = this.rtrim(txtlines[l])
            let [spc,tls,itmtxt,typ] = this.parseLine(line)
            let line_no = l
            if (tls.startsWith('void ')) {
                itmtxt = tls.slice(5)
                typ = 'function'
            } else if (tls.startsWith('//---')) {
                if (tls.startsWith('//---f:') && txtlines.length > l+1) {
                    itmtxt = txtlines[l+1].trim()
                    line_no = l+1
                    typ = 'function'
                } else if (tls.startsWith('//---o:') && txtlines.length > l+1) {
                    itmtxt = txtlines[l+1].split('=')[0].trim()
                    typ = 'object'
                    line_no = l+1
                    if (itmtxt.startsWith('let ') || itmtxt.startsWith('var ')) {
                        itmtxt = itmtxt.slice(4)
                    } else if (itmtxt.startsWith('const ')) {
                        itmtxt = itmtxt.slice(6)
                    }
                } else {
                    itmtxt = tls.slice(5)
                    typ = 'heading'
                }
            }

            if (itmtxt != null) {
                // Remove ending bracket
                if (itmtxt.endsWith('{')) {itmtxt = itmtxt.slice(0,-1)}
                
                outline.push({
                    txt:itmtxt,
                    space:spc,
                    typ:typ,
                    line:line_no,
                })
            }

        }
        return outline
    },

    css: function(txt) {
        
        let txtlines = txt.split('\n')
        let outline = []
        let l = -1
        for (let row of txtlines) {
            l++
            let line = this.rtrim(txtlines[l])
            let [spc,tls,itmtxt,typ] = this.parseLine(line)
            if (tls.startsWith('/*---')) {
                itmtxt = tls.slice(5,-2)
                typ = 'heading'
            } else if (tls.startsWith('//---')) {
                // SASS heading
                itmtxt = tls.slice(5)
                typ = 'heading'
            } else if (!tls.startsWith('/*') && !tls.startsWith('//')) {
                let matches = tls.match(/.*{/)
                if (matches) {
                    itmtxt = matches[0].slice(0,-1).trim()
                    if (itmtxt.startsWith('.') || itmtxt.startsWith('@')) {
                        typ = 'function'
                    } else {
                        typ = 'object'
                    }
                }
            }

            if (itmtxt != null) {
                // Remove ending bracket
                if (itmtxt.endsWith('{')) {itmtxt = itmtxt.slice(0,-1)}
                
                outline.push({
                    txt:itmtxt,
                    space:spc,
                    typ:typ,
                    line:l,
                })
            }

        }
        return outline
    },

    scss: function(txt) {
        return OutlineParser.css(txt)
    },

    sass: function(txt) {
        return OutlineParser.css(txt)
    },


    html: function(txt) {
        let txtlines = txt.split('\n')
        let outline = []
        let l = -1
        for (let row of txtlines) {
            l++
            let line = this.rtrim(txtlines[l])
            let [spc,tls,itmtxt,typ] = this.parseLine(line)
            let line_no = l
            //---HTML
            if (tls.startsWith('<body')) {
                itmtxt='<BODY>'
                typ='object'
            } else if (tls.startsWith('<h1')) {
                itmtxt=tls
                typ='function'
            } else if (tls.startsWith('<h2')) {
                itmtxt=tls
                typ='function'
            } else if (tls.startsWith('<head')) {
                itmtxt='<head>'
                typ='object'
            } else if (tls.startsWith('<!---')) {
                itmtxt=tls.slice(5,-3)
                typ='heading'
                
            //---JavaScript
            } else if (tls.startsWith('function ')) {
                itmtxt = tls.slice(9)
                typ = 'function'
            } else if (tls.startsWith('async function ')) {
                itmtxt = tls.slice(15)
                typ = 'function'
            } else if (tls.startsWith('export function ')) {
                itmtxt = tls.slice(16)
                typ = 'function'
            } else if (tls.startsWith('static ')) {
                itmtxt = tls.slice(7).split('=')[0]
                typ = 'function'
            } else if (tls.startsWith('constructor(')) {
                itmtxt = tls
                typ = 'function'
            } else if (tls.startsWith('class ')) {
                itmtxt = tls.slice(6)
                typ = 'object'
            } else if (!tls.startsWith('//') && (tls.indexOf(': function')>=0 || tls.indexOf(':function')>=0 ||
                    tls.indexOf(':async function')>=0 || tls.indexOf(': async function')>=0)) {
                itmtxt = tls.split(':')[0]+tls.split('function').slice(-1)[0]
                typ = 'function'
            } else if (tls.startsWith('//---')) {
                if (tls.startsWith('//---f:') && txtlines.length > l+1) {
                    itmtxt = txtlines[l].trim().slice(7).trim()
                    if (itmtxt == '') {itmtxt = txtlines[l+1].trim()}
                    line_no = l+1
                    typ = 'function'
                } else if (tls.startsWith('//---o:') && txtlines.length > l+1) {
                    itmtxt = txtlines[l+1].split('=')[0].trim()
                    typ = 'object'
                    line_no = l+1
                    if (itmtxt.startsWith('let ') || itmtxt.startsWith('var ')) {
                        itmtxt = itmtxt.slice(4)
                    } else if (itmtxt.startsWith('const ')) {
                        itmtxt = itmtxt.slice(6)
                    }
                } else {
                    itmtxt = tls.slice(5)
                    typ = 'heading'
                }
            } else if ((tls.startsWith('const ') || tls.startsWith('let ')) && (tls.endsWith('=> {') || tls.endsWith('=>{'))) {
                itmtxt = tls.split('=')[0]
                typ = 'function'
                if (itmtxt.startsWith('let ')) {
                    itmtxt = itmtxt.slice(4)
                } else if (itmtxt.startsWith('const ')) {
                    itmtxt = itmtxt.slice(6)
                }
                
            //---CSS
            } else if (tls.startsWith('/*---')) {
                // CSS
                itmtxt = tls.slice(5,-2)
                typ = 'heading'
            }
            
            if (itmtxt != null) {
                // Remove ending bracket
                if (itmtxt.endsWith('{')) {itmtxt = itmtxt.slice(0,-1)}
                
                outline.push({
                    txt:itmtxt,
                    space:spc,
                    typ:typ,
                    line:line_no,
                })
            }
            
        }
        
        return outline
    },
    
    handlebars: function(txt) {
        return OutlineParser.html(txt)
    },

    
    markdown: function(txt) {
        
        let txtlines = txt.split('\n')
        let outline = []
        let l = -1
        for (let row of txtlines) {
            l++
            let line = this.rtrim(txtlines[l])
            let [spc,tls,itmtxt,typ] = this.parseLine(line)
            if (line.startsWith('#')) {
                let h = tls.split(' ')[0].split('#').length-1
                spc = (h-1)*4
                itmtxt = tls.split('#').slice(-1)[0].trim()
                if (h == 1) {
                    typ = 'object'
                } else {
                    typ = 'function'
                }
            }
            
            if (itmtxt != null) {
                outline.push({
                    txt:itmtxt,
                    space:spc,
                    typ:typ,
                    line:l,
                })
            }
            
        }
        
        return outline
    },
    
    python: function(txt) {
        let show_decorators = this.settings.showDecorators
        let txtlines = txt.split('\n')
        let outline = []
        let l = -1
        for (let row of txtlines) {
            l++
            let line = this.rtrim(txtlines[l])
            let [spc,tls,itmtxt,typ] = this.parseLine(line)
            let line_no = l
            if (tls.startsWith('def ')) {
                itmtxt = tls.slice(4,-1)
                typ = 'function'
            } else if (tls.startsWith('async def ')) {
                itmtxt = tls.slice(9,-1)
                typ = 'function'
            } else if (show_decorators && tls.startsWith('@')) {
                itmtxt = tls
                typ = 'decorator'
            } else if (tls.startsWith('class ')) {
                itmtxt = tls.slice(6,-1)
                typ = 'object'
            } else if (tls.startsWith('#---')) {
                
                if (tls.startsWith('#---o:') && txtlines.length > l+1) {
                    itmtxt = txtlines[l+1].split('=')[0].trim()
                    typ = 'object'
                    line_no = l+1
                } else {
                    itmtxt = tls.slice(4)
                    typ = 'heading'
                }
                
            }
            
            if (itmtxt != null) {
                outline.push({
                    txt:itmtxt,
                    space:spc,
                    typ:typ,
                    line:line_no,
                })
            }
            
        }
        
        return outline
    },

    ini: function(txt) {
        
        let txtlines = txt.split('\n')
        let outline = []
        let l = -1
        for (let row of txtlines) {
            l++
            let line = this.rtrim(txtlines[l])
            let [spc,tls,itmtxt,typ] = this.parseLine(line)
            if (tls.startsWith('[')) {
                itmtxt = tls.slice(1,-1)
                typ = 'object'
            } else if (tls.startsWith('#---')) {
                itmtxt = tls.slice(4)
                typ = 'heading'
            }
            
            if (itmtxt != null) {
                outline.push({
                    txt:itmtxt,
                    space:spc,
                    typ:typ,
                    line:l,
                })
            }
            
        }
        
        return outline
    },

    lua: function(txt) {
        
        let txtlines = txt.split('\n')
        let outline = []
        let l = -1
        for (let row of txtlines) {
            l++
            let line = this.rtrim(txtlines[l])
            let [spc,tls,itmtxt,typ] = this.parseLine(line)
            if (tls.startsWith('function')) {
                itmtxt = tls.slice(9)
                typ = 'function'
            } else if (tls.startsWith('local function')) {
                itmtxt = tls.slice(14)
                typ = 'function'
            } else if (tls.startsWith('-----')) {
                itmtxt = tls.slice(4)
                typ = 'heading'
            }
            
            if (itmtxt != null) {
                outline.push({
                    txt:itmtxt,
                    space:spc,
                    typ:typ,
                    line:l,
                })
            }
            
        }
        
        return outline
    },

    cheatsheet: function(txt) {
        return this.kite(txt)
    },

    kite: function(txt) {
        
        let txtlines = txt.split('\n')
        let outline = []
        let l = -1
        for (let row of txtlines) {
            l++
            let line = this.rtrim(txtlines[l])
            let [spc,tls,itmtxt,typ] = this.parseLine(line)
            if (tls.startsWith('h>')) {
                itmtxt = tls.slice(2)
                typ = 'object'
            } else if (tls.startsWith('c>')) {
                itmtxt = tls.slice(2)
                typ = 'function'
                spc = 2
            }
            
            if (itmtxt != null) {
                outline.push({
                    txt:itmtxt,
                    space:spc,
                    typ:typ,
                    line:l,
                })
            }
            
        }
        
        return outline
    },

}


const path = require('path')
const fs = require('fs')

//---o:
let Plugin = {
    js:['arduino_ui.js'], // ui javascript to load into app
    css:[],  // css files to load into app
    settings:{
        'cli_path':'{{scope}}/plugins/arduino/arduino-cli'
    },
}

function load(IDE) {
    Arduino.IDE = IDE
    Arduino.cli  = Arduino.IDE.settings.plugins['arduino'].cli_path.replace(/{{scope}}/gi,Arduino.IDE.path)
    
    return Arduino
}


//---o:
let Arduino = {
    // Backend functions here
    
    checkCLI: function() {
        let ok = 0
        if (fs.existsSync(Arduino.cli)) {
            ok = 1
        } else {
            Arduino.IDE.win.webContents.send("plugin", {plugin:'arduino', function:'addOutputText', details:{mode:'error', 
            text:'Arduino CLI was not found at path:\n'+Arduino.cli+'\nEither copy the executable to that location or change the location in your Scope settings'}})
        }
        return ok
    },
    

    selectDevices: function(details) {
        let devices = []
        if (Arduino.checkCLI()) {
            let cmd = Arduino.cli+' board list'
            
            let stdout = require('child_process').execSync(cmd)
            Arduino.IDE.win.webContents.send("plugin", {plugin:'arduino', function:'addOutputText', details:{mode:'stdout', text:stdout.toString()}})
            let lines = stdout.toString().split('\n')
            let fqbn_index = lines[0].indexOf('FQBN')
            let name_index = lines[0].indexOf('Board Name')
            if (fqbn_index > -1 && lines.length > 1)
            for (let line of lines.slice(1)) {
                if (line.startsWith('/dev')) {
                    let fqbn = line.slice(fqbn_index).split(' ')[0]
                    let name = line.slice(name_index,fqbn_index)
                    if (fqbn != '') {
                        devices.push({port:line.split(' ')[0], fqbn:fqbn, name:name})
                    }
                }
            }
        }
        return devices
        
    },

    installDevice: function(details) {
        if (Arduino.checkCLI()) {
            let cmd = Arduino.cli+' core install '+details.device
            let stdout = require('child_process').execSync(cmd)
            Arduino.IDE.win.webContents.send("plugin", {plugin:'arduino', function:'addOutputText', details:{mode:'stdout', text:stdout.toString()}})
        }
    },

    verify: function(details) {
        if (Arduino.checkCLI()) {
            let cmd = Arduino.cli+' compile --fqbn '+details.fqbn+' "'+details.filename+'"'
            console.log(cmd)
            require('child_process').exec(cmd, (error, stdout, stderr)=>{
                // Arduino.IDE.win.webContents.send("status", {text:proc.toString()})
                console.log(stderr)
                if (error) {
                    Arduino.IDE.win.webContents.send("plugin", {plugin:'arduino', function:'addOutputText', details:{mode:'error', text:error}})
                } else {
                    Arduino.IDE.win.webContents.send("plugin", {plugin:'arduino', function:'addOutputText', details:{mode:'stdout', text:stdout+'Done'}})
                }
            })
        }
    },

    upload: function(details) {
        if (Arduino.checkCLI()) {
            let cmd = Arduino.cli+' compile --fqbn '+details.fqbn+' "'+details.filename+'"'
            cmd += ' && ' + Arduino.cli+' upload --port '+details.port+' --fqbn '+details.fqbn+' "'+details.filename+'"'
            console.log(cmd)
            require('child_process').exec(cmd, (error, stdout, stderr)=>{
                // Arduino.IDE.win.webContents.send("status", {text:proc.toString()})
                console.log(stderr)
                if (error) {
                    Arduino.IDE.win.webContents.send("plugin", {plugin:'arduino', function:'addOutputText', details:{mode:'error', text:error}})
                // } else if (stderr) {
                } else {
                    Arduino.IDE.win.webContents.send("plugin", {plugin:'arduino', function:'addOutputText', details:{mode:'stdout', text:stdout}})
                    Arduino.IDE.win.webContents.send("plugin", {plugin:'arduino', function:'addOutputText', details:{mode:'stdout', text:stderr+'Done'}})
                }
            })
        }
    },

}

module.exports = {load,Plugin}
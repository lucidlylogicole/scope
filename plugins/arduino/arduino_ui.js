
//---o:
let ArduinoPlugin = {
    
    view: function() {
        if (ArduinoPlugin.tab === undefined) {
            
            // Add Bottom Window Plugin
            let arduino_elm = {
                h:'#arduino_page.flex-row flex', c:[
                    {h:'.flex-col pad scroll-y', c:[
                        {h:'button.scope-btn',text:'verify',onclick:"ArduinoPlugin.verify()", title:'verify sketch'},
                        {h:'button.scope-btn',text:'upload',onclick:"ArduinoPlugin.upload()", title:'compile and upload sketch'},
                        {h:'button.scope-btn margin-t',text:'find device',onclick:"ArduinoPlugin.selectDevices()", title:'find connected arduinos'},
                        // {h:'button.scope-btn margin-l-lg',text:'view devices',onclick:"ArduinoPlugin.viewDevices()"},
                        {h:'button.scope-btn margin-l-lg',text:'install device',onclick:"ArduinoPlugin.installDevice()", title:"install current FQBN device"},
                        {h:'button.scope-btn margin-l-lg margin-t',text:'serial monitor',onclick:"ArduinoPlugin.launchSerialMonitor()"},
                    ]},
                    {h:'.flex-col scroll-y pad-l', c:[
                        {h:'.margin-sm-t',text:'Device'},
                        {h:'#arduino_device_name.clr-w',},
                        {h:'.margin-t',text:'Port'},
                        {h:'input#le_arduino_device_port.scope-input'},
                        {h:'.margin-t',text:'FQBN'},
                        {h:'input#le_arduino_device_fqbn.scope-input'},
                    ]},
                    
                    {h:'#arduino_output.flex output-console'},
                    
                ],
                
            }
        
            // Add to Bottom tabs
            ArduinoPlugin.tab = IDE.BottomTab.addTab({
                tooltip:'Arduino',
                icon:'plugins/arduino/arduino.svg',
                close:0,
                page:arduino_elm,
                title:'Arduino'
            })
            
        }
        
        // Select Current
        IDE.BottomTab.selectTab(ArduinoPlugin.tab.index)
        
        // Check CLI
        window.electron_api.sendSync('plugin',{plugin:'arduino',function:'checkCLI',})
        
    },
    
    verify: function() {
        
        // Check for valid file and fqbn
        let error_text = ''
        if (!(IDE.CURRENT_FILE && IDE.files[IDE.CURRENT_FILE].path.endsWith('.ino'))) {
            error_text = 'The current tab in Scope is not an Arduino (.ino) File'
        } else if ($P.val('le_arduino_device_fqbn') == '') {
            error_text = 'Please select a device or enter the FQBN'
        }
        
        if (error_text == '') {
            ArduinoPlugin.clearOutput()
            ArduinoPlugin.addOutputText({text:'Compiling/Verifying...'})
            window.electron_api.sendSync('plugin',{plugin:'arduino',function:'verify',
                details:{filename:IDE.files[IDE.CURRENT_FILE].path, fqbn:$P.val('le_arduino_device_fqbn')}
            })
        } else {
            ArduinoPlugin.addOutputText({mode:'error',text:error_text})
        }
    },
    
    upload: function() {
        // Check for valid file and fqbn
        let error_text = ''
        if (!(IDE.CURRENT_FILE && IDE.files[IDE.CURRENT_FILE].path.endsWith('.ino'))) {
            error_text = 'The current tab in Scope is not an Arduino (.ino) File'
        } else if ($P.val('le_arduino_device_fqbn') == '' || $P.val('le_arduino_device_port') == '') {
            error_text = 'Please select a device or enter the FQBN and port'
        }
        
        if (error_text == '') {
            ArduinoPlugin.clearOutput()
            ArduinoPlugin.addOutputText({text:'Compiling and Uploading...'})
            window.electron_api.sendSync('plugin',{plugin:'arduino',function:'upload',
                details:{filename:IDE.files[IDE.CURRENT_FILE].path, port:$P.val('le_arduino_device_port'),fqbn:$P.val('le_arduino_device_fqbn'), }
            })
        } else {
            ArduinoPlugin.addOutputText({mode:'error',text:error_text})
        }
    },

    selectDevices: function() {
        ArduinoPlugin.clearOutput()
        ArduinoPlugin.addOutputText({text:'Devices'})
        let devices = window.electron_api.sendSync('plugin',{plugin:'arduino',function:'selectDevices',
            details:{}
        })
        
        if (devices.length == 1 && $P.val('le_arduino_device_fqbn') == '') {
            $P.html('arduino_device_name', devices[0].name)
            $P.val('le_arduino_device_port', devices[0].port)
            $P.val('le_arduino_device_fqbn', devices[0].fqbn)
        }
        
    },

    installDevice: function() {
        if ($P.val('le_arduino_device_fqbn') != '') {
            ArduinoPlugin.addOutputText({text:'Installing Device'})
            let devices = window.electron_api.sendSync('plugin',{plugin:'arduino',function:'installDevice',
                details:{device:$P.val('le_arduino_device_fqbn').split(':').slice(0,2).join(':')}
            })
        } else {
            ArduinoPlugin.addOutputText({text:'FQBN cannot be blank',mode:'error'})
        }
    },
    
    addOutputText: function(outputD) {

        // Create new line
        let newline = document.createElement('div')
        if (outputD.mode == 'error') {
            newline.innerHTML = outputD.text
        } else {
            // newline.innerText = $P.sanitize(txt,'html')
            newline.innerText = outputD.text
        }
        if (outputD.mode === undefined) {outputD.mode = 'outtext'}
        newline.className = outputD.mode
        
        // Append to output
        let out_elm = $P.elm(`arduino_output`)
        out_elm.appendChild(newline)
        out_elm.scrollTo(0,out_elm.scrollHeight)
    },
    
    clearOutput: function() {
        $P.clear(arduino_output)
    },
    
    launchSerialMonitor: function() {
        if ($P.val('le_arduino_device_port') != '') {
            LaunchersPlugin.addConsole({
                cmd:'cat '+$P.val('le_arduino_device_port'),
                title:'Serial Monitor',
                autoStart:1,
            })
        } else {
            ArduinoPlugin.addOutputText({mode:'error',text:'Could not launch Serial Monitor.  No Device is selected'})
        }
    },
    
}

function createPlugin() {
    // UI Plugin Startup Code

    IDE.plugins['arduino'] = ArduinoPlugin

    // Add Icon to the Left
    let ff_btn = $P.create({h:'button#b_find_files.btn-clear', onclick:"ArduinoPlugin.view()",
        title:"Arduino", c:[{h:'img',src:'plugins/arduino/arduino.svg'}],
        parent:'sidebar_plugin_buttons'
    })

    // ArduinoPlugin.view()


}

createPlugin()
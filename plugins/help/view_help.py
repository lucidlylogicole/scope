import sys

def pdir(sobj, imp=None):
    l = {}
    if type(sobj) == str:
        if imp == None: imp = sobj.split('.')[0]
        exec('import '+imp,globals(),l)
        exec('d=dir('+sobj+')',globals(),l)
    else:
        l['d']=dir(sobj)
    for obj in l['d']:
        if not obj.startswith('__'):
            print(obj)

def phelp(sobj, imp=None):
    print(help(sobj))

#---main
if __name__ == '__main__':
    if len(sys.argv) == 2:
        sobj = sys.argv[1]
        phelp(sobj)
    elif len(sys.argv) > 2:
        imp = None
        sobj = sys.argv[2]
        if len(sys.argv) >3: imp = sys.argv[3]
        if '-d' in sys.argv:
            pdir(sobj,imp)
        elif '-h' in sys.argv:
            phelp(sobj,imp)

//---o:
let HelpPlugin = {
    // UI Template object
    
    view: function() {
        if (!(HelpPlugin.page_uuid in IDE.RightWindow.pages)) {
            // Load 
            let ff_elm = {
                h:'#pg_help.page flex-col flex', 'data-display':'flex', c:[
                    {h:'.flex-row-c pad', c:[
                        {h:'select#help_lang.scope-input',c:[
                            {h:'option',text:'JS (Browser)'},
                            // {h:'option',text:'JS (Node)'},
                            {h:'option',text:'Python', selected:'selected'},
                            {h:'option',text:'Qt'},
                        ]},
                        {h:'.margin-l margin-sm-r',text:'Search'},
                        {h:'input#i_help_text.flex-2 scope-input ', placeholder:'module / object',onkeydown:"if (event.keyCode == 13) {HelpPlugin.search()}"},
                    ]},
                    {h:'textarea#help_results.scope-textarea flex pad show-color', readonly:'true',
                        'text':`Simple reference search.\n\n  Add a . at the end to list available functions/attributes\n\nExamples:\n\n    os.path     (python)\n    os.path.    (python - list functions)`, 
                        style:'white-space: pre;overflow-wrap: normal;'},
                ]
                
            }
            HelpPlugin.page_uuid = IDE.RightWindow.addPage({content:ff_elm,iframe:0})
            
        } 
        
        // Toggle
        IDE.RightWindow.showPage(HelpPlugin.page_uuid)
        
        $P.elm('i_help_text').focus()
    },
    
    search: function() {
        $P.clear('help_results')
        let stxt = $P.val('i_help_text')
        let lang = $P.val('help_lang')
        let results
        
        if (lang == 'JS (Browser)') {
            let obj = window
            for (let ky of stxt.split('.')) {
                if (ky != '') {
                    obj = obj[ky]
                    if (obj === undefined) {
                        break
                    }
                }
            }
            if (obj === undefined) {
                results = stxt+' not found'
            } else {
                let prop = Object.getOwnPropertyNames(obj)
                results = ''
                for (let p of prop) {
                    results += p+'\n'
                }
            }
        } else {
            results = window.electron_api.sendSync('plugin',{plugin:'help',function:'search',
                details:{text:stxt, lang:lang}
            })
        }
        
        $P.val('help_results',results)
        
    },
    

}

function createPlugin() {
    // UI Plugin Startup Code

    // Add Folder Button
    let ff_btn = $P.create({h:'button.btn-clear', onclick:"HelpPlugin.view()",
        title:"Help / Reference", c:[{h:'img',src:'plugins/help/help.svg'}],
        parent:'sidebar_plugin_buttons'
    })
    
    // Add keyboard shortcut
    IDE.keymap.add('h',HelpPlugin.view,{ctrl:1}) 
    
    IDE.plugins['help'] = HelpPlugin

}

createPlugin()
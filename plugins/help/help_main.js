const path = require('path')
const fs = require('fs')

//---o:
let Plugin = {
    js:['help_ui.js'], // ui javascript to load into app
    css:[],  // css files to load into app
    settings:{
        
    },
}

function load(IDE) {
    HelpPlugin.IDE = IDE
    return HelpPlugin
}


//---o:
let HelpPlugin = {
    // Backend functions here
    
    search: function(details) {
        //---Python
        if (details.lang == 'JS (Node)') {
            
            // return Object.getOwnPropertyNames(window[details.text])
            
        } else if (details.lang == 'Python') {
            
            let args = [HelpPlugin.IDE.path+'/plugins/help/view_help.py','-h',details.text]
            
            // If endswith '.', then list directory
            if (details.text.endsWith('.')) {
                args[1]='-d'
                args[2]=details.text.slice(0,details.text.length-1)
            } else {
                args = ['-m','pydoc',details.text]
            }
            
            
            // Run command
            proc = require('child_process').spawnSync(HelpPlugin.IDE.settings.paths['python'],args,{
                shell: false, detached: true, encoding : 'utf8' 
            })
            
            return proc.stdout
            
        } else if (details.lang == 'Qt') {
        //---Qt
            // Load Qt Docs
            if (this.qt_docs === undefined) {
                let qt_pth = HelpPlugin.IDE.path+'/plugins/help/qt5_docs.json'
                let text = fs.readFileSync(qt_pth)
                this.qt_docs = JSON.parse(text)
                
            }
            
            let parts = details.text.toLowerCase().split('.')
            // Find top level
            let l1,l2
            for (let cls in this.qt_docs) {
                if (cls.toLowerCase() == parts[0].toLowerCase()) {
                    l1 = this.qt_docs[cls]
                    
                    if (parts.length >1) {
                        l2 = parts[1]
                    }
                    
                    break
                }
            }
            
            // Find l2
            if (l1 === undefined) {
                for (let cls in this.qt_docs) {
                    for (let obj in this.qt_docs[cls]) {
                        if (obj.toLowerCase() == parts[0].toLowerCase()) {
                            l1 = cls
                            l2 = parts[0]
                            parts.unshift(cls)
                        }
                    }
                }
            }
            
            if (l2 === undefined) {
                // Show Modules
                return 'not found'
            } else {
                //  Show Module
                // if (parts.length > 2) {
                    let txt = ''
                    txt += l1+'.'+this.qt_docs[l1][l2].name+'\n'
                    if (parts[2] == '') {
                        // Directory
                        for (let itm in this.qt_docs[l1][l2].modules){
                            txt += '\n'+itm
                        }
                    } else {
                        for (let prop in this.qt_docs[l1][l2].modules) {
                            // console.log(prop)
                            if (prop.toLowerCase().startsWith(parts[2]) || parts[2]===undefined) {
                                // txt += '\n'+l1+'.'+this.qt_docs[l1][l2].name+'.'
                                let pname = this.qt_docs[l1][l2].modules[prop].def
                                if (pname.includes('::')) {
                                    pname = pname.split('::')[1]
                                }
                                txt += '\n'+pname
                                txt += '\n   '+this.qt_docs[l1][l2].modules[prop].desc+'\n'
                            }
                        }
                    }
                    if (txt == '') {
                        return l1+'.'+this.qt_docs[l1][l2].name+'.'+parts[2]+'\nNOT FOUND'
                    }
                    return txt
                // }
            }
            
            let txt = l1
            if (l2 && l2 in this.qt_docs[l1]) {
                txt +='.'+this.qt_docs[l1][l2].name
            }
            return txt
            
        }
            
        
    },
    
}

module.exports = {load,Plugin}

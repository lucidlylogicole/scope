import sys, json
from pyflakes import api

class myReporter:
    def __init__(self):
        self.error_line = None
        self.error_text = None
        
    def unexpectedError(self, filename, msg):
        pass
        
    def syntaxError(self, filename, msg, lineno, offset, text):
        # print(lineno,msg)
        self.error_line = lineno
        self.error_text = msg
        
    def flake(self, message):
        pass


def check_file(filename=None,txt=None):
    if filename != None:
        with open(filename,'r') as f:
            txt = f.read()

    rpt = myReporter()
    errors = api.check(txt,filename,rpt)
    return json.dumps({'line':rpt.error_line, 'msg':rpt.error_text})

if __name__ == '__main__' and len(sys.argv) > 1:
    resp = check_file(sys.argv[1])
    print(resp)
    # 'Error on line '+resp['line']+': '+resp['msg']
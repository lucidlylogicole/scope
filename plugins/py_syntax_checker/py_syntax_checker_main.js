const path = require('path')

//---o:
let Plugin = {
    js:['py_syntax_checker_ui.js'], // ui javascript to load into app
    css:[],  // css files to load into app
    settings:{
        check_on_save:1,
    },
}

function load(IDE) {
    SyntaxChecker.IDE = IDE
    
    return SyntaxChecker
}


//---o:
let SyntaxChecker = {
    // Backend functions here
    
    parse: function(details) {
        let IDE = SyntaxChecker.IDE
        // console.log('parse',details)
        
        let args = [IDE.path+'/plugins/py_syntax_checker/scope_reporter.py',details.filename]
        
        proc = require('child_process').spawnSync(IDE.settings.paths['python'],args,{
          shell: false, detached: true, encoding : 'utf8' 
        })
        
        console.log(proc.stdout)
        return proc.stdout
    },
    
}

module.exports = {load,Plugin}
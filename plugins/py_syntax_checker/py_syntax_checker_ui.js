
//---o:
let SyntaxChecker = {
    // UI Template object
    
    checkCurrentFile: function() {
        let fD = IDE.currentFile()
        if (fD && fD.path.endsWith('.py')) {
            SyntaxChecker.checkFile(fD.path)
            
        }
    },
    
    checkFile: function(filename) {
        let results = window.electron_api.sendSync('plugin',{plugin:'py_syntax_checker',function:'parse',
            details:{filename:filename}
        })
        results = JSON.parse(results)
        // console.log('R',results)
        
        // Show Error in Statusbar
        if (results.line != null) {
            let txt = 'Error on line '+results.line+': '+ results.msg
            IDE.setStatusBar(txt, 'warning', 5, 1)
        }
    },
    
    checkOnSave:function(event) {
        if (event.detail.filename.endsWith('.py')) {
            SyntaxChecker.checkFile(event.detail.filename)
        }
    },
    
}

function createPlugin() {
    // UI Plugin Startup Code

    IDE.plugins['py_syntax_checker'] = SyntaxChecker

    // Add button to Text tools
    if (IDE.plugins.text_tools != undefined) {
        $P.create({h:'.margin-sm-t',parent:'text_tools_more_container'})
        $P.create({h:'button.scope-btn',text:'Python Syntax Check', 
            onclick:"IDE.plugins['py_syntax_checker'].checkCurrentFile()",parent:'text_tools_more_container'})
    }

    // Check to load on save
    if (IDE.settings.plugins['py_syntax_checker'].check_on_save) {
        window.addEventListener('fileSaved', SyntaxChecker.checkOnSave)
    }

}

createPlugin()
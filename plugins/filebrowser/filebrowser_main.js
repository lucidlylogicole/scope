const {ipcMain,shell} = require('electron')
const path = require('path')

//---o:
let Plugin = {
    version:'2.0.1',
    js:['filebrowser_ui.js'],
    css:['filebrowser.css'],
    settings:{
        ignore_dirs:['__pycache__','.git'],
        ignore_exts:['pyc'],
        color:1,
        open_new_file:1,
        external_exts:['png','jpg','pdf','odt','ods','odp','xlsx','docx','pptx','zip'],
    },
}

function load(IDE) {
    FileBrowser.IDE = IDE
    return FileBrowser
}

//---o:
let FileBrowser = {
    openExternal: function(path) {
        shell.openPath(path)
    },
}

module.exports = {load,Plugin}
//---o:
let FileBrowser = {
    hidden:0,
    cwd:undefined,
    MENU_ITEM:undefined,
    
    //---Events
    toggle: function() {
        $P.toggle('left_window')
        $P.toggle('left_win_splitter')
        if ($P.isVisible('left_window')) {
            $P.class.remove('b_folder','untoggled')
        } else {
            $P.class.add('b_folder','untoggled')
            
        }
    },
    
    focus: function() {
        $P.elm('filebrowser').focus()
    },
    
    click: function(event) {
        if (event.ctrlKey) {
            FileBrowser.selectItem(event.target)
        } else {
            FileBrowser.setActive(event.target)
        }
        
    },
    
    dblclick: function(event) {
        event.stopPropagation()
        FileBrowser.selectItem(event.target)
    },

    //---Navigation
    setActive: function(elm) {
        // Select File or Folder
        if (elm) {
            $P.class.remove($P.query('#fb_dir .active'),'active')
            $P.class.add(elm,'active')
        }
    },

    selectItem: function(elm) {
        
        if (elm === undefined) {
            elm = FileBrowser.getCurrentItem()
            if (elm === undefined) {return}
        }
        
        if ($P.class.has(elm,'fb-folder')) {
            // Expand Folder
            let sub_elm = $P.query1(elm,'.fb-subfolder')
            if (sub_elm.children.length > 0) {
                // Hide
                $P.clear(sub_elm)
                $P.class.remove(elm,'expanded')
            } else {
                $P.class.add(elm,'expanded')
                FileBrowser.loadDirectory($P.attr(elm,'data-path'),sub_elm)
            }
        } else if ($P.class.has(elm,'fb-file')) {
            // Open File
            let file_path = $P.attr(elm,'data-path')
            let ext = file_path.split('.').slice(-1)[0]
            // Check if external
            if (IDE.settings.plugins['filebrowser'].external_exts.includes(ext)) {
                // Launch External
                FileBrowser.MENU_ITEM = elm
                FileBrowser.openExternal(event)
            } else {
                // Open File
                IDE.openFile(file_path)
            }
            
        }
    },
    
    getCurrentItem(select) {
        let cur_item = $P.query1('#fb_dir .active')
        if (cur_item) {
            return cur_item
        } else if (select) {
            // Select First Item
            let cur_item = $P.query1('.fb-folder,.fb-file')
            if (cur_item) {
                FileBrowser.setActive(cur_item)
                return cur_item
            }
        }
    },
    
    prevItem: function() {
        let cur_item = FileBrowser.getCurrentItem(1)
        if (!cur_item) {return}
        
        let new_item = cur_item.previousElementSibling
        if (new_item) {
            FileBrowser.setActive(new_item)
        } else if ($P.class.has(cur_item.parentElement,'fb-subfolder')) {
            FileBrowser.setActive(cur_item.parentElement.parentElement)
        }
    },
    
    nextItem: function() {
        let cur_item = FileBrowser.getCurrentItem(1)
        if (!cur_item) {return}

        let new_item = cur_item.nextElementSibling
        if (new_item) {
            FileBrowser.setActive(new_item)
        } else if ($P.class.has(cur_item.parentElement,'fb-subfolder')) {
            let next_item = cur_item.parentElement.parentElement.nextElementSibling
            FileBrowser.setActive(next_item)
        }
        
    },

    goIntoFolder: function() {
        let cur_item = FileBrowser.getCurrentItem(1)
        if (!cur_item) {return}

        if (!$P.class.has(cur_item,'expanded')) {
            FileBrowser.selectItem(cur_item)
        } else {
            // Select First item
            let first_item = $P.query1(cur_item,'.fb-folder, .fb-file')
            if (first_item) {
                FileBrowser.setActive(first_item)
            }
        }
    },

    goOutofFolder: function() {
        let cur_item = FileBrowser.getCurrentItem(1)
        if (!cur_item) {return}
        
        if ($P.class.has(cur_item.parentElement,'fb-subfolder')) {
            let next_item = cur_item.parentElement.parentElement
            FileBrowser.setActive(next_item)
        }
        
    },

    //---Directory Functions
    loadDirectory: function(dir,parent_element) {
        $P.clear(parent_element)
        if (parent_element == 'fb_dir') {
            FileBrowser.cwd = dir
        }
        let dirD = window.electron_api.sendSync('getDirInfo',{path:dir,hidden:FileBrowser.hidden})
        
        if (dirD === undefined) {
            IDE.setStatusBar('could not load directory.  try reloading','error')
            return
        }
        
        let dir_elms = []
        
        if (!dir.endsWith(IDE.path_delim)) {
            dir += IDE.path_delim
        }
        
        // Load Folders
        for (let fld of dirD.folders) {
            if (!IDE.settings.plugins['filebrowser'].ignore_dirs.includes(fld.name)) {
                dir_elms.push({
                    h:'.fb-folder',text:fld.name, 'data-path':dir+fld.name, c:[{h:'.fb-subfolder'}]
                })
            }
        }
        // Load Files
        for (let fl of dirD.files) {
            if (!IDE.settings.plugins['filebrowser'].ignore_exts.includes(fl.ext)){
                dir_elms.push({
                    h:'.fb-file ext-'+fl.ext,text:fl.name,'data-path':dir+fl.name,
                })
            }
        }
        $P.create(dir_elms,{parent:parent_element})
        
        // Select First item
        let first_item = $P.query1(parent_element,'.fb-folder, .fb-file')
        if (first_item) {
            FileBrowser.setActive(first_item)
        }
        
    },
    
    getItemDirectory: function(item) {
        let dir_path,dir_elm
        if (item === undefined) {item = FileBrowser.MENU_ITEM}
        dir_elm = item

        if (item.id == 'fb_dir') {
            dir_path = FileBrowser.cwd
        } else if ($P.class.has(item,'fb-folder')) {
            dir_elm  = $P.query1(item,'.fb-subfolder')
            dir_path = $P.attr(item,'data-path')
        } else if ($P.class.has(item,'fb-file')) {
            dir_path = IDE.getDirName($P.attr(item,'data-path'))
        }
        return dir_path
    },
    
    
    //---Menu Functions
    reload: function(event) {
        let item_pth = FileBrowser.getItemDirectory()
        
        if (FileBrowser.MENU_ITEM.id == 'fb_dir') {
            FileBrowser.loadDirectory(item_pth,'fb_dir')
        } else if ($P.class.has(FileBrowser.MENU_ITEM,'fb-folder')) {
            let sub_elm = $P.query1(FileBrowser.MENU_ITEM,'.fb-subfolder')
            FileBrowser.loadDirectory(item_pth,sub_elm)
        } else if ($P.class.has(FileBrowser.MENU_ITEM,'fb-file')) {
            FileBrowser.loadDirectory(item_pth,FileBrowser.MENU_ITEM.parentElement)
        }
    },
    
    newFile: function(event) {
        // Get Current Directory
        let pth = FileBrowser.getItemDirectory()
        pth += '/'
        
        // nresp = window.electron_api.sendSync('newFile',{filename:pth,text:'',not_open:1})
        // if (nresp.ok) {
        //     FileBrowser.reload(event)
        // }
        $P.dialog.input({text:'new file name',val:''}).then(resp => {
            if (resp) {
                
                // Check if it exists
                let exists = window.electron_api.sendSync('pathExists',{path:pth+resp})
                if (exists) {
                    $P.dialog.message({text:'The file already exists.'}).then(()=>{
                        FileBrowser.newFile()
                    })
                } else {
                    // Create New file
                    nresp = window.electron_api.sendSync('saveFile',{filename:pth+resp,text:'',not_open:1})
                    if (nresp.ok) {
                        FileBrowser.reload(event)
                        
                        // Open File
                        if (IDE.settings.plugins['filebrowser'].open_new_file) {
                            IDE.openFile(pth+resp)
                        }
                    }
                }
                
            }
        })

    },
    
    newFolder: function(event) {
        // Get Current Directory
        let pth = FileBrowser.getItemDirectory()
        pth += '/'
        
        $P.dialog.input({text:'new folder name',val:''}).then(resp => {
            if (resp) {
                // Create New file
                nresp = window.electron_api.sendSync('newFolder',pth+resp)
                if (nresp.ok) {
                    FileBrowser.reload(event)
                }
                
            }
        })
    },
    
    rename: function(event, current_item) {
        if (current_item === undefined) {
            current_item = FileBrowser.MENU_ITEM
        }
        
        
        if (current_item) {
            // Open File
            let pth = $P.attr(current_item,'data-path')
            if (pth) {
                
                // Check if File Open
                if (IDE.isFileOpen(pth)) {
                    $P.dialog.message({text:'The file is open. Please close before renaming.'})
                    return
                }
                
                // Get new name
                let basename = IDE.getBaseName(pth)
                $P.dialog.input({title:'Rename File',val:basename}).then(resp=>{
                    if (resp !== undefined && resp) {
                        let new_path = IDE.getDirName(pth)+IDE.path_delim+resp
                        
                        // Check if it exists
                        let exists = window.electron_api.sendSync('pathExists',{path:new_path})
                        if (exists) {
                            $P.dialog.message({text:'The file/folder already exists.'})
                        } else {
                            let err = window.electron_api.sendSync('renamePath',{
                                old_path:pth,
                                new_path:new_path,
                            })
                            if (!err) {
                                // Update Filebrowser
                                $P.attr(current_item,'data-path',new_path)
                                $P.elm(current_item).firstChild.nodeValue = resp
                                if ($P.class.has(current_item,'expanded')) {
                                    FileBrowser.reload(event)
                                }
                            } else {
                                IDE.setStatusBar('could not rename file','error')
                            }
                        }
                    }
                })
            }
        }
    },

    delete: function(event) {
        if (FileBrowser.MENU_ITEM) {
            // Open File
            let pth = $P.attr(FileBrowser.MENU_ITEM,'data-path')
            if (pth) {
                
                // Check if File Open
                if (IDE.isFileOpen(pth)) {
                    $P.dialog.message({text:'The file is open. Please close before renaming.'})
                    return
                }
                let basename = IDE.getBaseName(pth)
                
                // Confirm Delete
                $P.dialog.message({text:'Do you want to delete: <br><div class="pad-l clr-orange">'+basename+'</div>',btns:['yes','no','cancel']}).then(resp=>{
                    if (resp == 'yes') {
                        let err = window.electron_api.sendSync('deletePath',pth)
                        if (!err) {
                            $P.remove(FileBrowser.MENU_ITEM)
                        } else {
                            IDE.setStatusBar('could not delete file','error')
                        }
                    }
                })
                
            }
        }
    },

    openExternal: function(event) {
        let pth = FileBrowser.cwd
        if (FileBrowser.MENU_ITEM) {
            // Open File
            let dpth = $P.attr(FileBrowser.MENU_ITEM,'data-path')
            if (dpth){pth = dpth}
        }
        window.electron_api.send('plugin',{plugin:'filebrowser',function:'openExternal',details:pth})
    },

}

//---
function createFileBrowser() {

    // Register output plugin
    IDE.plugins['filebrowser'] = FileBrowser
    IDE.FileBrowser = FileBrowser

    // Filebrowser Button
    let folder_btn = $P.create({h:'button#b_folder.btn-clear', onclick:"FileBrowser.toggle()",
        title:"toggle file browser (F3)", c:[{h:'img',src:'plugins/filebrowser/folder.svg'}],
        parent:'sidebar_plugin_buttons'
    })
    
    let clr_class = ''
    if (IDE.settings.plugins['filebrowser'].color) {
        clr_class = ' show-color'
    }
    
    // Main Filebrowser
    fb_elm = $P.create({
        h:'#filebrowser.flex-col',c:[
            {h:'#fb_dir.flex show_icons'+clr_class, onclick:"FileBrowser.click(event)", 
                ondblclick:"FileBrowser.dblclick(event)", text:'directory'},
        ], tabindex:0,
        parent:'left_window'})
    
    //---Keymapping
    FileBrowser.keymap = $P.keymap('filebrowser')
    FileBrowser.keymap.add('ArrowUp',FileBrowser.prevItem) 
    FileBrowser.keymap.add('ArrowDown',FileBrowser.nextItem)
    FileBrowser.keymap.add('ArrowRight',FileBrowser.goIntoFolder)
    FileBrowser.keymap.add('ArrowLeft',FileBrowser.goOutofFolder)
    FileBrowser.keymap.add('Enter',FileBrowser.selectItem)
    FileBrowser.keymap.add('F2',function(){FileBrowser.rename(event,FileBrowser.getCurrentItem())})
    
    IDE.keymap.add('b',FileBrowser.focus,{alt:1}) 
    
    // Load Current Working Directory
    FileBrowser.loadDirectory(IDE.settings.cwd,'fb_dir')

    //---Filebrowser menu
    $P.create({
        h:'#menu_filebrowser.menu-container', onclick:"$P.menu.close('menu_filebrowser')", c:[
            {h:'.menu-item',text:'Open External', onclick:'FileBrowser.openExternal(event)'},
            {h:'.menu-separator'},
            {h:'.menu-item',text:'New File', onclick:'FileBrowser.newFile(event)'},
            {h:'.menu-item',text:'New Folder', onclick:'FileBrowser.newFolder(event)'},
            {h:'.menu-separator'},
            {h:'#fb_menu_rename.menu-item',text:'Rename', onclick:'FileBrowser.rename(event)'},
            {h:'#fb_menu_delete.menu-item',text:'Delete', onclick:'FileBrowser.delete(event)'},
            {h:'.menu-separator'},
            {h:'.menu-item',text:'Reload', onclick:'FileBrowser.reload(event)'},
        ], parent:fb_elm
    })
    
    fb_elm.addEventListener('contextmenu', event => {
        event.preventDefault()
        FileBrowser.MENU_ITEM = event.target
        
        if ($P.class.has(event.target,'fb-folder') || event.target.id == 'fb_dir') {
            $P.hide('fb_menu_delete')
        } else {
            $P.show('fb_menu_delete')
        }
        if (event.target.id == 'fb_dir') {
            $P.hide('fb_menu_rename')
        } else {
            $P.show('fb_menu_rename')
        }
        $P.menu.show('menu_filebrowser')
    })

}


createFileBrowser()

//---o:
FindFilesPlugin = {
    // loaded:0,
    running:0,
    page_uuid:undefined,
    
    view: function() {
        if (!(FindFilesPlugin.page_uuid in IDE.RightWindow.pages)) {
            // Load 
            let ff_elm = {
                h:'#pg_find_files.page flex-col flex', 'data-display':'flex', c:[
                    {h:'#find_files_input_row.flex-row-c', c:[
                        {h:'input#i_find_files_path.flex-2 scope-input ', placeholder:'path',onkeydown:"if (event.keyCode == 13) {FindFilesPlugin.search()}"},
                    ]},
                    {h:'#find_files_toolbar.flex-row-c',c:[
                        {h:'.scope-btn', text:'&DownTeeArrow;', style:"font-size:1.2rem;", onclick:"FindFilesPlugin.expandAll()", title:'expand all',},
                        {h:'.scope-btn', text:'&UpTeeArrow;', style:"font-size:1.2rem;", onclick:"FindFilesPlugin.collapseAll()", title:'collapse all'},
                        {h:'input#i_find_files_ext.scope-input', style:'width:120px;', placeholder:'ext', value:IDE.settings.plugins['find_files'].default_ext,
                            onkeydown:"if (event.keyCode == 13) {FindFilesPlugin.search()}"},
                        {h:'input#i_find_files_search.flex scope-input ', placeholder:'search term',onkeydown:"if (event.keyCode == 13) {FindFilesPlugin.search()}"},
                        {h:'#b_find_files_search.scope-btn',text:'search', onclick:"FindFilesPlugin.searchClick()"},
                    ]},
                    {h:'#find_files_output.flex pad show-color', 'text':''},
                ]
                
            }
            FindFilesPlugin.page_uuid = IDE.RightWindow.addPage({content:ff_elm,iframe:0})
            
            window.addEventListener('workspaceOpened', (event)=>{
                $P.val('i_find_files_path',event.detail.path)
            })
            
            $P.val('i_find_files_path',IDE.CWD)
            
            // FindFilesPlugin.loaded = 1
            
        } 
        // Toggle
        IDE.RightWindow.showPage(FindFilesPlugin.page_uuid)
    },
    
    toggle: function() {
        if (!(FindFilesPlugin.page_uuid in IDE.RightWindow.pages)) {
                FindFilesPlugin.view()
        } else {
            if (!$P.isVisible(IDE.RightWindow.pages[FindFilesPlugin.page_uuid ].element)) {
                FindFilesPlugin.view()
                
            } else {
                IDE.RightWindow.toggle()
            }
        }
    },
    
    searchClick:function() {
        if (FindFilesPlugin.running) {
            // Stop
            FindFilesPlugin.stop()
            $P.elm('b_find_files_search').innerText = 'search'
        } else {
            $P.elm('b_find_files_search').innerText = 'stop'
            // Search
            FindFilesPlugin.search()
        }
    },
    
    search: function() {
        $P.clear('find_files_output')
        $P.elm('b_find_files_search').innerText = 'stop'
        IDE.setStatusBar('searching for files...',undefined,0)
        FindFilesPlugin.running = 1
        window.electron_api.send('plugin',{plugin:'find_files',function:'search',
            details:{
                path:$P.val('i_find_files_path'),
                ext:$P.val('i_find_files_ext'),
                search_text:$P.val('i_find_files_search'),
            }})
    },
    
    done: function() {
        $P.elm('b_find_files_search').innerText = 'search'
        FindFilesPlugin.running = 0
        IDE.setStatusBar('done searching!',undefined,1)
    },
    
    stop: function() {
        FindFilesPlugin.running = 0
        IDE.setStatusBar('stop searching!',undefined,1)
        $P.elm('b_find_files_search').innerText = 'search'
        window.electron_api.send('plugin',{plugin:'find_files',function:'stop'})
    },
    
    addResult: function(outputD) {
        
        let lines = []
        for (let line of outputD.lines) {
            lines.push({h:'.find_files-line-row', c:[
                {h:'.find_files-line-cnt', text:line.no, onclick:`IDE.openFile(String.raw\`${outputD.filepath}\`, ${line.no})`},
                {h:'.find_files-line-text', text:$P.sanitize(line.text.trim().slice(0,200),'html')},
            ]})
        }
        
        let ext =  'ext-' + outputD.filepath.split('.').slice(-1)[0]
        
        $P.create({
            h:'.find_files-item', c:[
                {h:'.flex-row-c', c:[
                    {h:'.find_files-expand grad-text', text:'&colone;', onclick:"FindFilesPlugin.toggleFileExpand(this)", title:'show/hide lines of code'},
                    {h:'.find_files-file flex '+ext, text:outputD.filename, onclick:`IDE.openFile(String.raw\`${outputD.filepath}\`)`},
                    {h:'.find_files-cnt',text:outputD.lines.length},
                ]}, 
                {h:'.find_files-line-container', c:lines},
                
            ],
            parent:'find_files_output'})
    },
    
    
    expandAll: function() {
        $P.class.add($P.query('#pg_find_files .find_files-line-container '),'active')
    },
    
    collapseAll: function() {
        $P.class.remove($P.query('#pg_find_files .find_files-line-container '),'active')
    },
    
    toggleFileExpand: function(item) {
        $P.class.toggle($P.query($P.parent(item).parentElement,'.find_files-line-container'),'active')
    },
    
}


function createFindFiles() {
    // Add Folder Button
    let ff_btn = $P.create({h:'button#b_find_files.btn-clear', onclick:"FindFilesPlugin.view()",
        title:"search in files", c:[{h:'img',src:'plugins/find_files/find_files.svg'}],
        parent:'sidebar_plugin_buttons'
    })
    
    IDE.keymap.add('f',FindFilesPlugin.toggle,{alt:1}) 
    
    IDE.plugins['find_files'] = FindFilesPlugin
    
    
}


createFindFiles()
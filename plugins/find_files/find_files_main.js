const path = require('path')
const fs = require('fs')

//---o:
let Plugin = {
    js:['find_files_ui.js'],
    css:['find_files.css'],
    settings:{
        ignore_dirs:["__pycache__",".git"],
        default_ext:'py,js,html',
    }
}

function load(IDE) {
    FindFiles.IDE = IDE
    return FindFiles
}

let stop_find_files = 0

function walk(dirpath, options) {
    if (options === undefined) {options = {}}
    let walkD = {files:[], dirs:[]}
    if (!stop_find_files) {
        fs.readdirSync(dirpath).forEach(item =>{
            if (stop_find_files){return}
            let item_path = path.join(dirpath, item)
            let item_stat
            try {
                item_stat = fs.statSync(item_path)
            } catch(e){}
            if (item_stat && item_stat.isDirectory()) {
                
                if (!FindFiles.IDE.settings.plugins['find_files'].ignore_dirs.includes(item)) {
                    
                    // Directory
                    if (options.dir_callback !== undefined) {
                        options.dir_callback(item_path)
                    }
                    walkD.dirs.push({name:item,children:walk(item_path, options)})
                }
            } else {
                // File
                if (options.file_callback !== undefined) {
                    options.file_callback(item_path)
                }
                walkD.files.push(item)
            }
        })
    }
    return walkD
}

//---o:
let FindFiles = {
    CWD:undefined,
    stop:0,
    
    search: function(details) {
        let dir_path = details.path
        FindFiles.CWD = dir_path
        let ext = details.ext
        let search_text = details.search_text
        stop_find_files = 0
        
        if (fs.existsSync(dir_path)) {
            
            walk(dir_path, {file_callback:function(filepath){
                FindFiles.searchFile(filepath,{text:search_text,ext:ext})
            }})
            
        }
        FindFiles.IDE.win.webContents.send("plugin", {plugin:'find_files', function:'done'}) 
        
    },
    
    stop: function() {
        stop_find_files = 1
    },
    
    searchFile: function(filename, options) {
        if (stop_find_files) {return}
        
        // Check File Extension
        if (options.ext != '') {
            let exts = options.ext.split(',')
            let file_ext = path.extname(filename).slice(1)
            if (!exts.includes(file_ext)) {
                return
            }
        }
        
        fs.readFile(filename,'utf8', (err, text)=>{
            
            let found_lines = []
            let cnt = 0

            // Check filecontent
            if (text) {
                let lines = text.split(/\r?\n/)
                for (let line of lines) {
                    cnt++
                    if (line.toLowerCase().includes(options.text.toLowerCase())){
                        found_lines.push({no:cnt,text:line})
                    }
                }
                
            }
            
            // Check filename (if not found)
            if (found_lines.length == 0) {
                if (path.basename(filename).toLowerCase().includes(options.text.toLowerCase())) {
                    found_lines.push({no:cnt,text:'(filename)'})
                }
            }
            if (found_lines.length > 0) {
                FindFiles.IDE.win.webContents.send("plugin", {plugin:'find_files', function:'addResult', 
                    details:{filepath:filename, filename:path.relative(FindFiles.CWD,filename),lines:found_lines}})
            }
        })

    },
    
}

module.exports = {load,Plugin}
// Version 2.11.0    Copyright 2021-2024 pebble   Released under the MIT License

//---Window Onload
window.addEventListener('load',function(event) {
    // Load Page if hash
    if (location.hash) {
        try {
            let pg = document.querySelector(`#${location.hash.slice(1)}.page`)
            if (pg) {$P.page.show(pg)}
        } catch(e){}
    }
    
    // Set Table Sortable
    for (let tbl of Array.from($P.query('table.sortable'))) {
        $P.table.setSortable(tbl)
    }
    
    // Create Spinners
    for (let spinner of Array.from($P.query('.spinner-container'))) {
        $P.spinner({parent:spinner})
    }
})


//---
//---$P
let $P = {
    // options: {
    //     hashchange:false,
    //     pages:true,
    // },

    //---Element Functions
    elm: function(elm_or_id) {
        if (typeof elm_or_id == 'string') {
            return document.getElementById(elm_or_id)
        } else {
            return elm_or_id
        }
    },

    iterElements: function(elm_or_id_or_arr, func) {
        if ($P.isElementList(elm_or_id_or_arr)) {
            for (let p=0; p < elm_or_id_or_arr.length; p++) {
                func($P.elm(elm_or_id_or_arr[p]))
            }
        } else {
            let elm = $P.elm(elm_or_id_or_arr)
            if (elm) {func(elm)}
        }
    },
    
    isElementList: function(element_list) {
        return element_list.constructor === Array || element_list.constructor === NodeList
    },
    
    query: function(elm_or_id,qry) {
        let elm = document
        if (qry === undefined) {
            qry = elm_or_id
        } else if (elm_or_id !== undefined) {
            elm = $P.elm(elm_or_id)
        }
        return Array.from(elm.querySelectorAll(qry))
    },
    
    query1: function(elm_or_id, qry) {
        let elm = document
        if (qry === undefined) {
            qry = elm_or_id
        } else if (elm_or_id !== undefined) {
            elm = $P.elm(elm_or_id)
        }
        return elm.querySelector(qry)
    },
    
    parent: function(elm_or_id) {
        return $P.elm(elm_or_id).parentElement
    },
    
    children: function(elm_or_id) {
        return Array.from($P.elm(elm_or_id).children)
    },
    
    append: function(elm_or_id,children) {
        let par_elm = $P.elm(elm_or_id)
        $P.iterElements(children, function(elm){
            par_elm.appendChild(elm)
        })
    },
    
    remove: function(elm_or_id) {
        $P.iterElements(elm_or_id, function(elm){
            if (elm !== undefined && elm.parentNode != undefined) {
                elm.parentNode.removeChild(elm)
            }
        })
    },

    clear: function(elm_or_id) {
        $P.iterElements(elm_or_id, function(elm){
            while (elm.firstChild) {
                elm.removeChild(elm.firstChild)
            }
        })
    },

    exists: function(elm_or_id) {
        return $P.elm(elm_or_id) !== null
    },

    //---Create
    create: function(elements,options) {
        
        if (typeof(elements) == 'string') {
            elements = [{h:elements}]
        } else if (elements.constructor !== Array) {
            elements = [elements]
        }
        if (options === undefined) {
            options = {}
        } else if (options.constructor !== Object) {
            options = {parent:options}
        }
        
        let element_list = []
        for (let e=0; e < elements.length; e++) {
            let element = elements[e]
            let tag,attr={},parent
            
            for (let ky in element) {
                if (ky == 'tag') {
                    tag = element.tag
                } else if (ky == 'parent') {
                    parent = element.parent
                } else if (ky == 'h') {
                    // Hypertext like
                    let cl_spl = element.h.split('.')
                    let id_spl = cl_spl[0].split('#')
                    let tg_spl = id_spl[0].trim()
                    if (tg_spl) {
                        element.tag = tg_spl
                    }
                    if (id_spl.length > 1) {
                        attr.id = id_spl[1].trim()
                    }
                    if (cl_spl.length > 1) {
                        attr.class = cl_spl.slice(1).join('.')
                    }
                } else if (ky == 'c') {
                    // Replace c key with children
                    delete Object.assign(element, {'children': element['c']}).c 
                } else if (ky == 'children') {
                    // ignore for now
                } else if (ky == 'e' || ky == 'events') {
                    // ignore events
                    if (ky == 'e') {
                        delete Object.assign(element, {'events': element['e']}).e
                    }
                } else if (element[ky] !== undefined) {
                    attr[ky] = element[ky]
                }
            }
            
            // Create Element
            if (element.tag === undefined || element.tag === null) {
                element.tag='div'
            }
            
            // Element
            let elm
            if (options.svg || element.tag=='svg') {
                options.svg = 1
                elm = document.createElementNS("http://www.w3.org/2000/svg", element.tag)
            } else {
                elm = document.createElement(element.tag)
            }
            
            // Add Attributes
            for (let ky in attr) {
                if (ky == 'innerHTML') {
                    elm.innerHTML += attr.innerHTML
                } else if (ky == 'text') {
                    elm.innerHTML += attr.text
                } else if (ky == 'innerText') {
                    elm.innerText += attr.text
                } else {
                    elm.setAttribute(ky,attr[ky])
                }
            }

            // Add Event Listeners
            if ('events' in element) {
                for (let eky in element.events) {
                    elm.addEventListener(eky,element.events[eky])
                }
            }

            // Add Children
            if ('children' in element && element['children'] != undefined) {
                for (let c=0; c < element.children.length; c++) {
                    if (element.children[c] instanceof Element) {
                        elm.appendChild(element.children[c])
                    } else {
                        element.children[c].parent = elm
                        $P.create([element.children[c]],options)
                    }
                }
            }
            
            element_list.push(elm)
            
            // Add to Parent
            let par_elm
            if (element.parent !== undefined && element.parent !== null) {
                par_elm = element.parent
            } else if (options.parent !== undefined) {
                par_elm = options.parent
            }
            if (par_elm !== undefined && par_elm !== null) {
                $P.elm(par_elm).appendChild(elm)
            }
        }
        
        if (element_list.length == 1) {
            return element_list[0]
        } else {
            return element_list
        }
        
    },

    //---Element Attributes
    attr: function(elm_or_id,atrb,val) {
        let elm = $P.elm(elm_or_id)
        if ($P.isObject(atrb)) {
            // Object, set multiple attributes
            for (let ky in atrb) {
                elm.setAttribute(ky,atrb[ky])
            }
        } else if (val === undefined) {
            // Get
            return elm.getAttribute(atrb)
        } else {
            // Set
            elm.setAttribute(atrb,val)
            return elm
        }
    },
    
    html: function(elm_or_id,txt) {
        if (txt === undefined) {
            return $P.elm(elm_or_id).innerHTML
        } else {
            $P.iterElements(elm_or_id, function(elm){
                elm.innerHTML=txt
            })
        }
    },

    text: function(elm_or_id,txt) {
        if (txt === undefined) {
            return $P.elm(elm_or_id).innerText
        } else {
            $P.iterElements(elm_or_id, function(elm){
                elm.innerText=txt
            })
        }
    },

    val: function(elm_or_id,txt) {
        let main_elm = $P.elm(elm_or_id)
        if (['input','select','textarea','button'].indexOf(main_elm.tagName.toLowerCase()) > -1) {
            // Single Input
            if (txt === undefined) {
                // Return Value
                let v = main_elm.value
                if (main_elm.type == 'number') {
                    if (v == '') {v=null} else {v=v*1}
                } else if (main_elm.type == 'checkbox') {
                    v = main_elm.checked
                } else if (main_elm.type == 'radio') {
                    v = main_elm.checked
                } else if (main_elm.type.startsWith('select') && main_elm.multiple) {
                    // get multiple values
                    v = []
                    for (let opt_elm of main_elm.children) {
                        if (opt_elm.selected) {
                            v.push(opt_elm.value)
                        }
                    }
                }
                return v
            } else {
                // Set Value
                if (main_elm.type == 'checkbox' || main_elm.type == 'radio') {
                    main_elm.checked = txt
                } else if (main_elm.type.startsWith('select') && main_elm.multiple && Array.isArray(txt)) {
                    // set multiple
                    $P.query(elm_or_id,'option').forEach(sel_elm=>{
                        let opt_ok = 0
                        let attr_val = $P.attr(sel_elm,'value')
                        if (attr_val !== undefined) {
                            if (txt.indexOf(attr_val) > -1) {
                                opt_ok = 1
                            }
                        } else if (txt.indexOf($P.text(sel_elm)) > -1) {
                            opt_ok = 1
                        }
                        if (opt_ok) {
                            $P.attr(sel_elm,'selected','selected')
                        } else {
                            sel_elm.removeAttribute('selected')
                        }
                    })
                    
                } else {
                    main_elm.value = txt
                }
            }
        } else {
            // Return Multiple values
            let valD = {}
            let in_elms = $P.query(elm_or_id,'input,textarea,select,button')
            for (let i=0; i < in_elms.length; i++) {
                let in_elm = in_elms[i]
                if ($P.attr(in_elm,'key') !== null) {
                    valD[$P.attr(in_elm,'key')]=$P.val(in_elm)
                } else if (in_elm.type == 'radio') {
                    if (in_elm.checked) {
                        valD[in_elm.name]=in_elm.value
                    }
                } else if (in_elm.id != '' && ['button','submit','reset'].indexOf((''+in_elm.type).toLowerCase()) <0) {
                    valD[in_elm.id]=$P.val(in_elm)
                }
            }
            return valD
        }
    },

    //---Display
    toggle: function(elm_or_id,show) {
        $P.iterElements(elm_or_id, function(elm){
            if (show === undefined) {
                if ($P.class.has(elm,'hidden')) {
                    $P.show(elm)
                } else {
                    $P.hide(elm)
                }
            } else {
                if (show) {
                    $P.show(elm)
                } else {
                    $P.hide(elm)
                }
            }
        })
    },

    show: function(elm_or_id) {
        $P.class.remove(elm_or_id,'hidden')
    },
    
    hide: function(elm_or_id) {
        $P.class.add(elm_or_id,'hidden')
    },
    
    // display: function(elm_or_id,display) {
    //     if (display == 0) {display = 'none'}
    //     else if (display == 1) {display = 'block'}
    //     $P.iterElements(elm_or_id, function(elm){
    //         elm.style.display = display
    //     })
    // },
    
    isVisible: function(elm_or_id) {
        let elm = $P.elm(elm_or_id)
        return !(getComputedStyle(elm).display == 'none' || elm.getClientRects().length === 0 || elm.clientHeight === 0)
        // return !!( elm.offsetWidth || elm.offsetHeight || elm.getClientRects().length )
    },

    setVisible: function(elm_or_id,visible) {
        if (visible === undefined) {visible = 1}
        // $P.class.toggle(elm_or_id,'hidden',!visible)
        if (visible) {
            $P.class.remove(elm_or_id,'hidden')
        } else {
            $P.class.add(elm_or_id,'hidden')
        }
    },

    setEnabled: function(elm_or_id,enable) {
        if (enable === undefined) {enable = 1}
        // $P.class.toggle(elm_or_id,'disabled',!enable)
        if (enable) {
            $P.class.remove(elm_or_id,'disabled')
        } else {
            $P.class.add(elm_or_id,'disabled')
        }
    },

//---Class Functions
    //---o:
    class: {
        add: function(elm_or_id,cls) {
            $P.iterElements(elm_or_id,function(elm){
                try {
                    elm.classList.add(cls)  // future
                } catch(e) {
                    elm.className += ' '+cls
                }
            })
        },
        
        has: function(elm_or_id,cls) {
            return $P.elm(elm_or_id).classList.contains(cls)
        },
        
        set: function(elm_or_id,cls) {
            $P.iterElements(elm_or_id,function(elm){
                elm.className = cls
            })
        },
        
        toggle: function(elm_or_id,cls) {
            $P.iterElements(elm_or_id,function(elm){
                if ($P.class.has(elm,cls)) {
                    $P.class.remove(elm,cls)
                } else {
                    $P.class.add(elm,cls)
                }
            })
        },
        
        remove: function(elm_or_id,cls) {
            let re = new RegExp(cls,"g")
            $P.iterElements(elm_or_id,function(elm){
                try {
                    elm.classList.remove(cls) 
                } catch(e) {
                    elm.className.replace(new RegExp('(?:^|\\s)'+ cls + '(?:\\s|$)'), ' ')
                }
            })
        },
    },

    //---Events
    listen: function(elm_or_id,event,func) {
        $P.iterElements(elm_or_id,function(elm){
            elm.addEventListener(event, func)
        })
        if (!$P.isElementList(elm_or_id)) {
            return $P.elm(elm_or_id)
        }
    },
    
    unlisten: function(elm_or_id,event,func) {
        // note: can't remove event listeners from anonymous functions
        $P.iterElements(elm_or_id,function(elm){
            elm.removeEventListener(event, func)
        })
        if (!$P.isElementList(elm_or_id)) {
            return $P.elm(elm_or_id)
        }
    },
    
    sendEvent: function(elm_or_id, event_name, detail, options) {
        if (options === undefined) {options = {}}
        options.detail = detail
        let evt = new CustomEvent(event_name,options)
        $P.elm(elm_or_id).dispatchEvent(evt)
        return evt
    },

    promise: function() {
        // Get a promise to use resolve (accept) outside of promise
        let _resolve, _reject
        let promise = new Promise(function(resolve,reject){
            _resolve = resolve
            _reject = reject
        })
        
        promise.accept = function(value) {
            _resolve(value)
        }
        promise.cancel = function(value) {
            _reject(value)
        }
        
        return promise
    },

//---
//---Dialog
    //---o:
    dialog: {
        dialog: function(options) {
            if (options === undefined) {options = {}}
            let Dialog = {
                options:options,
                build:function(opt){
                    if (opt !== undefined){Dialog.options = opt}
                    if (Dialog.elm !== undefined){$P.remove(Dialog.elm)}
                    Dialog.elm = $P.dialog.build(Dialog.options)
                    // $P.class.set(Dialog.elm,'dlg-bg')
                },
                show:function(){
                    if (Dialog.elm === undefined) {
                        Dialog.build()
                    }
                    $P.dialog.show(this.elm)
                },
                close:function(){
                    $P.dialog.close(this.elm)
                    Dialog.elm = undefined
                },
            }
            
            if (options.show) {
                Dialog.show()
            }

            return Dialog
        },
        
        build: function(options) {
            
            // Dialog Elements
            let dlg_elm = {h:'.dlg-bg-temp', c:[
                {h:'.dlg-frame',c:[]}
            ]}

            // Size
            if (options.size) {
                dlg_elm.c[0].h='.dlg-frame-'+options.size
            }
            
            // Dialog Title
            dlg_elm.c[0].c.push({h:'.dlg-title-frame',c:[
                {h:'.dlg-title',text:options.title},
            ]})
            if (options.title === undefined) {
                dlg_elm.c[0].c[0].h='.dlg-no-title-frame'
            }
            
            // Closable
            if (options.close) {
                dlg_elm.onclick = '(function (evt){if ($P.class.has(evt.target,"dlg-bg-temp") || $P.class.has(evt.target,"dlg-bg")){$P.dialog.close(evt.target)}})(event)'
                dlg_elm.c[0].c[0].c.push(
                    {h:'.btn-close',
                        onclick:'$P.dialog.close(event.target)',
                        title:'close',
                        // text:'&times;',
                    }
                )
            }
            
            // Content
            if (options.content) {
                let content_class = ''
                if (options.content_class){content_class = options.content_class}
                dlg_elm.c[0].c.push({h:'.dlg-content '+content_class,c:[options.content]})
            }
            
            // Buttons
            if (options.buttons) {
                dlg_elm.c[0].c.push({h:'.dlg-bottom',c:options.buttons})
            }
            
            // Create
            let elm = $P.create(dlg_elm)
            document.body.appendChild(elm)

            return elm
            
        },
       
        show: function(elm_or_id) {
            let dlg_elm = $P.elm(elm_or_id)
            $P.class.add(dlg_elm,'active')
            $P.class.remove(dlg_elm,'hidden')
        },
        
        close: function(current_dialog) {
            let bkgd_elm = $P.elm(current_dialog)
            if (current_dialog!== undefined && $P.class.has(bkgd_elm,'dlg-bg')) {
                $P.class.remove(bkgd_elm,'active')
                return
            }
            if (current_dialog !== undefined) {
                let bkgd = bkgd_elm.closest(".dlg-bg-temp")
                if (bkgd) {
                    $P.remove(bkgd)
                } else {
                    bkgd = bkgd_elm.closest(".dlg-bg")
                    $P.class.remove(bkgd,'active')
                }
            } else {
                // Close All
                $P.iterElements($P.query('.dlg-bg-temp'),(elem)=>{
                    elem.parentNode.removeChild(elem)
                })
                $P.class.remove($P.query('.dlg-bg.active'),'active')
            }
        },
    
        message: function(options) {
            if (options === undefined) {options = {}}
            let opt = options  // Build options
            
            // Backwards compatibility
            if (options.btns) {options.buttons = options.btns}
            if (options.msg) {options.text = options.msg}
            
            if (options.buttons === undefined) {
                options.buttons = ['ok']
                // opt.close = true
            }
            
            let dlg_doneP = $P.promise()
            dlg_doneP.close = function() {
                this.accept()
                $P.dialog.close(event.target)
            }
            let btn_elements = []
            for (let i=0; i < options.buttons.length; i++) {
                btn_elements.push($P.create({h:'button.btn',text:options.buttons[i]}))
                btn_elements[i].onclick = function(event){
                    
                    // Check for beforeClose function
                    let ok_close = 1
                    if (options.beforeClose !== undefined) {
                        ok_close = options.beforeClose(event.target.textContent)
                    }
                    if (ok_close) {
                        dlg_doneP.accept(event.target.textContent)
                        $P.dialog.close(event.target)
                    }
                }
            }
            
            if (options.text) {
                opt.content = {text:options.text}
            } else if (options.content) {
                opt.content = options.content
            }
            opt.buttons = btn_elements
            $P.dialog.build(opt)

            // Set focus to first button
            if (btn_elements.length > 0) {
                btn_elements[0].focus()
            }

            return dlg_doneP
        },
        
        input: function(options) {
            let val = ''
            if (options === undefined) {options = {}}
            if (options.val !== undefined) {val=options.val}
            if (options.msg !== undefined) {options.text=options.msg}
            if (options.text === undefined) {options.text=''}
            
            let dlg_id = $P.uuid(0)
            
            options.content = {
                h:'input#i_'+dlg_id+'.pebble wide',type:"text",
                events:{
                    keydown:function(event) {
                        if (event.keyCode == 13) {
                            event.preventDefault()
                            document.getElementById('b_ok_'+dlg_id).click()
                        }
                    }
                },
                style:"margin-top:10px;font-size:1.1rem;",value:val
            }
            
            if (options.text) {
                options.content = {c:[{text:options.text},options.content]}
            }
            
            let dlg_doneP = $P.promise()
            
            options.buttons = []
            // Ok 
            options.buttons.push($P.create({h:'button#b_ok_'+dlg_id+'.btn btn-dlg-ok',text:'ok'}))
            options.buttons[0].onclick = function(){
                dlg_doneP.accept(document.getElementById("i_"+dlg_id).value)
                $P.dialog.close(event.target)
            }
            // Cancel
            options.buttons.push($P.create({h:'button.btn',text:'cancel'}))
            options.buttons[1].onclick = function(event){
                dlg_doneP.accept()
                $P.dialog.close(event.target)
            }
            $P.dialog.build(options)
            // Focus and Set Cursor to end
            $P.elm("i_"+dlg_id).focus()
            $P.val("i_"+dlg_id,'')
            $P.val("i_"+dlg_id,val)
            
            return dlg_doneP
            
        },
        
        // List Select
        list: function(options) {
            options.content = {c:[]}
            if (options.list !== undefined) {options.items = options.list}
            
            if (options.text !== undefined && options.text != '') {
                options.content.c.push({h:'.dlg-content pad-b',text:options.text})
            }
            
            let dlg_doneP = $P.promise()
            let list_elm = {h:'.li-item-container ',c:[]}
            
            for (let i=0; i < options.items.length; i++) {
                
                let val = options.items[i]
                let txt = options.items[i]
                if (Array.isArray(options.items[i]) && options.items[i].length > 1) {
                    val = options.items[i][1]
                    txt = options.items[i][0]
                }
                
                list_elm.c.push({h:'.li-item-h sel',val:val,text:txt})
            }
            
            // Add list and setup onlick
            options.content.c.push($P.create(list_elm))
            options.content.c.slice(-1)[0].onclick = function(event) {
                dlg_doneP.accept(event.target.getAttribute('val'))
                $P.dialog.close(event.target)
            }
            
            options.buttons = []
            // Cancel
            options.buttons.push($P.create({h:'button.btn',text:'cancel'}))
            options.buttons[0].onclick = function(event){
                dlg_doneP.accept()
                $P.dialog.close(event.target)
            }

            $P.dialog.build(options)
            
            return dlg_doneP
        },
        
        image: function(options) {
            let dlg_doneP = $P.promise()
            dlg_doneP.close = function() {
                this.accept()
                $P.dialog.close(event.target)
            }
            
            // options.frame_class='dlg-frame-none'
            options.size='none'
            options.close=true
            options.content = {h:'img.dlg-img',src:options.src}
            if (options.element !== undefined) {
                options.content = {h:'img.dlg-img',src:$P.attr(options.element,'src')}
            }

            dlg_elm = $P.dialog.build(options)
            return dlg_doneP
        },

        spinner: function(options) {
            if (options === undefined){options = {}}
            let msg = options.text
            if (options.show == undefined) {options.show=1}
            
            options.content = {h:'.align-c',c:[
                $P.spinner(),
                {h:'h3.dlg-loading-msg pebble',style:'opacity:0.7;',text:msg}
            ]}
            
            let spin_dlg = $P.dialog.dialog(options)
            spin_dlg.setText = function(txt) {
                $P.text($P.query(spin_dlg.elm,'.dlg-loading-msg'),txt)
            }
            
            return spin_dlg
        },
        
    },

//---
//---Pages and Tabs
    //---o:
    page: {
        show: function(elm_or_id,options) {
            let pg_elm = $P.elm(elm_or_id)
            if (pg_elm) {
                $P.class.remove($P.query(pg_elm.parentElement,':scope > .page.active'),'active')
                $P.class.add(pg_elm,'active')
                $P.class.remove(pg_elm,'hidden')
                
                if (options === undefined) {options = {}}
                if (options.set_hash) {window.location.hash='#'+pg_elm.id}
                if (options.scroll) {pg_elm.scrollIntoView(true)}
            }
        },
    },

    //---o:
    tab: {
        click: function(event,options) {
            
            if (options === undefined) {options = {}}
            
            // Close Button Clicked
            if ($P.class.has(event.target,'btn-close')) {
                options.tab_elm = event.target.parentElement
                $P.tab.close(undefined,options)
            } else {
            
                // Find Tab
                let tab_elm = event.target
                if ($P.class.has(event.target.parentElement,'tab')) {
                    tab_elm = event.target.parentElement
                }
    
                // Check for actual tab 
                if (!($P.class.has(tab_elm,'tab'))) {
                    return
                }
                options.tab_elm = tab_elm
                $P.tab.show(undefined, options)
            }
            
        },
        show: function(tab_id, options) {
            
            if (options === undefined) {options = {}}
            
            let tab_elm
            if (options.tab_elm) {
                tab_elm = $P.elm(options.tab_elm)
                tab_id = tab_elm.getAttribute('tab-id')
            } else {
                tab_elm = $P.tab.tab(tab_id,options.parent)
            }
            let tab_cntr = tab_elm.parentElement.parentElement
            
            // Page Visibility
            let pages = tab_cntr.querySelectorAll(':scope > .tab-pages')[0].querySelectorAll(':scope > .tab-page')
            let sel_pages = tab_cntr.querySelectorAll(':scope > .tab-pages')[0].querySelectorAll(':scope > .tab-page[tab-id="'+tab_id+'"]')

            // Hide Pages
            for (let i = 0; i < pages.length; i++) {
                pages[i].classList.remove("active")
            }

            // Show Page
            if (sel_pages.length > 0) {
                for (let i = 0; i < sel_pages.length; i++) {
                    sel_pages[i].classList.add("active")
                }
            }
            
            // Tab Visibility
            let tabs, sel_tabs
            try {
                tabs = tab_cntr.querySelectorAll(':scope > .tab-bar')[0].querySelectorAll(':scope > .tab')
                sel_tabs = tab_cntr.querySelectorAll(':scope > .tab-bar')[0].querySelectorAll(':scope > .tab[tab-id="'+tab_id+'"]')
            } catch(e) {
                tabs = tab_cntr.querySelectorAll('.tab')
                sel_tabs = tab_cntr.querySelectorAll('.tab[tab-id="'+tab_id+'"]')
            }
            
            if (sel_tabs.length > 0) {
            
                // Hide Pages
                for (let i = 0; i < tabs.length; i++) {
                    tabs[i].classList.remove("active")
                }
                // Show Page
                for (let i = 0; i < sel_tabs.length; i++) {
                    sel_tabs[i].classList.add("active")
                }
            }
            
            // Tab Change
            if (options.tabChange) {
                options.tabChange(tab_id)
            }

        },
        close: function(tab_id,options) {
            if (options === undefined) {options = {}}  // default options
            let tab_elm
            if (options.tab_elm) {
                tab_elm = $P.elm(options.tab_elm)
                tab_id = tab_elm.getAttribute('tab-id')
            } else {
                tab_elm = $P.tab.tab(tab_id,options.parent)
            }
            
            let tab_cntr = tab_elm.parentElement.parentElement
            let tab_bar = tab_elm.parentElement
                
            let ok = 1
            // Check Before Close
            if (options.beforeClose) {
                ok = options.beforeClose(event.target.parentElement)
            }
            
            if (ok) {
                    
                // Get closing tab location
                let close_tab_array_ind = -1
                let cur_tab
                let tabs = $P.query(tab_bar,':scope > .tab')
                for (let ti=0; ti < tabs.length; ti++) {
                    if ($P.attr(tabs[ti],'tab-id') == tab_id) {
                        close_tab_array_ind = ti
                    } else if ($P.class.has(tabs[ti],'active')) {
                        cur_tab = tabs[ti]
                    }
                }
                
                // Remove Page
                let sel_pages
                try {
                    sel_pages = tab_cntr.querySelectorAll(':scope > .tab-pages')[0].querySelectorAll(':scope > .tab-page[tab-id="'+tab_id+'"]')
                } catch(e) {
                    sel_pages = tab_cntr.querySelectorAll('.tab-page[tab-id="'+tab_id+'"]')
                }
                
                if (sel_pages.length > 0) {
                    $P.remove(sel_pages[0])
                }
                delete sel_pages
                
                // Remove Tab
                $P.remove(tab_elm)
                $P.array.remove(tabs,tab_elm)
                delete tab_elm
                
                // Select another tab
                if (cur_tab) {
                    cur_tab.click()
                } else {
                    // Select nearest tab if current one not valid
                    if (tabs.length > 0) {
                        let new_ind = Math.min(tabs.length-1,close_tab_array_ind)
                        new_ind = Math.max(0,new_ind)
                        tabs[new_ind].click()
                    }
                }
                
                // Call afterClose callback
                if (options.afterClose) {
                    options.afterClose()
                }
            }
        },
        
        add: function(options) {
            // {parent, title, icon, close, page}
            let tab_container = $P.elm(options.parent)
            let tab_id = options.id
            if (tab_id === undefined) {tab_id = $P.uuid()}
            // Tab Element
            let tab_items = []
            if (options.icon) {tab_items.push({h:'img',src:options.icon})}
            if (options.title) {tab_items.push({h:'span',text:options.title})}
            if (options.close) {tab_items.push({h:'button.btn-close'})}
            let tab_elm = $P.create({h:'.tab', 'tab-id':tab_id, c:tab_items})
            
            // Add tab after specified tab or last tab
            let after_elm = options.after
            if (after_elm === undefined) {
                // Check for tab space
                let tab_spc = $P.query1(tab_container,':scope > .tab-bar .tab-bar-space')
                if (tab_spc) {
                    after_elm = tab_spc
                } else {
                    let tab_elms = $P.query(tab_container,':scope > .tab-bar .tab')
                    if (tab_elms.length ==0) {
                        after_elm = undefined
                    } else {
                        after_elm = tab_elms[tab_elms.length-1].nextSibling
                    }
                }
            } else {
                after_elm = after_elm.nextSibling
            }
            if (after_elm !== undefined) {
                $P.query1(tab_container,'.tab-bar').insertBefore(tab_elm,after_elm)
            } else {
                $P.query1(tab_container,'.tab-bar').appendChild(tab_elm)
            }
            
            // Page Element
            let page_item
            if (options.page) {
                // add specified page
                options.page.parent=$P.query1(tab_container,'.tab-pages')
                let pg_elm = $P.create(options.page)
                $P.class.add(pg_elm,'tab-page')
                $P.attr(pg_elm,'tab-id',tab_id)
            } else {
                // create new page
                $P.create({h:'.tab-page', 'tab-id':tab_id, 
                    c:page_item, parent:$P.query1(tab_container,'.tab-pages')})
            }
            
            // Show Page
            $P.tab.show(undefined,{tab_elm:tab_elm})
            
            return tab_id
        },
        
        page: function(tab_id,tab_container) {
            return $P.query1(tab_container,`.tab-page[tab-id="${tab_id}"]`)
        },
        
        tab: function(tab_id,tab_container) {
            return $P.query1(tab_container,`.tab[tab-id="${tab_id}"]`)
        },
        
    },


//---
//---Menu
    //---o:
    menu: {
        show: function(elm_or_id,options) {
            let opt = {x_off:0,y_off:0,fixed:0,}
            if (options !== undefined) {opt = $P.merge(opt,options)}
            
            // Show Menu Element
            let elm = $P.elm(elm_or_id)
            $P.class.add(elm,'active')
            
            // if (!opt.fixed) {
            //     opt.y_off += window.scrollY
            //     opt.x_off += window.scrollX
            //     elm.style.position='absolute'
            // } else {
            //     elm.style.position='fixed'
            // }

            let x = {}
            if (opt.anchor) {
                // Adjust Position to anchor element
                let anchor_elm = $P.elm(opt.anchor)
                let rect = anchor_elm.getBoundingClientRect()
                if (opt.loc == 'side') {
                    // Side Position
                    if (opt.align == 'left') {
                        x.left = rect.left-elm.offsetWidth+opt.x_off
                    } else {
                        x.left = rect.right+opt.x_off
                    }
                    x.top = rect.top+opt.y_off

                } else {
                    // Below Position
                    if (opt.align == 'right') {
                        x.left = rect.right-elm.offsetWidth+opt.x_off
                    } else {
                        x.left = rect.left+opt.x_off
                    }
                    x.top = rect.bottom+opt.y_off
                }

            } else {
                // Adjust to Event
                if (opt.event) {event = opt.event}
                if (event) {
                    x.left = event.pageX - pageXOffset
                    x.top = event.pageY - pageYOffset
                }
            }
            
            if (x.left) {
                // Check right of Screen
                if (x.left + elm.offsetWidth > pageXOffset+window.innerWidth) {
                    x.left = pageXOffset+window.innerWidth-elm.offsetWidth-10
                }
                elm.style.left = x.left + 'px'
            }
            if (x.top) {
                // Check Bottom of Screen
                if (x.top+elm.offsetHeight > pageYOffset+window.innerHeight) {
                    x.top = pageYOffset+window.innerHeight-elm.offsetHeight-10
                }
                elm.style.top = x.top + 'px'
            }
            
            // Add Background container
            if (document.querySelectorAll('.menu-bg').length == 0) {
                document.body.appendChild($P.create({h:'div.menu-bg',
                    onclick:'$P.menu.close()',oncontextmenu:'$P.menu.close();return false',
                }))
            }
    
        },
        
        close: function(elm_or_id) {
            let close_bkgd = 0
            if (elm_or_id === undefined) {
                $P.class.remove($P.query('.menu-container.active'),'active')
                close_bkgd = 1
            } else {
                let elm = $P.elm(elm_or_id)
                // elm.style.display='none'
                $P.class.remove(elm,'active')
                close_bkgd = ($P.query('.menu-container.active').length == 0)
            }
            
            // Remove Background
            if (close_bkgd) {
                document.querySelectorAll('.menu-bg').forEach(function(bkgd) {
                    bkgd.parentNode.removeChild(bkgd)
                })
            }
        },
        
        build: function(options) {
            let Menu = {
                options:options,
                show: function() {
                    $P.menu.show(Menu.element,Menu.options)
                },
                close: function() {
                    $P.menu.close(Menu.element)
                },
            }
            
            if (options.closeOnClick === undefined) {options.closeOnClick = 1}
            
            // Build Menu Items
            let menu_items = []
            for (let itm of options.items) {
                if (itm == 'separator') {
                    menu_items.push({h:'.menu-separator'})
                } else {
                    let melm = $P.create({h:'.menu-item'})
                    if (itm.icon) {
                        $P.create([{h:'img',src:itm.icon},{h:'span',text:itm.text}],{parent:melm})
                    } else {
                        melm.innerText = itm.text
                    }
                    if (itm.onclick) {
                        $P.listen(melm,'click',itm.onclick)
                    }
                    menu_items.push(melm)
                }
            }
            
            Menu.element = $P.create({h:'.menu-container',c:menu_items, parent:document.body})
            if (options.closeOnClick) {$P.listen(Menu.element,'click',function(){Menu.close()})}
            return Menu
        },
        
    },
    
//---
//---Pagination
    pagination: function(options) {
        let Paginator = {
            count:options.count,
            current:undefined,
            loadPage:options.loadPage,
            page: function(page, navcheck) {
                if (isNaN(page) || page===undefined) {page = Paginator.current}
                if (page===undefined) {page = 0}
                if (page < 0) {
                    page = 0
                } else if (page >= Paginator.count) {
                    page = Paginator.count-1
                }
                if (!navcheck || Paginator.current != page) {
                    Paginator.loadPage(page)
                }
                Paginator.current = page
                if (Paginator.i_current_page) {
                    Paginator.i_current_page.value = page+1
                }
            },
            next: function(inc) {
                Paginator.page(Paginator.current+1,1)
            },
            prev: function(inc) {
                Paginator.page(Paginator.current-1,1)
            },
            setCount: function(count) {
                Paginator.count = count
                Paginator.count_elm.innerText = 'of '+Paginator.count
                Paginator.page()
            },
            build: function() {
                let pg_f = $P.create({h:'.btn', text:'&lsaquo;&lsaquo;', title:'first page'})
                pg_f.addEventListener('click',function(){Paginator.page(0,1)})
                let pg_l = $P.create({h:'.btn', text:'&rsaquo;&rsaquo;', title:'last page'})
                pg_l.addEventListener('click',function(){Paginator.page(Paginator.count,1)})
                Paginator.i_current_page = $P.create({h:'input.pagination-i-page',type:'number',value:1})
                Paginator.i_current_page.addEventListener('change',function(){Paginator.page(parseInt(event.target.value)-1,1)})
                Paginator.count_elm = $P.create({h:'.pagination-count',text:'of '+Paginator.count})
                let pg_p = $P.create({h:'.btn', text:'&lsaquo;', title:'previous page'})
                pg_p.addEventListener('click',function(){Paginator.prev()})
                let pg_n = $P.create({h:'.btn', text:'&rsaquo;', title:'next page'})
                pg_n.addEventListener('click',function(){Paginator.next()})
                Paginator.element = $P.create({h:'.pagination-container',
                    c:[pg_p,pg_n,Paginator.i_current_page,Paginator.count_elm,pg_f,pg_l,],
                })
            }
        }
        
        Paginator.build()
        if (options.parent) {$P.append(options.parent,Paginator.element)}
        
        return Paginator
        
    },

//---
//---Spinner
    spinner: function(options) {
        // {id, parent, style}
        if (options === undefined) {options = {}}
        
        let childs = []
        for (let i =1; i<6; i++) {
            childs.push({h:'.bar-pulse'+i})
            if (options.style !== undefined) {
                childs[i-1]['style']=options.style;
            }
        }
        
        if (options.style !== undefined) {
            delete options.style
        }
        
        return $P.create(
            {h:".spinner-bar-pulse",
                children:childs
            }, options
        )
    },

//---
//---Spliiter
    splitter:function(splitter_elm, resize_elm, div_position) {
        splitter_elm = $P.elm(splitter_elm)
        resize_elm = $P.elm(resize_elm)
        if (div_position == 'left' || div_position == 'right') {
            splitter_elm.classList.add('splitter')
        } else {
            splitter_elm.classList.add('splitter-h')
        }
        
        // Events
        splitter_elm.addEventListener('mousedown', function(e) {
            if (event.buttons == 1) {
                e.preventDefault()
                // Add Splitter Background Overlay
                if (document.querySelectorAll('.splitter-bg').length == 0) {
                    let split_bkgd = document.createElement('div')
                    split_bkgd.className = 'splitter-bg'
                    document.body.appendChild(split_bkgd)
                }
                window.addEventListener('mousemove', resize)
                window.addEventListener('mouseup', stopResize)
                window.addEventListener('mouseleave', stopResize)
            }
        })
        
        function resize(event) {
            if (div_position == 'left') {
                resize_elm.style['flex-basis'] = event.pageX - resize_elm.getBoundingClientRect().left + 'px'
                resize_elm.style['flex-grow'] = 0
            } else if (div_position == 'right') {
                resize_elm.style['flex-basis'] = resize_elm.getBoundingClientRect().right - event.pageX + 'px'
                resize_elm.style['flex-grow'] = 0
            } else if (div_position == 'top') {
                resize_elm.style['flex-basis'] = event.pageY - resize_elm.getBoundingClientRect().top + 'px'
                resize_elm.style['flex-grow'] = 0
            } else if (div_position == 'bottom') {
                resize_elm.style['flex-basis'] = resize_elm.getBoundingClientRect().bottom - event.pageY + 'px'
                resize_elm.style['flex-grow'] = 0
            }
        }
    
        function stopResize() {
            document.body.removeChild(document.querySelector('.splitter-bg'))
            window.removeEventListener('mousemove', resize)
            window.removeEventListener('mouseup', stopResize)
            window.removeEventListener('mouseleave', stopResize)
        }
        
    },


//---
//---Table
    //---o:
    table: {
        build: function(options) {
            // options = {element, cols, rows, sort}
            
            // if undefined, create table
            let tbl = options.element
            if (tbl === undefined) {
                tbl = $P.create({h:'table', parent:options.parent})
            }
            
            // Add Header
            if (options.cols) {
                let thead = {h:'thead',c:[{h:'tr',c:[]}],parent:tbl}
                
                // Check for row index
                if (options.row_index) {
                    thead.c[0].c.push({h:'th.cell-corner'})
                }
                
                // Add Columns
                for (let col of options.cols) {
                    thead.c[0].c.push({h:'th',text:col})
                }
                $P.create(thead)
            }
            
            // Set Sort
            if (options.sort) {
                $P.class.add(tbl,'sortable')
                $P.table.setSortable(tbl)
            }
            
            // Add Cells
            if (options.data) {
                let tbody = {h:'tbody',c:[]}
                for (let r in options.data) {
                    let row = options.data[r]
                    let tr = {h:'tr',c:[]}
                    if (options.row_index) {
                        tr.c.push({h:'th.tbl-row-index',text:r})
                    }
                    for (let cell of row) {
                        if (cell == null) {
                            tr.c.push({h:'td'})
                        } else if (typeof(cell) == 'object') {
                            tr.c.push(cell)
                        } else {
                            tr.c.push({h:'td',text:cell})
                        }
                    }
                    tbody.c.push(tr)
                }
                $P.create(tbody,{parent:tbl})
            }
            
            return tbl
            
        },
        export: function(elm_or_id, options) {
            // options = {parseCell (function), delim(string)}
            if (options === undefined) {options = {}}
            let exportD = {cols:[],data:[]}
            let tbl = $P.elm(elm_or_id)
            
            // Get Cols
            for (col of Array.from(tbl.querySelector('tr').querySelectorAll('th'))) {
                exportD.cols.push(col.innerText)
            }
            
            // Get Rows
            let rows = Array.from(tbl.querySelectorAll('tr')).slice(1)
            for (row of rows) {
                let rw = []
                for (let cell of row.cells) {
                    if (options.parseCell) {
                        rw.push(options.parseCell(cell.innerText))
                    } else {
                        rw.push(cell.innerText)
                    }
                }
                exportD.data.push(rw)
            }
            
            // Other formats
            if (options.delim !== undefined) {
                let txt = $P.array.toText(exportD.cols,options.delim)
                if (exportD.cols.length > 0) {txt+='\n'}
                txt += $P.array.toText(exportD.data,options.delim)
                return txt
            }
            
            return exportD
            
        },
        setSortable: function(elm_or_id) {
            let tbl = $P.elm(elm_or_id)
            // Add click event listener to column header
            let thead = tbl.querySelector('thead')
            if (!thead.sortlistener) {
                thead.sortlistener = 1
                thead.addEventListener('click', event=> {
                    if (event.target.nodeName == 'TH') {
                        let col_index = Array.from(thead.querySelectorAll('th')).indexOf(event.target)
                        $P.table.sort(tbl,col_index)
                    }
                })
            }
        },
        sort:function(elm_or_id, col, options){
            if (options === undefined) {options = {}}
            let tbl = $P.elm(elm_or_id)
            let col_elms = tbl.querySelectorAll('thead th')
            let col_elm = col_elms[col]
            
            if ($P.class.has(col_elm,'sort-none')){return} // Non sortable column
            
            // check sort direction
            let dir = options.dir
            if (dir === undefined) {
                dir = 'asc'
                if ($P.class.has(col_elm,'sort-asc')) {dir = 'desc'}
            }

            // Update column class
            for (let ci in Array.from(col_elms)) {
                $P.class.remove(col_elms[ci],'sort-asc')
                $P.class.remove(col_elms[ci],'sort-desc')
            }
            $P.class.add(col_elm,'sort-'+dir)
            
            // Sort rows
            let reverse = dir =='desc'
            rows = Array.from(tbl.querySelectorAll('tbody tr'))
            rows.sort(function (a, b) {
                let x = (reverse ? b : a).cells[col].innerText
                let y = (reverse ? a : b).cells[col].innerText
                return isNaN(x - y) ? x.localeCompare(y) : x - y
            })
            
            // Clone the column header
            let tbody = $P.create({h:'tbody'})
            for (let row of rows) {
                tbody.appendChild(row)
            }
            tbl.replaceChild(tbody,tbl.querySelector('tbody'))
            
        },
    },
    
    
    
//---
//---Key Event Mapping
    keymap:function(elm_or_id) {
        let keymap = new Object()
        keymap.keys = {}
        keymap.add = function(key,func,options) {
            if (options === undefined) {options = {}}
            let keyA = [key]
            if (options.ctrl){keyA.push(true)} else {keyA.push(false)}
            if (options.shift){keyA.push(true)} else {keyA.push(false)}
            if (options.alt){keyA.push(true)} else {keyA.push(false)}
            if (options.meta){keyA.push(true)} else {keyA.push(false)}
            if (options.preventDefault === undefined){options.preventDefault=1}
            if (key == 'else') {
                keymap.else = {'function':func, preventDefault:options.preventDefault}
            } else {
                keymap.keys[keyA] = {'function':func, preventDefault:options.preventDefault}
            }
        }
        
        keymap.event = function(event) {
            let keyA = [event.key,event.ctrlKey,event.shiftKey,event.altKey,event.metaKey]
            if (keymap.keys[keyA]) {
                if (keymap.keys[keyA].preventDefault){event.preventDefault()}
                keymap.keys[keyA].function()
            } else if (keymap.else) {
                if (keymap.else.preventDefault){event.preventDefault()}
                keymap.else.function()
            }
        }
        
        if (elm_or_id !== undefined) {
            $P.listen(elm_or_id,'keydown',keymap.event)
        }
        
        return keymap
    },

//---
//---Drag and Drop
    setDropEvent:function(options) {
        let elm = $P.elm(options.element)
        
        // Drop Event
        let onDrop = function(ev) {
            ev.preventDefault()
            let drag_data = ev.dataTransfer.getData("text")
            let drag_type = ev.dataTransfer.getData("dataType")
            let drop_elm = ev.target
            if (options.types === undefined || options.types.indexOf(drag_type) > -1) {
                options.onDrop(ev, drag_data, drag_type)
            }
        }
        
        let dragOver = function(ev) {
            ev.preventDefault()
            if (options.dragOver !== undefined) {
                options.dragOver(ev)
            }
        }
        
        let dragLeave = function(ev) {
            options.dragLeave(ev)
        }
        
        // Setup Listener Events
        $P.listen(elm,'drop',onDrop)
        $P.listen(elm,'dragover',dragOver)
        if (options.dragLeave !== undefined) {
            $P.listen(elm,'dragleave',dragLeave)
        }
    },
    
    setDragEvent: function(options) {
        let elm = $P.elm(options.element)
        
        if (options.attr === undefined) {options.attr='id'}
        
        let dragStart = function(ev) {
            ev.dataTransfer.setData("text", ev.target.getAttribute(options.attr))
            ev.dataTransfer.setData("dataType", options.type)
            
            if (options.dragStart !== undefined) {
                options.dragStart(ev)
            }
        }
        
        let dragEnd = function(ev) {
            options.dragEnd(ev)
        }
        
        $P.listen(elm,'dragstart',dragStart)
        if (options.dragEnd !== undefined) {
            $P.listen(elm,'dragend',dragEnd)
        }
        
    },
    
    setDraggable:function(elements, enabled) {
        if (enabled === undefined) {enabled = 'true'}
        $P.iterElements(elements, function(elm){
            $P.attr(elm,'draggable',enabled)
        })
    },


//---
//---Utilities (Some Nice JavaScript Functions)

//---String Functions
    replaceAll: function(string, search_val, replace_val) {
        return string.split(search_val).join(replace_val)
    },
    
    sanitize: function(txt,mode,replace_text) {
        if (replace_text === undefined){replace_text = ''}
        let reg = /[^a-z0-9'_\- .]/gi
        if (mode == 'file') {
            reg = /[^a-z0-9'_\- .\]\[\(\)\&]/gi
        } else if (mode == 'strict') {
            reg = /[^a-z0-9_]/gi
        } else if (mode == 'html') {
            return txt.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        }
        return txt.replace(reg, replace_text)
    },
//---Array Functions
    //---o:
    array: {
        move: function(array, fromIndex, toIndex) {
            return array.splice(toIndex, 0, array.splice(fromIndex,1)[0])
        },
        remove: function(array, item) {
            for (let i = array.length-1; i >=0; i--) {
                if (array[i] == item) {
                    array.splice(i,1)
                }
            }
            return array
        },
        in: function(array,item) {
            return array.indexOf(item)>-1
        },
        sort: function(array) {
            array.sort((a,b) => {
                return isNaN(a - b) ? String(a).localeCompare(b) : a - b
            })
        },
        toText: function(array,delim) {
            if (delim === undefined) {delim='\t'}
            if (Array.isArray(array[0])) {
                // 2D Array
                let txt = ''
                for (rw of array) {
                    txt += rw.join(delim)+'\n'
                }
                return txt.slice(0,-1)
            } else {
                return array.join(delim)
            }
        }
    },

//---Number Functions 
    round:function(n,d) {
        if (d === undefined) {d=0}
        return parseFloat(n.toFixed(d))
    },
    
    random:function(n) {
        if (n === undefined) {n=1}
        return Math.round(Math.random()*n)
    },
    
    isNumber:function(n) {
        return !isNaN(parseFloat(n)) && isFinite(n)
    },

    uuid: function(dash) {
        let d = new Date().getTime()
        if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
            d += performance.now() //use high-precision timer if available
        }
        let s = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
        if (dash !== undefined && !dash) {s = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'}
        return s.replace(/[xy]/g, function (c) {
            let r = (d + Math.random() * 16) % 16 | 0
            d = Math.floor(d / 16)
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16)
        })
    },

//---Date Functions
    //---o:
    date:{
        //---o:
        months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        //---o:
        weekdays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        //---o:
        month_day_counts: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],

        date:function(year_or_datestr,month,day) {
            if (year_or_datestr === undefined) {
                return new Date()
            }
            if ($P.date.isDate(year_or_datestr)) {
                return new Date(year_or_datestr)
            }
            if (month === undefined) {
                return new Date(year_or_datestr +' 00:00')
            }
            if (day === undefined) {day=1}
            return new Date(year_or_datestr+'-'+month+'-'+day+' 00:00')
        },
        
        addDays:function(date_or_datestr,days) {
            let new_date = $P.date.date(date_or_datestr)
            new_date.setDate(new_date.getDate() + days)
            return new_date
        },
        
        isDate: function(date) {
            return typeof(date.getMonth) === 'function'
        },
        
        str:function(year_or_date,month,day){
            let m,d,y
            if (year_or_date === undefined) {
                year_or_date = new Date()
            }
            if (month !== undefined) {
                y = year_or_date
                m = month//-1
                d = day
                if (d === undefined) {d = 1}
            } else {
                m = year_or_date.getMonth()+1
                d= year_or_date.getDate()
                y = year_or_date.getFullYear()
            }
            if (m < 10) {
                m = '0'+m
            }
            if (d < 10) {
                d = '0'+d
            }
            return ''+y+'-'+m+'-'+d
            
        },

    },

//---Other
    isEmpty: function(obj) {
        let empty = 1
        if (obj === undefined) {
        } else if (obj.length > 0) {
            empty = 0
        } else if (Object.keys(obj).length > 0) {
            empty = 0
        }
        return empty
    },
    
    merge: function(obj1,obj2) {
        let obj3 = {}
        if (typeof Object.assign !== 'function') {
            for (let atr in obj1) { obj3[atr] = obj1[atr]}
            for (let atr in obj2) { obj3[atr] = obj2[atr]}
        } else {
            obj3 = Object.assign({}, obj1, obj2)
        }
        return obj3
    },
    
    fetch: function(url, typeOrOptions, options) {
        prom = $P.promise()
        let opt
        let resp_type = 'json'
        if (typeof(typeOrOptions) == 'string') {
            resp_type = typeOrOptions
            opt = options
        } else {
            opt = typeOrOptions
            if (typeof(options) == 'string') {
                resp_type = options
            }
        }
        
        if (opt === undefined) {opt = {}}
        
        // Add nice json handling
        if (opt.json !== undefined) {
            if (opt.method === undefined) {opt.method = 'POST'} // default to post
            opt.headers = {'Content-Type': 'application/json;charset=utf-8'}
            if (typeof(opt.json) != 'string') {
                opt.body = JSON.stringify(opt.json)
            } else {opt.body = opt.json}
            delete opt.json
        }
        
        fetch(url,opt).then(resp=>{
            resp[resp_type]().then(data=>{
                prom.accept(data)
            }).catch(e=>{
                prom.cancel(e)
            })
        }).catch(e=>{
            prom.cancel(e)
        })
        
        return prom
        
    },
    
    ajax: function(ajaxD) {
        let tempD = {
            method:'GET',
            'async':1,
        }
        let aD = $P.merge(tempD,ajaxD)
        
        let xhr_doneP = {}
        let promise_ok = 0
        if ('Promise' in window) {
            xhr_doneP = $P.promise()
            promise_ok = 1
        }
        
        let xhr = new XMLHttpRequest()
        xhr_doneP.xhr = xhr
        xhr.open(aD.method, aD.url, aD.async)
        if (aD.responseType != '' && aD.responseType !== undefined) {
            xhr.responseType = aD.responseType
        }
        
        // Listening Events
        if (aD.events) {
            aD.events.forEach(function(evt){
                if (evt.obj) {
                    xhr[evt.obj].addEventListener(evt.event,evt.func,false)
                } else {
                    xhr.addEventListener(evt.event,evt.func,false)
                }
            })
        }
        
        xhr.onreadystatechange = function(){
            if (xhr.readyState == XMLHttpRequest.DONE ) {
                // if (xhr.readyState == 4 && xhr.status == 200) {
                if (xhr.status == 200) {
                    if (aD.success !== undefined) {
                        aD.success(xhr.response)
                    }
                    if (promise_ok) {
                        xhr_doneP.accept(xhr.response)
                    }
                } else {
                    if (aD.error != undefined) {
                        aD.error(xhr)
                    }
                    if (promise_ok) {
                        xhr_doneP.cancel(xhr)
                    }
                }
            }
        }

        if (aD.contentType != undefined) {
            xhr.setRequestHeader('Content-Type', aD.contentType)
        }
        // Add Headers
        if (aD.headers != undefined) {
            for (let ky in aD.headers) {
                xhr.setRequestHeader(ky, aD.headers[ky])
            }
        }
        if (aD.data != undefined) {
            xhr.send(aD.data)
        } else {
            xhr.send()
        }
        
        return xhr_doneP
        
    },

    copyToClipboard: function(text) {
        return navigator.clipboard.writeText(text)
        // let elm = document.createElement('textarea')
        // elm.value = text
        // document.body.appendChild(elm)
        // elm.select()
        // document.execCommand('copy')
        // document.body.removeChild(elm)
    },

    isObject: function(obj) {
        return obj === Object(obj)
    },
    
    sleep: function(ms) {
        return new Promise(resolve => setTimeout(resolve,ms))
    },
    
    //---o:
    file:{
        // File Reader with promise
        read:function(read_func,data){
            let prom = new $P.promise()
            let reader = new FileReader();
            reader.onload = function(event) {
                prom.accept(event.target.result)
            }
            reader[read_func](data)
            return prom
        },
    },

    //---o:
    url: {
        params: function() {
            // Parse Url Parameters
            let hD = {}
            if (location.search) {
                location.search.slice(1).split('&').forEach(function (h){
                    let hspl = h.split('=')
                    hD[hspl[0]]=undefined
                    if (hspl.length > 1){
                        hD[hspl[0]] = hspl[1]
                    }
                })
            }
            return hD
        },
        
        setParams: function(params) {
            let h = ''
            for (let ky in params) {
                if (h != '') {h += '&'}
                h += ky
                if (params[ky] !== undefined) {
                    h += '='+params[ky]
                }
            }
            window.history.pushState('data', undefined, location.origin + location.pathname +'?'+ h + location.hash)
        },
        
        updateParams: function(params) {
            let cur_params = $P.url.params()
            for (let ky in params) {
                cur_params[ky] = params[ky]
            }
            $P.url.setParams(cur_params)
        },
        
        removeParam: function(param) {
            let params = $P.url.params()
            delete params[param]
            $P.url.setParams(params)
        },
        
        listen: function(listen_func) {
            window.addEventListener('popstate', function(e){
                listen_func($P.url.params())
            })
        },
        
    },

}

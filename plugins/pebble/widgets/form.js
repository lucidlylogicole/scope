try {$P} catch(e) {$P={}}

//---o:
$P.form = function(options) {

let Form = {
    version:'1.5.1',
    fields:[],
    
    build: function() {
        
        // Build the form
        Form.element = $P.create({h:'.form-container pebble'})
        
        // Check View Only
        if (options.viewonly) {
            Form.element.classList.add('form-viewonly')
        }
        
        // Add Fields
        for (let i in options.fields) {
            Form.addField(options.fields[i])
        }
        
        // Add to parent
        if (options.parent) {
            Form.parent = $P.elm(options.parent)
            Form.parent.appendChild(Form.element)
        }
        
    },
    
    rebuild: function() {
        // Clear Parent
        $P.clear(Form.element)
        
        // Re-add Field Elements
        for (let i in Form.fields) {
            Form.addFieldElement(i,Form.fields[i])
        }

    },
    
    addField: function(fieldD) {
        Form.fields.push(fieldD)
        
        
        let field_index = Form.fields.length-1
        Form.addFieldElement(field_index,fieldD)
    },
    
    addFieldElement: function(field_index,fieldD) {
    
        if (fieldD.display == 'none') {
            return
        }
        
        // Row Elm
        let row_elm = document.createElement('div')
        row_elm.classList.add('form-row')
        row_elm.setAttribute('form-row-index',field_index)
        
        // Display
        if (fieldD.display !== undefined && fieldD.display != 'visible') {
            row_elm.classList.add(fieldD.display)
        }
        
        // Required
        if (!options.viewonly && fieldD.required && !['toggleswitch','checkbox','separator','heading','button'].includes(fieldD.type)) {
            row_elm.classList.add('required')
        }
        
        // Title Element
        let label_elm = document.createElement('label')
        if (fieldD.title) {
            label_elm.innerHTML = fieldD.title
        }
        row_elm.appendChild(label_elm)
        
        // Value alias
        if (fieldD.value) {fieldD.val = fieldD.value}
        
        // Input Element
        let input_elm
        // View only elm
        if (options.viewonly) {
            input_elm = document.createElement('div')
            input_elm.classList.add('form-val')
        }
        if (fieldD.type == 'combobox' || fieldD.type == 'select' || fieldD.type == 'list') {
        //---Combobox and List
        
            if (!options.viewonly) {
                input_elm = document.createElement('select')
            
                if (fieldD.type == 'list' || fieldD.size) {
                    if (!fieldD.size) {fieldD.size=4}
                    input_elm.setAttribute('size',fieldD.size)
                    input_elm.classList.add('form-list')
                }
                if (fieldD.multiple) {
                    input_elm.setAttribute('multiple','multiple')
                }
                
                // Add Items
                if (fieldD.items) {
                    for (let item of fieldD.items) {
                        let opt_elm = document.createElement('option')
                        if (typeof(item) == 'object' && item != null) {
                            opt_elm.value = item.val
                            opt_elm.text = item.title
                        } else {
                            opt_elm.value = item
                            opt_elm.text = item
                        }
                        input_elm.appendChild(opt_elm)
                    }
                }
            }
            
            // Add to Row
            row_elm.appendChild(input_elm)
            
        } else if (fieldD.type == 'button') {
        //---Button
            
            // Add Container
            let input_outer_elm = document.createElement('div')
            input_outer_elm.classList.add('form-field-container')
            
            // Button Elm
            input_elm = document.createElement('button')
            if (options.viewonly) {input_elm.setAttribute('disabled','disabled')}
            input_elm.innerHTML = fieldD.text
            if (fieldD.val) {input_elm.innerText = fieldD.val}
            if (fieldD.click) {input_elm.addEventListener('click',fieldD.click)}
            
            // Add to Row
            input_outer_elm.appendChild(input_elm)
            row_elm.appendChild(input_outer_elm)
            
        } else if (fieldD.type == 'textarea') {
        //---Text Area
            // View Only
            if (!options.viewonly) {
                input_elm = document.createElement('textarea')
            }
            
            let ta_rows = 4
            if (fieldD.rows) {ta_rows = fieldD.rows}
            input_elm.setAttribute('rows',ta_rows)
            
            // Add to Row
            row_elm.appendChild(input_elm)
        } else if (fieldD.type == 'separator') {
        //---Separator
            input_elm = document.createElement('div')
            
            if (fieldD.line) {
                label_elm.classList.add('form-separator-line')
            } else {
                label_elm.classList.add('form-separator')
            }
        } else if (fieldD.type == 'heading') {
        //---Heading
            input_elm = document.createElement('div')
            label_elm.classList.add('form-heading')
            // input_elm.innerText = fieldD.title
        } else if (fieldD.type == 'custom') {
        //---Custom
            input_elm = fieldD.element
        } else if (fieldD.type == 'note') {
        //---Note
            input_elm = document.createElement('div')
            label_elm.classList.add('form-note')
            if (fieldD.create !== undefined) {
                $P.create(fieldD.create,{parent:label_elm})
            } else if (fieldD.html !== undefined) {
                label_elm.innerHTML = fieldD.html
            }
        } else {
        //---Input Element
            input_elm = document.createElement('input')
            if (fieldD.type) {
                input_elm.setAttribute('type',fieldD.type)
                
                // Checkbox
                if (fieldD.type == 'checkbox' || fieldD.type == 'toggleswitch') {
                    if (fieldD.type == 'toggleswitch') {
                        input_elm.classList.add('toggle-switch')
                        input_elm.setAttribute('type','checkbox')
                    }
                    
                    // View Only 
                    if (options.viewonly) {input_elm.setAttribute('disabled','disabled')}
                    
                    // Add container
                    let input_outer_elm = document.createElement('div')
                    input_outer_elm.classList.add('form-field-container')
                    input_outer_elm.appendChild(input_elm)
                    row_elm.appendChild(input_outer_elm)
                    
                } else {
                    // View Only
                    if (options.viewonly) {
                        input_elm = document.createElement('div')
                        input_elm.classList.add('form-val')
                    }
                    
                    // Add to Row
                    row_elm.appendChild(input_elm)
                }
            }

        }
        
        // Additional Element Attributes
        input_elm.classList.add('form-field')
        
        // ID
        if (fieldD.id) {
            input_elm.id = fieldD.id
            label_elm.setAttribute('for',fieldD.id)
        }
        
        // Tooltip
        if (fieldD.tooltip) {
            input_elm.title = fieldD.tooltip
            label_elm.title = fieldD.tooltip
        }
        // Placeholder
        if (fieldD.placeholder) {input_elm.setAttribute('placeholder',fieldD.placeholder)}
        // Set Max Length
        if (fieldD.maxlength) {input_elm.setAttribute('maxlength',fieldD.maxlength)}
        // Wide Setting
        if (fieldD.wide || options.layout == 'column') {
            row_elm.classList.add('wide')
        }
        // Add Other Attributes
        for (let attr in fieldD.attr) {input_elm.setAttribute(attr,fieldD.attr[attr])}
        
        // Add Events
        for (let evt in fieldD.events) {
            input_elm.addEventListener(evt,fieldD.events[evt])
        }
        
        // Add optional Right Text Element
        if (fieldD.rtext) {
            let rtext_elm = document.createElement('div')
            rtext_elm.classList.add('form-rtext')
            rtext_elm.innerHTML = fieldD.rtext
            row_elm.appendChild(rtext_elm)
        }
        
        // Store field elements
        fieldD.elm = {
            input:input_elm,
            label:label_elm,
            row:row_elm,
        }
        Form.element.appendChild(row_elm)
        
        // Set Default Value
        if (fieldD.val !== undefined) {
            Form.setValue(field_index,fieldD.val)
        } else if (fieldD.default !== undefined) {
            Form.setValue(field_index,fieldD.default)
        }
        
        // Set Disabled
        if (fieldD.display == 'disabled') {
            Form.setEnabled(field_index,0)
        }
        
        return
    },
    
//---Values
    clearValues: function() {
        // Clear Values
        for (let fi in Form.fields) {
            let fieldD = Form.fields[fi]
            if (fieldD.default) {
                Form.setValue(fi,fieldD.default)
            } else {
                Form.setValue(fi,'')
            }
        }
    },
    
    setValue: function(field_index, val) {
        if (options.viewonly && !['checkbox','toggleswitch'].includes(Form.fields[field_index].type)) {
            if (val === undefined){val = ''}
            Form.fields[field_index].elm.input.innerText = val
        } else {
            $P.val(Form.fields[field_index].elm.input,val)
        }
    },
    
    setValues: function(valueD) {
        // Set Field Values
        
        let valD = valueD
        // Parse Array into dict
        if (Array.isArray(valueD)) {
            valD = {}
            for (let i in valueD) {
                valD[i] = valueD[i]
            }
        }
        
        for (let fi in Form.fields) {
            let fid = Form.fields[fi].id
            let val
            // Check if id or index in valD and set value
            if (fid in valD) {
                val = valD[fid]
            } else if (fi in valD) {
                val = valD[fi]
            }
            if (val) {form.setValue(fi,val)}
        }
    },
    
    getValue: function(field_index) {
        return $P.val(Form.fields[field_index].elm.input)
    },
    
    getValues: function(format) {
        // Get all field values
        if (format === undefined && Form.format !== undefined) {
            format = Form.format
        }
        let vals = []
        let valD = {}
        for (let fi in Form.fields) {
            let fD = Form.fields[fi]
            if (fD.display != 'none') {
                let val = Form.getValue(fi)
                if (format == 1) {
                    if (Form.fields[fi].id) {
                        fid = Form.fields[fi].id
                        valD[fid] = val
                    }
                } else {
                    vals.push(val)
                }
            }
        }
        if (format == 1){
            return valD
        } 
        return vals
    },

//---Functions
    getElements: function(field_index) {
        return Form.fields[field_index].elm
    },

    setEnabled: function(field_index,enabled) {
        let fieldD = Form.fields[field_index]
        if (fieldD.elm !== undefined) {
            if (enabled) {
                $P.class.remove(fieldD.elm.row,'disabled')
            } else {
                $P.class.add(fieldD.elm.row,'disabled')
            }
        }
    },
    
    checkRequired: function() {
        let req_fields = []
        for (let fi=0; fi < Form.fields.length; fi++) {
            let fD = Form.fields[fi]
            if (fD.required && !(['hidden','disabled','none'].includes(fD.display))) {
                if (['text','textarea','select','combobox','date'].includes(fD.type)) {
                    if (Form.getValue(fi) == ''){req_fields.push(fi)}
                } else if (['number'].includes(fD.type)) {
                    if (Form.getValue(fi) === null){req_fields.push(fi)}
                } else if (['list'].includes(fD.type)) {
                    if (Form.getValue(fi).length < 1){req_fields.push(fi)}
                }
            }
            
        }
        return req_fields
    },
    
}

// Load Fields
Form.build()

// Set default format
if (options.format) {
    Form.format = options.format
}

// Set Values if provided
if (options.values) {
    Form.setValues(options.values)
}

return Form

}

//---o:
$P.dialog.form = function(options) {
    
    let dlg_doneP = $P.promise()
    let dlg_id = $P.uuid(0)
    
    // Build Form
    let Form = $P.form(options)

    opt = {'buttons':[], content:Form.element}
    if (options.title) {opt.title = options.title}
    if (options.size) {opt.size = options.size}
    
    let val_msg = '<b>The following fields are required but not complete:</b>'
    if (opt.val_msg) {val_msg = opt.val_msg}
    
    // Ok 
    opt.buttons.push($P.create({h:'button#b_ok_'+dlg_id+'.btn btn-dlg-ok',text:'ok'}))
    opt.buttons[0].onclick = function(event){
        
        // Check Required
        let req_fields = Form.checkRequired()
        if (req_fields.length > 0) {
            let req_txt = ''
            for (let fi of req_fields) {
                req_txt += '<br> - '
                req_txt += Form.fields[fi].title
            }
            
            $P.dialog.message({text:val_msg+req_txt})
            return
        }
        
        dlg_doneP.accept(Form.getValues())
        $P.dialog.close(event.target)
    }
    // Cancel
    opt.buttons.push($P.create({tag:'button',text:'cancel',class:'btn'}))
    opt.buttons[1].onclick = function(event){
        dlg_doneP.accept()
        $P.dialog.close(event.target)
    }
    $P.dialog.build(opt)
    
    // Optional Onload call
    if (options.onload !== undefined) {
        options.onload(Form)
    }
    
    // Focus on first field
    try {Form.fields[0].elm.input.focus()} catch(e) {}
    
    return dlg_doneP

}

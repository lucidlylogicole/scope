function createScratchpad() {
    
    let scratch_elm = {
        h:'textarea#scratchpad_page', placeholder:'scratchpad',
        style:`display:flex;
            flex:1;
            background:rgb(30,30,30);
            padding:12px;
            color:rgb(150,150,150);
            border:0px;
            outline:none;
            resize: none;
            `
    }

    IDE.BottomTab.addTab({
        tooltip:'scratchpad',
        icon:'plugins/scratchpad/scratchpad.svg',
        close:0,
        page:scratch_elm,
    })

}

createScratchpad()
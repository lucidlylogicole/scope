
//---o:
let ColorPickerPlugin = {
    page_uuid: undefined,
    
    view: function() {
        if (!(ColorPickerPlugin.page_uuid in IDE.RightWindow.pages)) {
            ColorPickerPlugin.page_uuid = IDE.RightWindow.addPage({src:'plugins/color_picker/index.html',iframe:1,sandbox:0})
        } 
        // Toggle
        IDE.RightWindow.showPage(ColorPickerPlugin.page_uuid)
    },
}

function createPlugin() {
    // Add Folder Button
    let ff_btn = $P.create({h:'button#b_color_picker.btn-clear', onclick:"ColorPickerPlugin.view()",
        title:"color picker", c:[{h:'img',src:'plugins/color_picker/color_picker.svg'}],
        parent:'sidebar_plugin_buttons'
    })
    
    IDE.plugins['color_picker'] = ColorPickerPlugin
}

createPlugin()
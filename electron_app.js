const {app, BrowserWindow, dialog, Menu, MenuItem, ipcMain, shell, screen} = require('electron')
const fs = require('fs')
const os = require('os')
const path = require('path')

// Set Current file to path
process.chdir(path.resolve(__dirname))

// For troubleshooting, print warnings
if (process.argv.includes('debug')) { 
    process.on('warning', (e) => {
        console.log(e.stack)
    })
}

// Sandbox renderer for security
app.enableSandbox()

// Hide Menu
Menu.setApplicationMenu(null)
let win
let VERSION = require('./package.json').version

function createWindow () {
    
    // Get Current Display
    let point = screen.getCursorScreenPoint()
    let cur_display = screen.getDisplayNearestPoint(point)
    
    // Calculate window geometry
    let x,y,w,h
    if (IDE.settings.window.openMode == 1) {
        // Centered Window
        w = IDE.settings.window.size.width
        h = IDE.settings.window.size.height
    } else if (IDE.settings.window.openMode == 2) {
        // Expand Monitor
        x = IDE.settings.window.margin.left+cur_display.bounds.x
        y = IDE.settings.window.margin.top
        w = cur_display.bounds.width-IDE.settings.window.margin.left-IDE.settings.window.margin.right
        h = cur_display.bounds.height-IDE.settings.window.margin.bottom
    } else if (IDE.settings.window.openMode == 3) {
        // Maximum
        w = cur_display.bounds.width
        h = cur_display.bounds.height
        x = cur_display.bounds.x
        y = cur_display.bounds.y
    }
    
    //---o:
    win = new BrowserWindow({
        width: w,
        height: h,
        x:x,
        y:y,
        icon:path.join(__dirname,'icons/scope.png'),
        autoHideMenuBar:true,
        backgroundColor: '#1e1e1e',
        // center:false,
        webPreferences: {
            nodeIntegration: false,
            devTools: true,
            contextIsolation: true,
            enableRemoteModule: false,
            spellcheck:IDE.settings.window.spellcheckEnabled,
            preload: path.join(__dirname, "preload.js"),
        },
    })
    
    IDE.win = win
    
    // Maximize Window
    if (IDE.settings.window.openMode == 3) {
        win.maximize()
    }
    
    // Clear Cache (in future check version change before doing this
    win.webContents.session.clearCache(function(){})
    
    // Intercept External References
    win.webContents.session.protocol.interceptStreamProtocol('https', (request, callback) => {
        console.log('attempted https: ',request.url)
    })
    win.webContents.session.protocol.interceptStreamProtocol('http', (request, callback) => {
        console.log('attempted http: ',request.url)
    })

    if (process.argv.includes('debug')) { 
        win.webContents.openDevTools()
    }
    
    //---Override Error Dialogs
    //---f:
    dialog.showErrorBox = function(title, content) {
        console.error(`${title}\n${content}`)
        win.webContents.executeJavaScript("console.error(String.raw`"+`${title}\n${content}`+"`)")
    }

    //---f:
    win.errorLog = function(error) {
        console.error(error)
        win.webContents.send("errorLog", {error:error})
    }

    //---f:
    win.loadURL = function(){}
    
    //---f:
    win.loadFile(path.join(__dirname,'ide.html')).then(()=>{
        IDE.path = __dirname
        IDE.win.webContents.executeJavaScript(`IDE.path="${__dirname.replace(/\\/g,'\\\\')}";IDE.VERSION='${VERSION}'`)
        
        let plugin_objD = {}
        
        //---Plugins
        for (let plugin of IDE.settings.pluginsEnabled) {
            let plugin_obj
            try {
                plugin_obj = require(`./plugins/${plugin}/${plugin}_main.js`)
                plugin_objD[plugin] = plugin_obj
            
                for (let css_file of plugin_obj.Plugin['css']) {
                    // Insert CSS if there
                    let css_path = path.join(__dirname,'plugins',plugin,css_file)
                    if (fs.existsSync(css_path)) {
                        let css_text = fs.readFileSync(css_path,'utf8')
                        IDE.win.webContents.insertCSS(css_text)
                    }
                }
    
                // Get Plugin Settings
                if (!(plugin in IDE.settings.plugins)) {
                    IDE.settings.plugins[plugin]  = {}
                }
                if ('settings' in plugin_obj.Plugin) {
                    IDE.settings.plugins[plugin] = mergeSettings(plugin_obj.Plugin.settings, IDE.settings.plugins[plugin])
                }
            
            // Load Main (Electron) Plugin JS
            // try {
                IDE.plugins[plugin] = plugin_obj.load(IDE)
                if (IDE.plugins[plugin] === undefined){IDE.plugins[plugin]={}}
                IDE.plugins[plugin].settings = IDE.settings.plugins[plugin]
            } catch(e) {
                console.log('failed to load plugin: ',plugin)
                win.errorLog(e)
                win.webContents.send("status", {text:'error loading plugin: '+plugin,mode:'warning',delay:5,append:1})
            }

        }
        // Reload Settings
        IDE.win.webContents.executeJavaScript('IDE.getSettings()')
        
        // Run UI JavaScript if there
        for (let plugin of IDE.settings.pluginsEnabled) {
            if (plugin in plugin_objD) {
                for (let js_file of plugin_objD[plugin].Plugin['js']) {
                    let js_path = path.join(__dirname,'plugins',plugin,js_file)
                    if (fs.existsSync(js_path)) {
                        let js_text = fs.readFileSync(js_path,'utf8')
                        IDE.win.webContents.executeJavaScript(js_text)
                    }
                }
            }

        }

        //---Load Last Argument file
        let last_arg = process.argv.slice(-1)[0]
        if (!last_arg.endsWith('electron_app.js') && last_arg != '.' && fs.existsSync(last_arg)) {
            // win.webContents.send('openFile',{filename:path.resolve(last_arg)})
            IDE.win.webContents.executeJavaScript(`IDE.openFile('${path.resolve(last_arg)}')`)
        }
        
        //---Set Zoom Factor
        // win.webContents.setZoomFactor(1)
        
    }).catch(e=> {
        win.errorLog(e)
    })
    
    //---close window
    // Emitted when the window is closed.
    win.on('close', (e) =>{
        let ok = 1
        if (IDE.FORCE_CLOSE > 2) {
            let options = {
                title:'Force Close',
                message:'Do you want to force close Scope',
                type:'question',
                buttons:['yes','no']
            }
            ok = dialog.showMessageBoxSync(win,options)
        }
        if (ok && IDE.CHECK_CLOSE) {
            IDE.FORCE_CLOSE++
            e.preventDefault()
            win.webContents.send("closeall")
        }
    })
    win.on('closed', () => {
        win = null
    })
    
    //---new window
    if (win.webContents.setWindowOpenHandler !== undefined) {
        // Electron 12+
        win.webContents.setWindowOpenHandler(({ url }) => {
            // e.preventDefault()
            shell.openExternal(url)
            return {action: 'deny',}
        })
        // win.webContents.on('did-create-window', (childWindow) => {
        //   // For example...
        //   childWindow.webContents.on('will-navigate', (e) => {
        //     e.preventDefault()
        //   })
        // })

    } else {
        // Older versions (deprecated eventually)
        win.webContents.on('new-window', function(e, url) {
            e.preventDefault()
            shell.openExternal(url)
        })
    }
    

    win.webContents.on('will-navigate', function(e, url) {
        // override all links
        e.preventDefault()
        shell.openExternal(url)
    })
    
    //---Right Click Menu
    // Set up Right Click Menu
    const selectionMenu = Menu.buildFromTemplate([
        {role: 'copy'},
        {type: 'separator'},
        {role: 'selectall'},
    ])
    
    const inputMenu = Menu.buildFromTemplate([
        {role: 'cut'},
        {role: 'copy'},
        {role: 'paste'},
        {type: 'separator'},
        {role: 'selectall'},
        {type: 'separator'},
        {role: 'undo'},
        {role: 'redo'},
    ])
    
    // Spellcheck
    const spellMenu = new Menu()
    if (IDE.settings.window.spellcheckEnabled) {
        inputMenu.insert(0,new MenuItem({
            label:'Spellcheck',
            type:'submenu',
            submenu:spellMenu
        }))
        inputMenu.insert(1,new MenuItem({
            type:'separator',
        }))
    }
    
    win.webContents.on('context-menu', (e, props) => {
        const { selectionText, isEditable } = props
        
        if (isEditable) {
            // Spellcheck
            if (IDE.settings.window.spellcheckEnabled) {
                spellMenu.clear()
                for (let suggestion of props.dictionarySuggestions) {
                    spellMenu.append(new MenuItem({
                        label: suggestion,
                        click: () => win.webContents.replaceMisspelling(suggestion)
                    }))
                }
            }

            
            inputMenu.popup(win)
        } else if (selectionText && selectionText.trim() !== '') {
            selectionMenu.popup(win)
        }
    })

}

//---
//---o:
let IDE = {
    OPEN_FILES:{},
    CHECK_CLOSE:1,
    FILE_WATCHERS:{},
    lastPath:'',
    plugins:{},
    settings:{},
    FORCE_CLOSE:0,
    platform:process.platform,
}

//---Settings
// Load Settings
let SETTINGS_PATH = path.join(app.getPath('home'),'.scope')

// Check For portable settings
if (fs.existsSync(path.resolve('./.settings'))) {
    SETTINGS_PATH = path.resolve('./.settings')
}

// Create Settings Directory
if (!fs.existsSync(SETTINGS_PATH)) {fs.mkdirSync(SETTINGS_PATH)}
// Copy Default Settings
if (!fs.existsSync(path.join(SETTINGS_PATH, 'settings.json'))) {
    // Copy Settings File
    fs.copyFileSync(path.join(__dirname, 'settings.json'),path.join(SETTINGS_PATH, 'settings.json'))
}
// Setup Workspaces
if (!fs.existsSync(path.join(SETTINGS_PATH,'workspaces'))) {
    fs.mkdirSync(path.join(SETTINGS_PATH,'workspaces'))
}


// Parse Settings
function getSettings(reload) {
    if (reload) {
        let dflt_text = fs.readFileSync(path.join(__dirname, 'settings.json'),'utf8')
        let default_settings = JSON.parse(dflt_text)
        default_settings.cwd = app.getPath('home')
        
        try {
            let text = fs.readFileSync(path.join(SETTINGS_PATH, 'settings.json'),'utf8')
            let user_settings = JSON.parse(text)
            
            default_settings = mergeSettings(default_settings,user_settings)
            
        } catch(e) {
            console.log('error loading settings')
            if (win) {
                win.errorLog(e)
                win.webContents.send("status", {text:'error loading settings',mode:'warning'})
            }
            default_settings = JSON.parse(dflt_text)
            default_settings.cwd = app.getPath('home')
        }
        
        IDE.settings = default_settings
        
        // Get Plugin Settings
        for (let plugin of IDE.settings.pluginsEnabled) {
            if (!(plugin in IDE.settings.plugins)) {
                IDE.settings.plugins[plugin]  = {}
            }
            if (plugin in IDE.plugins && IDE.plugins[plugin] !== undefined && 'settings' in IDE.plugins[plugin]) {
                IDE.settings.plugins[plugin] = mergeSettings(IDE.plugins[plugin].settings, IDE.settings.plugins[plugin])
            }
        }
    }
    return IDE.settings
}

getSettings(1)

function mergeSettings(default_settings,user_settings) {
    // Get Default Settings
    for (let ky in default_settings) {
        if (ky in user_settings) {
            if (typeof(default_settings[ky]) == 'object') {
                for (let sky in default_settings[ky]) {
                    if (typeof(default_settings[ky][sky]) == 'object') {
                        if (sky in user_settings[ky]) {
                            for (let ssky in default_settings[ky][sky]) {
                                if (ssky in user_settings[ky]) {
                                    default_settings[ky][sky][ssky] = user_settings[ky][sky][ssky]
                                }
                            }
                        }
                    }
                }
            } else {
                // Add Main key
                default_settings[ky] = user_settings[ky]
            }
        }
    }
    
    // Add User Settings
    for (let ky in user_settings) {
        if (ky in default_settings) {
            for (let sky in user_settings[ky]) {
                if (typeof(user_settings[ky][sky]) == 'object' && sky in default_settings[ky]) {
                    for (let ssky in user_settings[ky][sky]) {
                        default_settings[ky][sky][ssky] = user_settings[ky][sky][ssky]
                    }
                } else {
                    default_settings[ky][sky] = user_settings[ky][sky]
                }
            }
        } else {
            // Add Main key
            default_settings[ky] = user_settings[ky]
        }
    }
    
    return default_settings
}

//---File Functions
function openFile(filepath,txt) {
    IDE.OPEN_FILES[filepath] = 0
    if (txt !== undefined) {
        IDE.OPEN_FILES[filepath] = checksum(txt)
    }
    addFileWatcher(filepath)
}

function addFileWatcher(filepath) {
    let fsWait = false
    if (filepath in IDE.FILE_WATCHERS) {
        return
    }
    IDE.FILE_WATCHERS[filepath] = fs.watch(filepath, (eventType, filename) => {
        if (filename) {
            if (fsWait) {return}
            
            // Delay half a second to make sure file is there
            fsWait = setTimeout(() => {
                // Check if file actually changed
                let exists = fs.existsSync(filepath)
                if ((eventType == 'rename' && !exists) || (exists && checksum(fs.readFileSync(filepath,'utf8')) != IDE.OPEN_FILES[filepath])) {
                    win.webContents.send("fileChanged", {filename:filepath,eventType:eventType})
                }
                fsWait = false
            }, 500)
        }
    })
}

function removeFileWatcher(filepath) {
    if (filepath in IDE.FILE_WATCHERS) {
        IDE.FILE_WATCHERS[filepath].close()
        delete IDE.FILE_WATCHERS[filepath]
    }
}

function saveFile(filename,text,not_open) {
    let resp = {ok:0,msg:''}
    let err
    try {
        err = fs.writeFileSync(filename, text) 
    } catch(e) {
        err = e
    }
    if (err) {
        win.errorLog(err)
        resp.msg='ERROR: '+ path.basename(filename)+' WAS NOT saved!'
        win.webContents.executeJavaScript("console.error(String.raw`"+`Error: Saving File\n${err}`+"`)")
    } else {
        resp.ok = 1
        resp.msg = path.basename(filename)+' saved!'
    }
    
    if (!not_open) {
        IDE.OPEN_FILES[filename] = checksum(text)
    }
    
    return resp
}

function closeFile(filename) {
    if (filename in IDE.OPEN_FILES) {
        delete IDE.OPEN_FILES[filename]
    }
    try {
        removeFileWatcher(filename) 
    } catch (e) {win.errorLog(e)}
}

//---
//---IPC Communication
//---Files
//---f:
ipcMain.on("browseFile", (event, args) => {
    let options = {
        title:'select file',
        properties: ['openFile','multiSelections'],
    }
    
    // Get Default open path
    if ('filename' in args && args.filename != '') {
        options.defaultPath = args.filename
    } else {
        options.defaultPath = IDE.settings.cwd
    }
    
    // Get Filters
    if ('filters' in args) {
        options.filters = args.filters
    }
    
    let resp = dialog.showOpenDialogSync(win,options)
    if (resp != undefined) {
        IDE.lastPath = resp[0]
        let files = []
        for (let file of resp) {
            let text = fs.readFileSync(file,'utf8')
            openFile(file,text)
            files.push({filename:file,text:text})
        }
        event.returnValue = files
    } else {
        event.returnValue = undefined
    }
})

//---f:
ipcMain.on("newFile", (event, args) => {
    let options = {
        title:'Save New File',
        defaultPath:args.filename,
        properties: ['showOverwriteConfirmation'],
    }
    let retv = {ok:0,msg:''}
    let resp = dialog.showSaveDialogSync(win,options)
    if (resp !== undefined) {
        let filename = resp
        let retv = saveFile(filename,args.text)
        openFile(filename, args.text)
        retv.new_filename = filename
        event.returnValue = retv
    } else {
        event.returnValue = {ok:0,msg:'canceled'}
    }

})

//---f:
ipcMain.on("openFile", (event, args) => {
    let filename = path.resolve(args.filename)
    let stat = 0
    let filetext = ''
    if (fs.existsSync(filename)) {
        try {
            filetext = fs.readFileSync(filename,'utf8')
            stat = 1
        } catch(e) {win.errorLog(e)}
    }
    event.returnValue = {text:filetext, status:stat, path:filename}
    if (stat) {
        openFile(filename, filetext)
    }
})

//---f:
ipcMain.on("saveFile", (event, args) => {
    event.returnValue = saveFile(args.filename,args.text,args.not_open)
})

//---f:
ipcMain.on("closeFile", (event, args) => {
    closeFile(args)
})

//---f:
ipcMain.on("closeWin", (event, args) => {
    IDE.CHECK_CLOSE=0
    win.close()
})

//---Directory
//---f:
ipcMain.on("selectDirectory", (event, args) => {
    let dflt_pth=args.path
    
    let options = {
        title:'Select Directory',
        properties: ['openDirectory'],
    }
    let fld = dialog.showOpenDialogSync(win,options)
    if (fld !== undefined) {fld = fld[0]}
    event.returnValue = fld
})

//---f:
ipcMain.on("pathExists", (event, args) => {
    event.returnValue = fs.existsSync(args.path)
})

//---f:
ipcMain.on("renamePath", (event, args) => {
    // Rename file or folder
    try {
        event.returnValue = fs.renameSync(args.old_path, args.new_path)
    } catch (e) {
        event.returnValue = 'error'
        win.errorLog(e)
    }
})

//---f:
ipcMain.on("deletePath", (event, args) => {
    // Delete file or folder
    try {
        event.returnValue = fs.unlinkSync(args)
    } catch(e) {
        event.returnValue = 'error'
        win.errorLog(e)
    }
})

//---f:
ipcMain.on("newFolder", (event, args) => {
    // new folder
    try {
        fs.mkdirSync(args)
        event.returnValue = {ok:1}
    } catch(e) {
        event.returnValue = 'error'
        win.errorLog(e)
    }
})

//---f:
ipcMain.on("getDirInfo", (event, args) => {
    // Get Directory Info
    let dir = args.path
    if (!fs.existsSync(dir)) {
        event.returnValue = undefined
        return
    }
    
    let hidden = args.hidden == 1
    
    const files = fs.readdirSync(dir, 'utf8')
    const response = {folders:[], files:[]}

    for (let file of files) {
        if (args.hidden || !file.startsWith('.')) {
            // let stats = fs.statSync(path.join(dir, file))
            let stats
            try {
                stats = fs.statSync(path.join(dir, file))
            } catch(e) {
                win.errorLog('ERROR getting stats from path: '+path.join(dir,file))
                win.errorLog(e)
            }
            if (stats === undefined) {
                // skip stats error
            } else if (stats.isDirectory()) {
                // Directory
                let fD = {
                    name: file,
                }
                response.folders.push(fD)
            } else {
                // File
                let fD = {
                    name: file,
                    ext: path.extname(file).slice(1),
                }
                response.files.push(fD)
            }
        }
    }

    // Rename file or folder
    event.returnValue = response
})

//---Workspaces
//---f:
ipcMain.on("getWorkspaces", (event, args) => {
    event.returnValue = fs.readdirSync(path.join(SETTINGS_PATH,'workspaces'), 'utf8')
})

//---f:
ipcMain.on("getWorkspace", (event, args) => {
    let wD = JSON.parse(fs.readFileSync(path.join(SETTINGS_PATH,'workspaces',args.name),'utf8'))
    // Check if path exists
    if (!fs.existsSync(wD.path)) {
        event.returnValue = 0
    } else {
        event.returnValue = wD
    }
})

//---f:
ipcMain.on("saveWorkspace", (event, args) => {
    event.returnValue = saveFile(path.join(SETTINGS_PATH,'workspaces',args.name),
        JSON.stringify(args.settings,null,2)) 
})

//---f:
ipcMain.on("renameWorkspace", (event, args) => {
    event.returnValue = fs.renameSync(path.join(SETTINGS_PATH,'workspaces',args.old_name), path.join(SETTINGS_PATH,'workspaces',args.new_name))
})

//---f:
ipcMain.on("changeWorkspacePath", (event, args) => {

    // Get workspace settings
    let wD = JSON.parse(fs.readFileSync(path.join(SETTINGS_PATH,'workspaces',args.name),'utf8'))
    
    // Select Directory
    let options = {
        title:'Select Directory',
        properties: ['openDirectory'],
        defaultPath:wD.path,
    }
    let fld = dialog.showOpenDialogSync(win,options)
    if (fld !== undefined) {
        fld = fld[0]
        wD.path = fld
        saveFile(path.join(SETTINGS_PATH,'workspaces',args.name),
            JSON.stringify(wD,null,2)) 
    }
    event.returnValue = fld

})

//---f:
ipcMain.on("deleteWorkspace", (event, args) => {
    event.returnValue = fs.unlinkSync(path.join(SETTINGS_PATH,'workspaces',args))
})


//---Settings
//---f:
ipcMain.on("getSettings", (event, args) => {
    event.returnValue = getSettings(args)
})

//---f:
ipcMain.on("settingsFile", (event, args) => {
    let settings_text = JSON.stringify(getSettings(1),null,4)
    event.returnValue = {path:path.join(SETTINGS_PATH, 'settings.json'),
        text:settings_text}
    openFile(path.join(SETTINGS_PATH, 'settings.json'),settings_text)
})

//---
//---Utils

//---f:
ipcMain.on("plugin", (event, args) => {
    if (args.plugin in IDE.plugins) {
        event.returnValue = IDE.plugins[args.plugin][args.function](args.details)
    }
})

ipcMain.handle('plugin', (event, args) => {
    if (args.plugin in IDE.plugins) {
        return IDE.plugins[args.plugin][args.function](args.details)
    }
})

//---f:
ipcMain.on("toggleDevTools", (event, args) => {
    win.webContents.toggleDevTools()
})

function checksum(str, algorithm, encoding) {
    const crypto = require('crypto')
    return crypto
        .createHash(algorithm || 'md5')
        .update(str, 'utf8')
        .digest(encoding || 'hex')
}

//---
//---App Ready
// Create window on ready
//---f:
app.on('ready', createWindow)

// Quit when all windows are closed.
//---f:
app.on('window-all-closed', () => {
    app.quit()
})

//---f:
app.on('activate', () => {
    if (win === null) {
        createWindow()
    }
})

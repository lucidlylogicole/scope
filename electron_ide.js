
// If Electron
if (window.electron_api) {
    
    //---f:load
    window.addEventListener('load', function(){

    })
    
    //---f:
    IDE.browseFiles = function() {
        // Load Current file path
        let filename = ''
        let cur_file = IDE.currentFile()
        if (cur_file) {
            filename = cur_file.path
        }
        // Browse Files
        let resp = window.electron_api.sendSync('browseFile',{filename:filename})
        if (resp) {
            for (let file of resp) {
                IDE.addEditor(file.filename,file.text)
            }
        }
    }
    
    //---f:
    IDE.openFile = async function(path,line) {
        let resp = window.electron_api.sendSync('openFile',{filename:path,watchFile:true})
        if (resp.status == 1) {
            let editorfile = await IDE.addEditor(resp.path,resp.text)
            if (line !== undefined) {
                IDE.currentEditor().gotoLine(line-1)
            }
            return editorfile
        } else {
            IDE.setStatusBar(`File not found: ${path}`,'warning',4,1)
        }
        return undefined
    }
    
    //---f:
    IDE.newFile = function(prompt) {
        if (prompt) {
            resp = window.electron_api.sendSync('newFile',{filename:IDE.CWD,text:''})
            if (resp.ok) {
                IDE.addEditor(resp.new_filename,'')
            }
        } else {
            // IDE.addEditor('Untitled '+(IDE.counter+1))
            $P.dialog.input({text:'new file name',val:'Untitled '+(IDE.counter+1)}).then(resp => {
                if (resp) {
                    IDE.addEditor(resp)
                }
            })
        }
    }
    
    //---f:
    IDE.saveFile = function(uuid) {
        // Get Current Tab
        if (uuid === undefined) {
            uuid = IDE.currentTab()
        }
        if (!(uuid && uuid in IDE.files && IDE.files[uuid].type == 'editor')) {
            return 0
        } else {
            // Get Text
            let Editor = IDE.files[uuid].Editor
            let txt = Editor.getText()
            
            let resp = {ok:0,msg:'Error saving'} 
            
            // Check New File
            if (IDE.files[uuid].path === undefined) {
                resp = window.electron_api.sendSync('newFile',{filename:IDE.files[uuid].name,text:txt,uuid:uuid})
                if (resp.new_filename === undefined) {
                    return 0
                } else {
                    IDE.renameFile(uuid,resp.new_filename)
                }
            } else {
                // Save File
                resp = window.electron_api.sendSync('saveFile',{filename:IDE.files[uuid].path,text:txt,uuid:uuid})
            }
            
            mode=''
            if (!resp.ok) {
                mode = 'error'
            }
            IDE.setStatusBar(resp.msg,mode)
            
            if (mode != 'error') {
                IDE.files[uuid].lastTime= new Date().getTime()
                
                // Update Changed
                IDE.files[uuid].changed = 0
                $P.class.remove($P.query(`.tab[data-uuid="${uuid}"] .tab-title`),'unsaved')
                Editor.CHANGED=0
                Editor.PREV_TEXT = txt
                
                // Reload if Settings File
                if (IDE.files[uuid].is_settings) {
                    // Check if valid
                    let settings_ok = 1
                    try {
                        JSON.parse(txt)
                    } catch(e) {
                        settings_ok = 0
                        $P.dialog.message({text:'There is an error saving the settings file.  It is not valid json.'})
                    }
                    if (settings_ok) {IDE.getSettings(1)} // Reload Settings
                }
                
                // Send Save Event
                var evt = new CustomEvent('fileSaved',{detail:{uuid:uuid,filename:IDE.files[uuid].path}})
                window.dispatchEvent(evt)
                
            }
            
            return resp.ok
        }
        
    }
    
    //---f:
    IDE.saveFileAs = function(uuid) {

        // Get Current Tab
        if (uuid === undefined) {
            uuid = IDE.currentTab()
        }
        
        if (!(uuid && uuid in IDE.files && IDE.files[uuid].type == 'editor')) {
            return 0
        } else {
            
            // Ask for new file
            let txt = IDE.currentEditor().getText()
            resp = window.electron_api.sendSync('newFile',{filename:IDE.currentFile().path,text:txt})
            if (resp.ok) {
                if (resp.new_filename !== IDE.currentFile().path) {
                    IDE.addEditor(resp.new_filename,txt)
                }
            }
            
            
        }

    }
    
    //---f:
    IDE.exit = async function() {
        let ok
        if (IDE.Workspace.current !== undefined) {
            ok = await IDE.Workspace.close()
        } else {
            ok = await IDE.closeAllTabs()
        }
        
        var evt = new CustomEvent('ideExit',{})
        window.dispatchEvent(evt)
        
        if (ok) {
            window.electron_api.send('closeWin')
        }
    }
    
    //---Workspace
    //---f:
    IDE.Workspace.getWorkspaces = function() {
        IDE.Workspace.workspaces = window.electron_api.sendSync('getWorkspaces')
        IDE.Workspace.workspaces.sort((a,b) => {return a.localeCompare(b)})
        return IDE.Workspace.workspaces
    }

    //---f:
    IDE.Workspace.getWorkspace = function(name) {
        return window.electron_api.sendSync('getWorkspace',{name:name})
    }
    
    //---f:
    IDE.Workspace.saveWorkspace = function(name,settings) {
        return window.electron_api.sendSync('saveWorkspace',{name:name,settings:settings})
    }

    //---f:
    IDE.Workspace.new = function(name,settings) {
        let fldpath = window.electron_api.sendSync('selectDirectory',{})
        if (fldpath) {
            let fld = IDE.getBaseName(fldpath)
            $P.dialog.input({text:'Enter name of workspace',val:fld}).then(resp=>{
                if (resp) {
                    IDE.Workspace.saveWorkspace(resp,{path:fldpath,files:[]})
                    
                    IDE.Workspace.load()
                }
            })
        }
    }
    
    //---f:
    IDE.Workspace.rename = function(name) {
        // Check if Open
        if (IDE.Workspace.current && name == IDE.Workspace.current.name) {
            $P.dialog.message({text:'You can\'t rename the workspace while it is open.  Please close the workspace first'})
        } else {
        
            $P.dialog.input({title:'Rename Workspace',val:name}).then(resp =>{
                if (resp) {
                    let ok = window.electron_api.sendSync('renameWorkspace',{old_name:name,new_name:resp})
                    IDE.Workspace.load()
                }
            })
        }
    }

    //---f:
    IDE.Workspace.delete = function(name) {
        // Check if Open
        if (IDE.Workspace.current && name == IDE.Workspace.current.name) {
            $P.dialog.message({text:'You can\'t remove the workspace while it is open.  Please close the workspace first'})
        } else {
            $P.dialog.message({title:'Remove Workspace?',text:`Do you want to remove the workspace: <b>${name}</b>?`,btns:['yes','no']}).then(resp=>{
                if (resp == 'yes') {
                    let ok = window.electron_api.sendSync('deleteWorkspace',name)
                    IDE.Workspace.load()
                }
            })
        }
    },
    
    //---f:
    IDE.Workspace.changePath = function(name) {
        let new_path = window.electron_api.sendSync('changeWorkspacePath',{name:name})
        // Update Workspace if Open
        if (new_path !== undefined && IDE.Workspace.current && name == IDE.Workspace.current.name) {
            IDE.Workspace.current.path = new_path
            if (IDE.FileBrowser) {
                IDE.FileBrowser.loadDirectory(new_path,'fb_dir')
            }
        }
    },

    //---
    //---f:
    IDE.getSettings = function(reload) {
        IDE.settings = window.electron_api.sendSync('getSettings',reload)
        if (!IDE.settings.cwd.includes('/')) {
            IDE.path_delim = '\\'
        }
        return IDE.settings
    }

    //---f:
    IDE.editSettings = function() {
        let resp = window.electron_api.sendSync('settingsFile')
        IDE.addEditor(resp.path,resp.text).then(uuid => {
            IDE.files[uuid].is_settings = 1
            
            // Also Show Docs
            IDE.viewDocs('settings.md')
        })
    },

    //---
    //---Events

    //---f:
    IDE.addDropEvent = async function(element) {
        element.ondragover = () => {return false}
        element.ondragleave = () => {return false}
        element.ondragend = () => {return false}
        element.ondrop = (e) => {
            e.preventDefault()
            for (let f of e.dataTransfer.files) {
                // Check file exists
                if (window.electron_api.sendSync('pathExists',{path:f.path})) {
                    IDE.openFile(f.path)
                }
            }
            return false
        }
    }
    
    //---f:
    IDE.toggleDevTools = function() {
        window.electron_api.send('toggleDevTools')
    }
    
    //---f:tabClosed
    window.addEventListener('tabClosed', (event)=>{
        window.electron_api.send('closeFile',event.detail.filepath)
    })

    //---f:openFile
    window.electron_api.receive("openFile", (data) => {
        IDE.openFile(data.filename)
    })
    
    //---f:fileChanged
    window.electron_api.receive("fileChanged", (data) => {
        let filename = data.filename
        
        // Find the uuid
        let uuid
        for (let f_uuid in IDE.files) {
            if (IDE.files[f_uuid].path == filename) {
                uuid = f_uuid
                break
            }
        }
        if (uuid) {
            
            if (IDE.files[uuid].lastTime > new Date().getTime()-2000) {
                return
            }
            
            if (IDE.files[uuid].dialog_open) {return} // dialog already open
            IDE.files[uuid].dialog_open = 1
            if (data.eventType == 'change') {
                $P.dialog.message({
                    title:'File Modified',
                    text:`<b>${filename}</b> has been modified.<br><br>Do you want to reload it?`,
                    btns:['yes','no'], wide:1})
                .then(resp =>{
                    IDE.files[uuid].dialog_open = 0
                    if (resp == 'yes') {
                        let fD = window.electron_api.sendSync('openFile',{filename:filename})
                        IDE.files[uuid].Editor.setText(fD.text)
                        IDE.files[uuid].changed = 0
                        IDE.files[uuid].lastTime = new Date().getTime()
                    } else {
                        IDE.files[uuid].changed = 1
                    }
                }).catch(e=>{IDE.files[uuid].dialog_open = 0})
            } else {
                $P.dialog.message({
                    title:'File Does Not Exist',
                    text:`The file, <b>${filename}</b> no longer exists.<br><br>Do you want to keep it open?`,
                    btns:['yes','no'], wide:1})
                .then(resp =>{
                    if (resp == 'no') {
                        // Close File
                        IDE.closeTab(uuid)
                    } else {
                        IDE.files[uuid].changed = 1
                        IDE.files[uuid].Editor.PREV_TEXT = ''
                        $P.class.add($P.query(`.tab[data-uuid="${uuid}"] .tab-title`),'unsaved')
                    }
                }).catch(e=>{IDE.files[uuid].dialog_open = 0})
            }
        }

    })

    //---f:closeall
    window.electron_api.receive("closeall", async (resp) => {
        IDE.exit()
    })

    //---f:statusbar
    window.electron_api.receive("status", (resp) => {
        IDE.setStatusBar(resp.text,resp.mode,resp.delay,resp.append)
    })

    //---f:plugins
    window.electron_api.receive("plugin", (resp) => {
        // Route to plugins
        IDE.plugins[resp.plugin][resp.function](resp.details)
        
    })

    //---f:closeall
    window.electron_api.receive("errorLog", async (resp) => {
        console.error(resp.error)
    })

    //---f:fileOpened
    // window.addEventListener('fileOpened', (event)=>{
    //     // console.log('open',event.detail)
    //     IDE.Workspace.update()
    // })

}

## Scope 5.1

- filebrowser changed from single click to double click to open files/folders
- keyboard navigation added to filebrowser

## Scope 5.0

The Scope 5.0 update is primarily centered around enabling other editors (besides Ace) and the capability to easily add custom tabs.

- `editor` moved to `plugins/ace_editor` folder
- added `IDE.settings.editors` section for editor settings
- `IDE.settings.ace` moved to `IDE.settings.editors.ace`
- updated Ace Editor to v1.23.1
- `IDE.settings.modes` were changed to match file extensions instead of Ace modes
    - `python` changed to `py`
    - `javascript` changed to `js`
    - `markdown` changed to `md`
- `find_replace` plugin changed to `text_tools`
- added optional `'editor'` option to each mode settings to specify the editor to be used (default is `'ace'`)
- new `IDE.addTab` function for adding custom tabs into Scope
- added `scope_night_80s` theme to replace the previous overwritten `tomorrow_night_eighties` theme
- changed `IDE.settings.ext` to `IDE.settings.extModeMap`
- added `IDE.settings.editors.ace.modeMap` to ace settings for mapping ace specific modes
- changed `IDE.settings.plugins_enabled` to `IDE.settings.pluginsEnabled`
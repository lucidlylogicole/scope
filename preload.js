const {
    contextBridge,
    ipcRenderer
} = require("electron");

// // Expose protected methods that allow the renderer process to use
// // the ipcRenderer without exposing the entire object

let sendChannels = ['openFile','saveFile','newFile','browseFile','closeFile','closeWin',
    'selectDirectory','renamePath','deletePath','getDirInfo','newFolder','pathExists',
    'getSettings','settingsFile','toggleDevTools','plugin',
    'getWorkspaces','getWorkspace','saveWorkspace','renameWorkspace','deleteWorkspace','changeWorkspacePath',
]

contextBridge.exposeInMainWorld(
    "electron_api", {
        send: (channel, data) => {
            // whitelist channels
            if (sendChannels.includes(channel)) {
                return ipcRenderer.send(channel, data)
            }
        },
        sendSync: (channel, data) => {
            // whitelist channels
            if (sendChannels.includes(channel)) {
                return ipcRenderer.sendSync(channel, data)
            }
        },
        receive: (channel, func) => {
            let validChannels = ['fileChanged','openFile','status','newFileSave','closeall','plugin','errorLog']
            if (validChannels.includes(channel)) {
                // Deliberately strip event as it includes `sender` 
                ipcRenderer.on(channel, (event, ...args) => func(...args))
            }
        },
        invoke: (channel, data) => {
            if (['plugin'].includes(channel)) {
                return ipcRenderer.invoke(channel, data)
            }
        },
    }
);

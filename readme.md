# Scope  - IDE

Scope is an open source IDE / Code Editor written in JavaScript and easily customizeable. Scope is designed for both being used as a web IDE and as a desktop IDE. This is the portable desktop Electron version.

![](docs/screenshots/scope.png)

## Features
- Portable, cross platform (Linux, Windows, Mac OS)
- Uses the [Ace Editor](https://ace.c9.io/index.html)
- Standard Plugins
    - File Browser
    - Code Outline
    - Search Files
    - Consoles
    - Output Viewer
    - Spellcheck
    - Markdown preview
- Lightweight feel
- Minimizing autocomplete from getting in the way (Ctrl+Spacebar to show)

## Requirements
- a recent version of Electron

## Installation
### From Source
1. Download a recent version of [Electron](https://www.electronjs.org/releases/stable)
2. Extract to a folder called `electron`
3. Download this repo and place in an adjacent folder to electron
    - `electron/`
    - `scope/`
4. Run `scope/scope.sh`

or 

1. Run the command `electron_app.js` with electron:

        electron/electron -i scope/electron_app.js

### Portable Version
For a portable version of Scope where the settings are stored with the app, create a folder called `.settings` in `scope/` before step 4 (above).

## Docs
- [Keyboard Shortcuts](docs/keyboard_shortcuts.md)
- [Settings](docs/settings.md)
    - [Ace Settings](docs/ace_settings.md)
- [Changelog](changelog.md) - view major changes
- [License](LICENSE) - GNU General Public License (GPL 3)

## Reference
Thanks to the following tools that Scope is built on:

- [Electron](https://www.electronjs.org/) - Desktop version
- [Ace Editor](http://ace.c9.io/) - HTML5 based code editor
- [Pebble](https://gitlab.com/lucidlylogicole/pebble) - CSS/JS Library
- [cmarkd](https://gitlab.com/lucidlylogicole/cmarkd) - utilized for [markdown](http://commonmark.org/)
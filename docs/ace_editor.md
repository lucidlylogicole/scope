# Ace Editor
Scope uses the [Ace Code Editor](https://ace.c9.io/) with additional functionality.  Most of the Ace features, settings, and keyboard shortcuts are utilized.

## Commenting
Scope utilizes special commenting to help organize the outline.

- `--- Heading` adding 3 dashes after the comment start (no space) will add a heading to the outline
- `---todo: do this` - adds a red bullet point to the outline
- `---pin:` - adds a pin to the outline to point to the section of code
- `---f:` for JavaScript, this will consider the next line of code a function and add it to the outline
- `---o:` for JavaScript, this will consider the next line of code an object and add it to the outline

## Settings
In the [Scope Settings](settings.md) file, default [Ace Settings](ace_settings.md) can be specified along with specific settings for each mode/language.

----

## Modifications to Ace
Scope has made the following modifications to ace:

- changed `Ctrl+D` to duplicate line instead of delete
- modified `mode-handlebars` and replaced comment string `{{!---` with `<!---`
- modified `mode-markdown` with `|` syntax for tables
- added `mode-p5js` for additional p5js library keywords
- modified `ext-settings_menu.js`
    - modified theme background and color
    - added scope and chive to `themeData`
- added `theme-scope.js` as primary theme
- modified `tomorrow_night_eighties` background colors a bit
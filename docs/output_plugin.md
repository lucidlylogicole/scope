# Output
Scope can display the output of running scripts in the bottom section of the IDE.  

## Output Tab
- After clicking the *run* button on the sidebar, an output tab will be created at the bottom and the file will be ran.
- While running, the *run* button will turn into a stop icon and is also used to stop the script.

![](screenshots/output.png)

- `F5` will also run the file (or stop it if currently running)
- `Shift + F5` will create the output tab but not run the file
- The output tab has its own run button that can be used.  The sidebar run button always runs the current file tab and not the current output tab.
- The command that is executed is shown at the top of the output and is editable
- Closing the output tab will also stop the process
- Right clicking anywhere in the output text will produce a menu with:
    - *Clear* - clear the output text
    - *Copy* - copy the selected text
    - *Copy All Text* - copy all output text
    - *Show/Hide Command* - show or hide the command bar at the top (default is shown)
    - *Show/Hide Input* - show or hide an input bar at the bottom (default is hidden). this can be used the file is prompting for user text input


## Run Settings
In order for Scope to run a file, the `run` parameter for the mode must be specified in the scope `modes` settings for the `mode` of the file.

![](screenshots/output_run_settings.png)

- the `{{python}}` in the example refers to the Python path set elsewhere in the settings.  For other languages, this can be replaced with the path to the respective executable.
- the `-u` is python specific for updating the output as it runs
- the `{{filename}}` is where scope inserts the current filepath with double quotes into the command
# Scope Keyboard Shortcuts

<style>
    td:first-child {
        font-family:monospace;
        font-size:0.9rem;
    }
</style>

## General
| Key               Function
|----------------------------------------------------------
| Ctrl + Tab        Go to previously opened tabs
| Ctrl + Page Up    Go to previous tab on list
| Ctrl + Page Down  Go to next tab on list
| Alt + Page Up     Go to previous bottom tab on list
| Alt + Page Down   Go to next bottom tab on list
| F3                Toggle the visibility of the left plugins (filebrowser)
| F4                Toggle the visibility of the outline
| F5                Run/Stop the current file
| Ctrl + F5         Run/Stop the currently running bottom tab
| Shift + F5        Load the command to run but don't run yet
| F6                Compile
| Shift + F5        Load the command to compile but don't run yet
| F8                Toggle the visibility of the bottom plugins
| F9                Toggle the visibility of the right plugins
| F11               Toggle Full Screen Mode - full screen and hide everything
| F12               Toggle Chromium devtools
| Alt + b           Focus on filebrowser
| Alt + e           Focus on editor

## Editor
| Key               Function
|----------------------------------------------------------
| Tab               Indent
| Shift + Tab       Unindent
| Ctrl + A          Select All
| Ctrl + C          Copy
| Ctrl + D          Duplicate current/selected line(s)
| Ctrl + Delete     Remove selected rows
| Ctrl + E          Toggle Line Comment
| Ctrl + F          Find
| Ctrl + G          Goto
| Ctrl + N          New Blank Tab
| Ctrl + O          Open File Dialog
| Ctrl + P          Insert print/console.log
| Ctrl + R          Replace
| Ctrl + S          Save
| Ctrl + V          Paste
| Ctrl + W          Toggle Word Wrap
| Ctrl + X          Cut
| Ctrl + Y          Redo
| Ctrl + Z          Undo
| Ctrl + +          Zoom In
| Ctrl + -          Zoom Out
| Ctrl + 0          Reset Zoom
| Ctrl + (space)    Show autocomplete menu
| Alt + Down        Move current selection down a line
| Alt + Up          Move current selection up a line
| Alt + P           Pin/Unpin the line to the outline
| Alt + S           Show current row and column, number of lines and number of words
| Alt + Z           Toggle Writer Mode


## Filebrowser
| Key               Function
|----------------------------------------------------------
| Up                Select previous folder/file
| Down              Select next folder/file
| Right             Go into folder or open file
| Left              Go up a directory
| Enter             Go into folder or open file
| Ctrl + Click      Go into folder or open file
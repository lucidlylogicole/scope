# Plugins
Scope comes with a set of plugins for common IDE features.

### Default Plugins

- **color_picker** - a color picker that provides output in rgb, hex, and hsl
- **filebrowser** - the main Scope filebrowser on the left
- **find_files** - search inside of files
- **home** - the default home tab when scope is open with Workspaces
- **launchers** - provides extra launchers like the console menu and git on the left sidebar
- **[output](output_plugin.md)** - displays output of files that are ran and provides simple consoles 
- **scratchpad** - simple text box for placing text
- **text_tools** - find/replace, spellcheck, file stats, and more

### Extra Plugins
Scope also comes with extra plugins that can be enabled in the settings.

- **arduino** - simple arduino functions utilizing the arduino api
- **py_syntax_checker** - checks python syntax on save or via a text tools button using the pyflakes library
- **pyqt** - launcher for Qt Designer, Qt to py converter, and QT docs shortcut
- **snippets** - store and view useful snippets

## Plugin Settings
Enabling the plugins and plugin specific settings are set in the main Scope [settings](settings.md).

- **pluginsEnabled** - the list of plugins that are enabled in the order they are loaded.
- **plugins** - main section where all the plugin settings are placed.  When a plugin is first loaded, it will generate its default settings here.